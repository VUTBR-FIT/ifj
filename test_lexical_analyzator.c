/*
 * Projekt: IFJ15 2015
 * Autoři:	Blašková Adriana (xblask02)
 * 			Čech Ondřej (xcecho03)
 * 			Černý Lukáš (xcerny63)
 * 			Dokoupil Václav (xdokou12)
 * 			Gábrle Martin (xgabrl01)
 */
#include <stdio.h>
#include <stdlib.h>
#include "error.h"
#include "lexical_analyzator.h"
#include "global_constants.h"

/**
 * Pole textu pouzr pro testovani
 */
const char *LEX_MSG[] = {
	[ST_START] = "ST_START",
	[ST_EOF] = "Konec souboru",
	[ST_ID] = "Identifikator",
	[ST_NUMBER] = "Cislo integer",
	[ST_NUMBER_EXPONENT] = "Integer zadanej exponentem",
    [IS_NUMBER_EXPONENT] = "chyba",
	[ST_DOUBLE_NUMMBER] = "Cislo double",
    [IS_DOUBLE_NUMMBER] = "chyba",
    [ST_DOUBLE_EXPONENT] = "Double zadanej exponentem",
    [IS_DOUBLE_EXPONENT] = "chyba",
	[ST_LITERAL] = "Literal",
	[IS_COMMENT_OR_DIV] = "chyba",
	[ST_LINE_COMMENT] = "Koment na radku",
	[ST_BLOCK_COMMENT] = "Blokove koment",
	[ST_BLOCK_COMMENT_END] = "Konec blokovyho komentu",
	[ST_MATH_OPERAND] = "Matematickej operand",
	[ST_ADD] = "Scitani",
	[ST_SUBB] = "Odcitani",
	[ST_DIV] = "Deleni",
	[ST_MULTI] = "Nasobeni",
	[ST_EQUAL] = "Rovnase",
	[ST_ABOVE] = "Vetsi",
	[ST_BELOW] = "Mensi",
	[ST_ABOVE_EQUAL] = "Vetsi rovno",
	[ST_BELOW_EQUAL] = "Mensi rovno",
	[ST_NOT_EQUAL] = "Nerovnase",
	[ST_ASSIGNMENT] = "Prirazeni",
	[ST_COMMA] = "carka",
	[ST_SEMICOLON] = "strednik",
	[ST_RBRACKET_L] = "leva kulata",
	[ST_RBRACKET_R] = "prava kulata",
	[ST_CBRACKET_L] = "leva slozena",
	[ST_CBRACKET_R] = "prava slozena",
	[ST_AUTO] = "sid auto",
    [ST_CIN] = "sid cin",
    [ST_COUT] = "sid cout",
    [ST_DOUBLE] = "sid double",
    [ST_ELSE] = "sid else",
    [ST_FOR] = "sid for",
    [ST_IF] = "sid if",
    [ST_INT] = "sid int",
    [ST_RETURN] = "sid return",
    [ST_STRING] = "sid string",
    [E_LEXICAL] = "lexikalni chyba",
};

int main(int argc, char *argv[]) {
    if (argc != 2){
    	errorMsg("Nebyl zadan soubor k prekladu.\n");
    	return (E_RUN_OTHER);
    } else {
    	printf("Byl zadan druhy parametr %s\n", argv[1]);
    }
    if ((sourceFile = fopen(argv[1], "r")) == NULL){
    	errorMsg("Soubor se nepodarilo otevrit.\n");
    	return (E_RUN_OTHER);
    }else{
    	printf("Soubor byl otevren\n");
    }

    currentRow = 0;
   // syntaxError = E_OK;
    int state,i=1;
    printf("Zacatek souboru\n");
    state = getToken();
    while ((state != ST_EOF)/* && (state != E_LEXICAL)*/){
        printf("%d. ",i);
    	DEBUGGING(LEX_MSG[state]);

        i=i+1;
        if((actualString != NULL))
        	printf("NACETL: %s\n", actualString->string);
        freeString();
    	if(/*(state == ST_EOF) || */(state == E_AUTHOR) /*|| (syntaxError != E_OK)*/) return (E_OK);
    	state = getToken();
    }
 //   DEBUGGING(LEX_MSG[state]);
    printf("Konec souboru\n");
    fclose(sourceFile);
    printf("Soubor byl zavren\n");
    return (EXIT_SUCCESS);
}

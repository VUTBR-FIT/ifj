/*
 * Projekt: IFJ15 2015
 * Autoři:	Blašková Adriana (xblask02)
 * 			Čech Ondřej (xcecho03)
 * 			Černý Lukáš (xcerny63)
 * 			Dokoupil Václav (xdokou12)
 * 			Gábrle Martin (xgabrl01)
 */

#include "ial.h"
#include "global_constants.h"
#include "syntax_analyzator.h"
#include "built-in_function.h"
#include "instruction_list.h"
#include "list.h"
#include "error.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int inter(tListOfInstruction *instrList){

  tListOfStackP *BlockStack = SInitP();
  tListOfStackB *Come_Back = SInitB();
  tInstr *I;
  uValue uVal;
  tValue var;
  tValue val;
  tValue result;
  PTR_List_Init();
  char co_jsem_nacetl[100];
  char* ret = NULL;
  tValue vracena_hodnota;
  var.value.uin = 0;
  val.value.uin = 0;



  char *s ;
  int i ;
  int n ;
  char *s1 ;
  char *s2 ;



  while (1){
    I = instruction_list_Get_Data(list);
    DEBUGGING("INTERPRET: new cyklus")
    if (X_DEBUG) printf("KOD_INSTRUKCE: %d\n", I->Instr);

    switch (I->Instr){

    case I_START: // hotovo
    	// Tato instrukce slouží pro start pro start programu... vyskytuje se pouze 1x, ukazuje na začátek funkce main, po vytvoření je aktivní

      PTR_list_Insert_Last(PTRlist, BTFind(globalTree,"main"));
    	break;


    case I_STOP://hotovo
      // instrukce konce programu
      PTR_list_Free(PTRlist);
      return (0);
      break;


      //hotovo
    case I_WRITE:
    	// Pokud je první adresa NULL, tak se nacází na druhem miste literar
    	if (I->address1 == NULL){// hotovo
            if(I->address2 == NULL)
            {
             errSemantic() ;
             return(0);
            }
    		printf("%s", (char *) I->address2);
    	}

      else {// hotovo
    		// Pokud prní adr není null tak vypisujeme to co je na adr1 (addresa 1 jméno proměnné )
    			 val=getValue((char *) I->address1);

               if(val.type == ST_INT){
                    printf("%u ", val.value.uin);
                }

                else if(val.type == ST_DOUBLE){
                    printf("%g", val.value.fl);
                }

                else if(val.type == ST_STRING){
                    printf("%s", val.value.ch);

            }
      }
    	break;

    	// hotovo
    case I_READ:  //hotovo
     // dodelat typ ze scanf
      // BUDE SE KONTROLOVAT TYP PROMĚNNÉ DO KTERÉ SE BUDE UKLÁDAT... POKUD SE NAČÍTÁ STRING JE JINÝ SCANF.... TO STEJNÉ U INT DOUBLE
      // instrukce pro nacteni hodnoty ze std. vstupu
      // 1. adresa - jméno proměnné kam se uloží načtená hodnota



      vracena_hodnota.type = (getValue((char *) I->address1)).type;
      scanf("%s", co_jsem_nacetl) ;
      DEBUGGING("INTERPRET: neco jsem nacetl")
      vracena_hodnota.value = setUValue(vracena_hodnota.type ,co_jsem_nacetl);
      if (syntaxError != E_OK){
            return (0);
        }
        else {
          setValue((char *) I->address1 , vracena_hodnota);
        }
      break;


    case I_CALL:
      //volaní funkce
      //vytvoření nového PTR listu -> vyhledání funkce v stromu
      // přidávám hodnoty až do té doby dokud nebude instrukce I_ARG_END

      // adresa 1 -> funkce která se bude volat
      PTR_list_Insert_Last(PTRlist, BTFind(globalTree, (char *) I->address1));
      Var_list_Activate_First(VarList);



    	break;
    case I_ARG:
       // adresa 1 proměná která se má uložit jako argument do volané funkce

      Var_list_Activate_First(PTRlist->Last->Lptr->PtrVAR);

      while (PTRlist->Last->Lptr->PtrVAR->Act->data->ID != (char *) I->address1){
            Var_list_Activate_Next(PTRlist->Last->Lptr->PtrVAR);
              if(PTRlist->Last->Lptr->PtrVAR == NULL){
                errSemantic();
                return(0);
              }
       }


      if (VarList->Act->data->type == ST_INT)
          VarList->Act->data->value.value.uin = PTRlist->Last->Lptr->PtrVAR->Act->data->value.value.uin;
      else if (VarList->Act->data->type == ST_DOUBLE)
          VarList->Act->data->value.value.fl = PTRlist->Last->Lptr->PtrVAR->Act->data->value.value.fl;
      else if (VarList->Act->data->type == ST_STRING)
          VarList->Act->data->value.value.ch = PTRlist->Last->Lptr->PtrVAR->Act->data->value.value.ch;
      Var_list_Activate_Next(VarList);


    break;

    case I_ARG_KONST:
    // adresa1 ukazatel na konstantu
    // if addr 1 == NULL -> addr 2 = literál () ukazatel na char)
      if (I->address1 != NULL){
          VarList->Act->data->value.value = setUValue(ST_INT, I->address1);
        free(I->address1);}

      else if (I->address2 != NULL){
          VarList->Act->data->value.value.ch = I->address2;
          }
      else if (I->address3 != NULL){
          VarList->Act->data->value.value = setUValue(ST_DOUBLE, I->address3);
        free(I->address3);
        }


          Var_list_Activate_Next(VarList);
      break;


    case I_ARG_END:
    // jakmile je I_ARG_END ... skáču na instrukci v INS_listu která obsahuje instrukci STARTFUNKTION hledám schodu ve jménu
    // addresa 1 jméno instrukce
    // pushnu adresu instrlistu pro návrat
    SPushB (Come_Back, list->Act);
    instruction_list_Activate_First(list);
    while(list->Act->data->Instr != I_STARTFUNCTION && list->Act->data->address1 != I->address1 ){
      instruction_list_Activate_Next(list);
      if(list->Act == NULL){
        errSemantic();
        return(0);
      }
    }
    break;
    case I_GOTO:// hotovo
      instruction_list_GO_TO(list, I->address1);
      break;

    case I_IFSTART: //hotovo
      // 1. adresa (&adress1) == 0 -> FALSE  else TRUE
      // 2. adresa místo kam skočit -> GOTO_instr


          if( (&I->address1) == 0){
              instruction_list_GO_TO(list, I->address2);
          }
          break;

    case I_ELSE:  // hotovo
      // 3. addresa ukazatewl na block kterej se má vytvořit

    case I_BLOCK_START: // hotovo
        //vytvoření vloženého blocku
        // na adrese 2 bude ukazatel na funkci ve stromu
        // na adrese 3 bude ukazatel na block
           Block_Var_Insert( BTFind(globalTree, (char *)I->address2) ,(tLParamElem *) I->address3 , BlockStack);
           break;

    case I_BLOCK_END:  // hotovo
        // zručení posledního blocku
        // bez braku provede se to samé co v IFEND
    case I_IFEND: // hotovo
      //  zrušení posledníhblocku
          Block_Delete_Last(VarList, BlockStack);
      break;



    case I_STARTFUNCTION:  //hotovo
      // 1 Adresa jméno funkce
      //  jenom break a jdu dál

      break;


    case I_ASSIGN: // hotovo ??
    	// předpoklad že na druhé adrese je ukazatel na strukturu (tDekVar *)
        // přiřazení výsledek výrazu do adresy 1
        // adresa 2 jakou hodnotu přiřadit
      //var.value = setUValue((getValue((char*) I->address1).type),((char *)(((tInstr *)I->address2)->address1)));

    	var = ((tDekVar *)I->address2)->value;
    	var.type = (getValue((char*) I->address1)).type;
    	setValue((char*) I->address1, var);
		break;


  case I_RETURN_SAVE:

       Var_list_Activate_First(VarList);

       while (VarList->Act->data->ID != (char *) I->address1){
            Var_list_Activate_Next(VarList);
              if(VarList->Act == NULL){
                errSemantic();
                return(0);
              }
       }


    if (VarList->Act->data->type == ST_INT)
          VarList->Act->data->value.value.uin = Return_value.uin;
      else if (VarList->Act->data->type == ST_DOUBLE)
          VarList->Act->data->value.value.fl = Return_value.fl;
      else if (VarList->Act->data->type == ST_STRING)
          VarList->Act->data->value.value.ch = Return_value.ch;




  break;

	case I_RETURN:
		// Kam uložit výsledek funkce
		// 1. CO

   Var_list_Activate_First(VarList);

       while (VarList->Act->data->ID != (char *) I->address1){
            Var_list_Activate_Next(VarList);
              if(VarList->Act == NULL){
                errSemantic();
                return(0);
              }
       }


    if (VarList->Act->data->type == ST_INT)
          Return_value.uin = VarList->Act->data->value.value.uin;
      else if (VarList->Act->data->type == ST_DOUBLE)
          Return_value.fl = VarList->Act->data->value.value.fl;
      else if (VarList->Act->data->type == ST_STRING)
          Return_value.ch = VarList->Act->data->value.value.ch;


    case I_ENDFUNCTION:
    	// Smaže se zásobníkový rámec
    	// skočí se na místo kde se volalo
      // vymazání ptr Listu
      //
    PTR_list_Delete_Last(PTRlist);
    list->Act = (STopPopB(Come_Back))->next;



	break;

  // 2 a 3 budou ukazatele .. nejdřív hledám v seznamu na konstanty pokud nenajdu pokud nenajdu přetypuju na char a najdu a získám
  // 1 = NULL tak to uložím no Konstant listu !=0 tak přetypuju na char a naleznu ve varlistu a ASIGNT ;

    case I_ADD:

    // adresa 2
    Var_list_Activate_First(VarList);
    KNST_list_Activate_First(KNSTlist);
        while (I->address2 != KNSTlist->Act){
          KNST_list_Activate_Next(KNSTlist);
            if(KNSTlist->Act == NULL){
              break;
            }
        }
        val = (KNST_list_Get_Data(KNSTlist))->value;
        if(KNSTlist->Act == NULL){
            while((char *)I->address2 != VarList->Act->data->ID){
                 Var_list_Activate_Next(VarList);

               if(VarList->Act == NULL){
                     errSemantic();
                      return(0);
               }
            }
            val = (Var_list_Get_Data(VarList))->value;
        }

 //------------------------------------------------------------------------------------------------------------------------
          //adresa3
      Var_list_Activate_First(VarList);
      KNST_list_Activate_First(KNSTlist);
        while (I->address3 != KNSTlist->Act){
          KNST_list_Activate_Next(KNSTlist);
            if(KNSTlist->Act == NULL){
              break;
            }
        }
        var = (KNST_list_Get_Data(KNSTlist))->value;
        if(KNSTlist->Act == NULL){
            while((char *)I->address3 != VarList->Act->data->ID){
                 Var_list_Activate_Next(VarList);

               if(VarList->Act == NULL){
                     errSemantic();
                      return(0);
               }
            }
            var = (Var_list_Get_Data(VarList))->value;
        }

//------------------------------------------------------------------------------------------------------------------------
        //zpracování

        if(val.type ==ST_INT ){
            if(var.type == ST_INT){
              result.type = ST_INT;
              result.value.uin = val.value.uin + var.value.uin;
            }
            else if(var.type == ST_DOUBLE){
              result.type = ST_DOUBLE;
              result.value.fl = val.value.uin + var.value.fl;
            }
            else if(var.type == ST_STRING){
              errSemanticBadType();
              return(0);
            }

        }
        else if(val.type ==ST_DOUBLE){
          if(var.type == ST_INT){
              result.type = ST_DOUBLE;
              result.value.fl = val.value.fl + var.value.uin;
            }
            else if(var.type == ST_DOUBLE){
              result.type = ST_DOUBLE;
              result.value.fl = val.value.fl + var.value.fl;
            }
            else if(var.type == ST_STRING){
              errSemanticBadType();
              return(0);
            }
        }
        else if(val.type ==ST_STRING){
             errSemanticBadType();
              return(0);
        }
//------------------------------------------------------------------------------------------------------------------------
          //adresa 1

        ((tKNST_list *)I->address1)->data->type = result.type;
        ((tKNST_list *)I->address1)->data->value = result;

      break;
//======================================================================================================================================
//======================================================================================================================================
//======================================================================================================================================
    case I_MOV:

    // adresa 2
    Var_list_Activate_First(VarList);
    KNST_list_Activate_First(KNSTlist);
        while (I->address2 != KNSTlist->Act){
          KNST_list_Activate_Next(KNSTlist);
            if(KNSTlist->Act == NULL){
              break;
            }
        }
        val = (KNST_list_Get_Data(KNSTlist))->value;
        if(KNSTlist->Act == NULL){
            while((char *)I->address2 != VarList->Act->data->ID){
                 Var_list_Activate_Next(VarList);

               if(VarList->Act == NULL){
                     errSemantic();
                      return(0);
               }
            }
            val = (Var_list_Get_Data(VarList))->value;
        }

 //------------------------------------------------------------------------------------------------------------------------
          //adresa3
      Var_list_Activate_First(VarList);
      KNST_list_Activate_First(KNSTlist);
        while (I->address3 != KNSTlist->Act){
          KNST_list_Activate_Next(KNSTlist);
            if(KNSTlist->Act == NULL){
              break;
            }
        }
        var = (KNST_list_Get_Data(KNSTlist))->value;
        if(KNSTlist->Act == NULL){
            while((char *)I->address3 != VarList->Act->data->ID){
                 Var_list_Activate_Next(VarList);

               if(VarList->Act == NULL){
                     errSemantic();
                      return(0);
               }
            }
            var = (Var_list_Get_Data(VarList))->value;
        }

//------------------------------------------------------------------------------------------------------------------------
        //zpracování

        if(val.type ==ST_INT ){
            if(var.type == ST_INT){
              result.type = ST_INT;
              result.value.uin = val.value.uin * var.value.uin;
            }
            else if(var.type == ST_DOUBLE){
              result.type = ST_DOUBLE;
              result.value.fl = val.value.uin * var.value.fl;
            }
            else if(var.type == ST_STRING){
              errSemanticBadType();
              return(0);
            }

        }
        else if(val.type ==ST_DOUBLE){
          if(var.type == ST_INT){
              result.type = ST_DOUBLE;
              result.value.fl = val.value.fl * var.value.uin;
            }
            else if(var.type == ST_DOUBLE){
              result.type = ST_DOUBLE;
              result.value.fl = val.value.fl * var.value.fl;
            }
            else if(var.type == ST_STRING){
              errSemanticBadType();
              return(0);
            }
        }
        else if(val.type ==ST_STRING){
             errSemanticBadType();
              return(0);
        }
//------------------------------------------------------------------------------------------------------------------------
          //adresa 1

        ((tKNST_list *)I->address1)->data->type = result.type;
        ((tKNST_list *)I->address1)->data->value = result;

      break;

//======================================================================================================================================
//======================================================================================================================================
//======================================================================================================================================
    case I_DIV:

    // adresa 2
    Var_list_Activate_First(VarList);
    KNST_list_Activate_First(KNSTlist);
        while (I->address2 != KNSTlist->Act){
          KNST_list_Activate_Next(KNSTlist);
            if(KNSTlist->Act == NULL){
              break;
            }
        }

        val = (KNST_list_Get_Data(KNSTlist))->value;


        if(KNSTlist->Act == NULL){
            while((char *)I->address2 != VarList->Act->data->ID){
                 Var_list_Activate_Next(VarList);

               if(VarList->Act == NULL){
                     errSemantic();
                      return(0);
               }
            }
            val = (Var_list_Get_Data(VarList))->value;
        }

 //------------------------------------------------------------------------------------------------------------------------
          //adresa3
      Var_list_Activate_First(VarList);
      KNST_list_Activate_First(KNSTlist);
        while (I->address3 != KNSTlist->Act){
          KNST_list_Activate_Next(KNSTlist);
            if(KNSTlist->Act == NULL){
              break;
            }
        }
        var = (KNST_list_Get_Data(KNSTlist))->value;
        if(KNSTlist->Act == NULL){
            while((char *)I->address3 != VarList->Act->data->ID){
                 Var_list_Activate_Next(VarList);

               if(VarList->Act == NULL){
                     errSemantic();
                      return(0);
               }
            }
            var = (Var_list_Get_Data(VarList))->value;
        }

//------------------------------------------------------------------------------------------------------------------------
        //zpracování

        if(val.type ==ST_INT ){
            if(var.type == ST_INT){
              result.type = ST_INT;
              result.value.uin = val.value.uin / var.value.uin;
            }
            else if(var.type == ST_DOUBLE){
              result.type = ST_DOUBLE;
              result.value.fl = val.value.uin / var.value.fl;
            }
            else if(var.type == ST_STRING){
              errSemanticBadType();
              return(0);
            }

        }
        else if(val.type ==ST_DOUBLE){
          if(var.type == ST_INT){
              result.type = ST_DOUBLE;
              result.value.fl = val.value.fl / var.value.uin;
            }
            else if(var.type == ST_DOUBLE){
              result.type = ST_DOUBLE;
              result.value.fl = val.value.fl / var.value.fl;
            }
            else if(var.type == ST_STRING){
              errSemanticBadType();
              return(0);
            }
        }
        else if(val.type ==ST_STRING){
             errSemanticBadType();
              return(0);
        }
//------------------------------------------------------------------------------------------------------------------------
          //adresa 1

        ((tKNST_list *)I->address1)->data->type = result.type;
        ((tKNST_list *)I->address1)->data->value = result;
      break;


//======================================================================================================================================
//======================================================================================================================================
//======================================================================================================================================
    case I_SUB:

    // adresa 2
    Var_list_Activate_First(VarList);
    KNST_list_Activate_First(KNSTlist);
        while (I->address2 != KNSTlist->Act){
          KNST_list_Activate_Next(KNSTlist);
            if(KNSTlist->Act == NULL){
              break;
            }
        }
        val = (KNST_list_Get_Data(KNSTlist))->value;
        if(KNSTlist->Act == NULL){
            while((char *)I->address2 != VarList->Act->data->ID){
                 Var_list_Activate_Next(VarList);

               if(VarList->Act == NULL){
                     errSemantic();
                      return(0);
               }
            }
            val = (Var_list_Get_Data(VarList))->value;
        }

 //------------------------------------------------------------------------------------------------------------------------
          //adresa3
      Var_list_Activate_First(VarList);
      KNST_list_Activate_First(KNSTlist);
        while (I->address3 != KNSTlist->Act){
          KNST_list_Activate_Next(KNSTlist);
            if(KNSTlist->Act == NULL){
              break;
            }
        }
        var = (KNST_list_Get_Data(KNSTlist))->value;
        if(KNSTlist->Act == NULL){
            while((char *)I->address3 != VarList->Act->data->ID){
                 Var_list_Activate_Next(VarList);

               if(VarList->Act == NULL){
                     errSemantic();
                      return(0);
               }
            }
            var = (Var_list_Get_Data(VarList))->value;
        }

//------------------------------------------------------------------------------------------------------------------------
        //zpracování

        if(val.type ==ST_INT ){
            if(var.type == ST_INT){
              result.type = ST_INT;
              result.value.uin = val.value.uin - var.value.uin;
            }
            else if(var.type == ST_DOUBLE){
              result.type = ST_DOUBLE;
              result.value.fl = val.value.uin - var.value.fl;
            }
            else if(var.type == ST_STRING){
              errSemanticBadType();
              return(0);
            }

        }
        else if(val.type ==ST_DOUBLE){
          if(var.type == ST_INT){
              result.type = ST_DOUBLE;
              result.value.fl = val.value.fl - var.value.uin;
            }
            else if(var.type == ST_DOUBLE){
              result.type = ST_DOUBLE;
              result.value.fl = val.value.fl - var.value.fl;
            }
            else if(var.type == ST_STRING){
              errSemanticBadType();
              return(0);
            }
        }
        else if(val.type ==ST_STRING){
             errSemanticBadType();
              return(0);
        }
//------------------------------------------------------------------------------------------------------------------------
          //adresa 1

         ((tKNST_list *)I->address1)->data->type = result.type;
        ((tKNST_list *)I->address1)->data->value = result;


      break;
//======================================================================================================================================
//======================================================================================================================================
//======================================================================================================================================
    case I_EQUAL:

    // adresa 2
    Var_list_Activate_First(VarList);
    KNST_list_Activate_First(KNSTlist);
        while (I->address2 != KNSTlist->Act){
          KNST_list_Activate_Next(KNSTlist);
            if(KNSTlist->Act == NULL){
              break;
            }
        }
        val = (KNST_list_Get_Data(KNSTlist))->value;
        if(KNSTlist->Act == NULL){
            while((char *)I->address2 != VarList->Act->data->ID){
                 Var_list_Activate_Next(VarList);

               if(VarList->Act == NULL){
                     errSemantic();
                      return(0);
               }
            }
            val = (Var_list_Get_Data(VarList))->value;
        }

 //------------------------------------------------------------------------------------------------------------------------
          //adresa3
      Var_list_Activate_First(VarList);
      KNST_list_Activate_First(KNSTlist);
        while (I->address3 != KNSTlist->Act){
          KNST_list_Activate_Next(KNSTlist);
            if(KNSTlist->Act == NULL){
              break;
            }
        }
        var = (KNST_list_Get_Data(KNSTlist))->value;
        if(KNSTlist->Act == NULL){
            while((char *)I->address3 != VarList->Act->data->ID){
                 Var_list_Activate_Next(VarList);

               if(VarList->Act == NULL){
                     errSemantic();
                      return(0);
               }
            }
            var = (Var_list_Get_Data(VarList))->value;
        }

//------------------------------------------------------------------------------------------------------------------------
        //zpracování

         if(val.type ==ST_INT ){
            if(var.type == ST_INT){
              result.type = ST_INT;
              result.value.uin = (val.value.uin == var.value.uin)?1:0;
            }
            else if(var.type == ST_DOUBLE){
              result.type = ST_INT;
             result.value.uin = ((double)val.value.uin == var.value.fl)?1:0;
            }
            else if(var.type == ST_STRING){
              errSemanticBadType();
              return(0);
            }

        }
        else if(val.type ==ST_DOUBLE){
          if(var.type == ST_INT){
              result.type = ST_INT;
              result.value.uin = (val.value.fl == (double)var.value.uin)?1:0;
            }
            else if(var.type == ST_DOUBLE){
              result.type = ST_INT;
              result.value.uin = (val.value.fl == var.value.fl)?1:0;
            }
            else if(var.type == ST_STRING){
              errSemanticBadType();
              return(0);
            }
        }
        else if(val.type ==ST_STRING){
            if (var.type == ST_STRING){
              result.type = ST_INT;
              result.value.uin = (strcmp(val.value.ch, var.value.ch) == 0)?1:0;
            }
            else{
             errSemanticBadType();
              return(0);
              }
        }

//------------------------------------------------------------------------------------------------------------------------
          //adresa 1

         ((tKNST_list *)I->address1)->data->type = result.type;
        ((tKNST_list *)I->address1)->data->value = result;

      break;
//======================================================================================================================================
//======================================================================================================================================
//======================================================================================================================================
    case I_LESSOREQUAL:
      // adresa 2
    Var_list_Activate_First(VarList);
    KNST_list_Activate_First(KNSTlist);
        while (I->address2 != KNSTlist->Act){
          KNST_list_Activate_Next(KNSTlist);
            if(KNSTlist->Act == NULL){
              break;
            }
        }
        val = (KNST_list_Get_Data(KNSTlist))->value;
        if(KNSTlist->Act == NULL){
            while((char *)I->address2 != VarList->Act->data->ID){
                 Var_list_Activate_Next(VarList);

               if(VarList->Act == NULL){
                     errSemantic();
                      return(0);
               }
            }
            val = (Var_list_Get_Data(VarList))->value;
        }

 //------------------------------------------------------------------------------------------------------------------------
          //adresa3
      Var_list_Activate_First(VarList);
      KNST_list_Activate_First(KNSTlist);
        while (I->address3 != KNSTlist->Act){
          KNST_list_Activate_Next(KNSTlist);
            if(KNSTlist->Act == NULL){
              break;
            }
        }
        var = (KNST_list_Get_Data(KNSTlist))->value;
        if(KNSTlist->Act == NULL){
            while((char *)I->address3 != VarList->Act->data->ID){
                 Var_list_Activate_Next(VarList);

               if(VarList->Act == NULL){
                     errSemantic();
                      return(0);
               }
            }
            var = (Var_list_Get_Data(VarList))->value;
        }

//------------------------------------------------------------------------------------------------------------------------
        //zpracování

         if(val.type ==ST_INT ){
            if(var.type == ST_INT){
              result.type = ST_INT;
              result.value.uin = (val.value.uin <= var.value.uin)?1:0;
            }
            else if(var.type == ST_DOUBLE){
              result.type = ST_INT;
             result.value.uin = ((double)val.value.uin <= var.value.fl)?1:0;
            }
            else if(var.type == ST_STRING){
              errSemanticBadType();
              return(0);
            }

        }
        else if(val.type ==ST_DOUBLE){
          if(var.type == ST_INT){
              result.type = ST_INT;
              result.value.uin = (val.value.fl <= (double)var.value.uin)?1:0;
            }
            else if(var.type == ST_DOUBLE){
              result.type = ST_INT;
              result.value.uin = (val.value.fl <= var.value.fl)?1:0;
            }
            else if(var.type == ST_STRING){
              errSemanticBadType();
              return(0);
            }
        }
        else if(val.type ==ST_STRING){
            if (var.type == ST_STRING){
              result.type = ST_INT;
              result.value.uin = (strcmp(val.value.ch, var.value.ch) <= 0)?1:0;
            }
            else{
             errSemanticBadType();
              return(0);
              }
        }

//------------------------------------------------------------------------------------------------------------------------
          //adresa 1

         ((tKNST_list *)I->address1)->data->type = result.type;
        ((tKNST_list *)I->address1)->data->value = result;

      break;
//======================================================================================================================================
//======================================================================================================================================
//======================================================================================================================================
    case I_GREATOREQUAL:
       // adresa 2
    Var_list_Activate_First(VarList);
    KNST_list_Activate_First(KNSTlist);
        while (I->address2 != KNSTlist->Act){
          KNST_list_Activate_Next(KNSTlist);
            if(KNSTlist->Act == NULL){
              break;
            }
        }
        val = (KNST_list_Get_Data(KNSTlist))->value;
        if(KNSTlist->Act == NULL){
            while((char *)I->address2 != VarList->Act->data->ID){
                 Var_list_Activate_Next(VarList);

               if(VarList->Act == NULL){
                     errSemantic();
                      return(0);
               }
            }
            val = (Var_list_Get_Data(VarList))->value;
        }

 //------------------------------------------------------------------------------------------------------------------------
          //adresa3
      Var_list_Activate_First(VarList);
      KNST_list_Activate_First(KNSTlist);
        while (I->address3 != KNSTlist->Act){
          KNST_list_Activate_Next(KNSTlist);
            if(KNSTlist->Act == NULL){
              break;
            }
        }
        var = (KNST_list_Get_Data(KNSTlist))->value;
        if(KNSTlist->Act == NULL){
            while((char *)I->address3 != VarList->Act->data->ID){
                 Var_list_Activate_Next(VarList);

               if(VarList->Act == NULL){
                     errSemantic();
                      return(0);
               }
            }
            var = (Var_list_Get_Data(VarList))->value;
        }

//------------------------------------------------------------------------------------------------------------------------
        //zpracování

         if(val.type ==ST_INT ){
            if(var.type == ST_INT){
              result.type = ST_INT;
              result.value.uin = (val.value.uin >= var.value.uin)?1:0;
            }
            else if(var.type == ST_DOUBLE){
              result.type = ST_INT;
             result.value.uin = ((double)val.value.uin >= var.value.fl)?1:0;
            }
            else if(var.type == ST_STRING){
              errSemanticBadType();
              return(0);
            }

        }
        else if(val.type ==ST_DOUBLE){
          if(var.type == ST_INT){
              result.type = ST_INT;
              result.value.uin = (val.value.fl >= (double)var.value.uin)?1:0;
            }
            else if(var.type == ST_DOUBLE){
              result.type = ST_INT;
              result.value.uin = (val.value.fl >= var.value.fl)?1:0;
            }
            else if(var.type == ST_STRING){
              errSemanticBadType();
              return(0);
            }
        }
        else if(val.type ==ST_STRING){
            if (var.type == ST_STRING){
              result.type = ST_INT;
              result.value.uin = (strcmp(val.value.ch, var.value.ch) >= 0)?1:0;
            }
            else{
             errSemanticBadType();
              return(0);
              }
        }

//------------------------------------------------------------------------------------------------------------------------
          //adresa 1

        ((tKNST_list *)I->address1)->data->type = result.type;
        ((tKNST_list *)I->address1)->data->value = result;

      break;
//======================================================================================================================================
//======================================================================================================================================
//======================================================================================================================================
    case I_NONEQUAL:
        Var_list_Activate_First(VarList);
    KNST_list_Activate_First(KNSTlist);
        while (I->address2 != KNSTlist->Act){
          KNST_list_Activate_Next(KNSTlist);
            if(KNSTlist->Act == NULL){
              break;
            }
        }
        val = (KNST_list_Get_Data(KNSTlist))->value;
        if(KNSTlist->Act == NULL){
            while((char *)I->address2 != VarList->Act->data->ID){
                 Var_list_Activate_Next(VarList);

               if(VarList->Act == NULL){
                     errSemantic();
                      return(0);
               }
            }
            val = (Var_list_Get_Data(VarList))->value;
        }

 //------------------------------------------------------------------------------------------------------------------------
          //adresa3
      Var_list_Activate_First(VarList);
      KNST_list_Activate_First(KNSTlist);
        while (I->address3 != KNSTlist->Act){
          KNST_list_Activate_Next(KNSTlist);
            if(KNSTlist->Act == NULL){
              break;
            }
        }
        var = (KNST_list_Get_Data(KNSTlist))->value;
        if(KNSTlist->Act == NULL){
            while((char *)I->address3 != VarList->Act->data->ID){
                 Var_list_Activate_Next(VarList);

               if(VarList->Act == NULL){
                     errSemantic();
                      return(0);
               }
            }
            var = (Var_list_Get_Data(VarList))->value;
        }

//------------------------------------------------------------------------------------------------------------------------
        //zpracování

         if(val.type ==ST_INT ){
            if(var.type == ST_INT){
              result.type = ST_INT;
              result.value.uin = (val.value.uin != var.value.uin)?1:0;
            }
            else if(var.type == ST_DOUBLE){
              result.type = ST_INT;
             result.value.uin = ((double)val.value.uin != var.value.fl)?1:0;
            }
            else if(var.type == ST_STRING){
              errSemanticBadType();
              return(0);
            }

        }
        else if(val.type ==ST_DOUBLE){
          if(var.type == ST_INT){
              result.type = ST_INT;
              result.value.uin = (val.value.fl != (double)var.value.uin)?1:0;
            }
            else if(var.type == ST_DOUBLE){
              result.type = ST_INT;
              result.value.uin = (val.value.fl != var.value.fl)?1:0;
            }
            else if(var.type == ST_STRING){
              errSemanticBadType();
              return(0);
            }
        }
        else if(val.type ==ST_STRING){
            if (var.type == ST_STRING){
              result.type = ST_INT;
              result.value.uin = (strcmp(val.value.ch, var.value.ch) != 0)?1:0;
            }
            else{
             errSemanticBadType();
              return(0);
              }
        }

//------------------------------------------------------------------------------------------------------------------------
          //adresa 1

         ((tKNST_list *)I->address1)->data->type = result.type;
        ((tKNST_list *)I->address1)->data->value = result;
      break;
//======================================================================================================================================
//======================================================================================================================================
//======================================================================================================================================
    case I_LESS:
        Var_list_Activate_First(VarList);
    KNST_list_Activate_First(KNSTlist);
        while (I->address2 != KNSTlist->Act){
          KNST_list_Activate_Next(KNSTlist);
            if(KNSTlist->Act == NULL){
              break;
            }
        }
        val = (KNST_list_Get_Data(KNSTlist))->value;
        if(KNSTlist->Act == NULL){
            while((char *)I->address2 != VarList->Act->data->ID){
                 Var_list_Activate_Next(VarList);

               if(VarList->Act == NULL){
                     errSemantic();
                      return(0);
               }
            }
            val = (Var_list_Get_Data(VarList))->value;
        }

 //------------------------------------------------------------------------------------------------------------------------
          //adresa3
      Var_list_Activate_First(VarList);
      KNST_list_Activate_First(KNSTlist);
        while (I->address3 != KNSTlist->Act){
          KNST_list_Activate_Next(KNSTlist);
            if(KNSTlist->Act == NULL){
              break;
            }
        }
        var = (KNST_list_Get_Data(KNSTlist))->value;
        if(KNSTlist->Act == NULL){
            while((char *)I->address3 != VarList->Act->data->ID){
                 Var_list_Activate_Next(VarList);

               if(VarList->Act == NULL){
                     errSemantic();
                      return(0);
               }
            }
            var = (Var_list_Get_Data(VarList))->value;
        }

//------------------------------------------------------------------------------------------------------------------------
        //zpracování

         if(val.type ==ST_INT ){
            if(var.type == ST_INT){
              result.type = ST_INT;
              result.value.uin = (val.value.uin < var.value.uin)?1:0;
            }
            else if(var.type == ST_DOUBLE){
              result.type = ST_INT;
             result.value.uin = ((double)val.value.uin < var.value.fl)?1:0;
            }
            else if(var.type == ST_STRING){
              errSemanticBadType();
              return(0);
            }

        }
        else if(val.type ==ST_DOUBLE){
          if(var.type == ST_INT){
              result.type = ST_INT;
              result.value.uin = (val.value.fl < (double)var.value.uin)?1:0;
            }
            else if(var.type == ST_DOUBLE){
              result.type = ST_INT;
              result.value.uin = (val.value.fl < var.value.fl)?1:0;
            }
            else if(var.type == ST_STRING){
              errSemanticBadType();
              return(0);
            }
        }
        else if(val.type ==ST_STRING){
            if (var.type == ST_STRING){
              result.type = ST_INT;
              result.value.uin = (strcmp(val.value.ch, var.value.ch) < 0)?1:0;
            }
            else{
             errSemanticBadType();
              return(0);
              }
        }

//------------------------------------------------------------------------------------------------------------------------
          //adresa 1

          ((tKNST_list *)I->address1)->data->type = result.type;
        ((tKNST_list *)I->address1)->data->value = result;
      break;
//======================================================================================================================================
//======================================================================================================================================
//======================================================================================================================================
    case I_GREATER:
        Var_list_Activate_First(VarList);
        KNST_list_Activate_First(KNSTlist);
        while (I->address2 != KNSTlist->Act){
          KNST_list_Activate_Next(KNSTlist);
            if(KNSTlist->Act == NULL){
              break;
            }
        }
        val = (KNST_list_Get_Data(KNSTlist))->value;
        if(KNSTlist->Act == NULL){
            while((char *)I->address2 != VarList->Act->data->ID){
                 Var_list_Activate_Next(VarList);

               if(VarList->Act == NULL){
                     errSemantic();
                      return(0);
               }
            }
            val = (Var_list_Get_Data(VarList))->value;
        }

 //------------------------------------------------------------------------------------------------------------------------
          //adresa3
      Var_list_Activate_First(VarList);
      KNST_list_Activate_First(KNSTlist);
        while (I->address3 != KNSTlist->Act){
          KNST_list_Activate_Next(KNSTlist);
            if(KNSTlist->Act == NULL){
              break;
            }
        }
        var = (KNST_list_Get_Data(KNSTlist))->value;
        if(KNSTlist->Act == NULL){
            while((char *)I->address3 != VarList->Act->data->ID){
                 Var_list_Activate_Next(VarList);

               if(VarList->Act == NULL){
                     errSemantic();
                      return(0);
               }
            }
            var = (Var_list_Get_Data(VarList))->value;
        }

//------------------------------------------------------------------------------------------------------------------------
        //zpracování

         if(val.type ==ST_INT ){
            if(var.type == ST_INT){
              result.type = ST_INT;
              result.value.uin = (val.value.uin > var.value.uin)?1:0;
            }
            else if(var.type == ST_DOUBLE){
              result.type = ST_INT;
             result.value.uin = ((double)val.value.uin > var.value.fl)?1:0;
            }
            else if(var.type == ST_STRING){
              errSemanticBadType();
              return(0);
            }

        }
        else if(val.type ==ST_DOUBLE){
          if(var.type == ST_INT){
              result.type = ST_INT;
              result.value.uin = (val.value.fl > (double)var.value.uin)?1:0;
            }
            else if(var.type == ST_DOUBLE){
              result.type = ST_INT;
              result.value.uin = (val.value.fl > var.value.fl)?1:0;
            }
            else if(var.type == ST_STRING){
              errSemanticBadType();
              return(0);
            }
        }
        else if(val.type ==ST_STRING){
            if (var.type == ST_STRING){
              result.type = ST_INT;
              result.value.uin = (strcmp(val.value.ch, var.value.ch) > 0)?1:0;
            }
            else{
             errSemanticBadType();
              return(0);
              }
        }

//------------------------------------------------------------------------------------------------------------------------
          //adresa 1
        ((tKNST_list *)I->address1)->data->type = result.type;
        ((tKNST_list *)I->address1)->data->value = result;
      break;
//======================================================================================================================================
//======================================================================================================================================
//======================================================================================================================================
    // hotovo
    case I_LENGTH:
    	var.type = ST_INT;
    	// pokud je na adrese1 NULL tak na adrese2 je literar
    	if (I->address1 == NULL){
    		uVal.uin = length((char *) I->address2);
    	} else {
			ret = (getValue((char*) I->address1)).value.ch;
			uVal.uin = length(ret);
    	}
    	Return_value = uVal;
      break;

    case I_SUBSTR:
    	// pokud na adrese 1 je NULL tak na druhe adrese je konstanta / literar
    	instruction_list_Activate_Next(instrList);
		I = instruction_list_Get_Data(list);
    	if (I->address1 == NULL){
    		s = (char*) I->address2;
    	} else {
    		s = (getValue((char*) I->address1)).value.ch;
    	}
    	DEBUGGING("SUBSTR <- CHAR")

    	instruction_list_Activate_Next(instrList);
		I = instruction_list_Get_Data(list);
    	if (I->address1 == NULL){
    		i = ((tDekVar *)I->address2)->value.value.uin;
    	} else {
    		i = (getValue((char*) I->address1)).value.uin;
    	}
    	DEBUGGING("SUBSTR <- INT")

    	instruction_list_Activate_Next(instrList);
    	I = instruction_list_Get_Data(list);
		if (I->address1 == NULL){
			n = ((tDekVar *)I->address2)->value.value.uin;
		} else {
			n = (getValue((char*) I->address1)).value.uin;
		}
		DEBUGGING("SUBSTR <- INT")
		uVal.ch = substr(s, i, n);
		Return_value = uVal;
      break;

    case I_CONCAT:
    	// pokud na adrese 1 je NULL tak na druhe adrese je konstanta / literar
		instruction_list_Activate_Next(instrList);
		I = instruction_list_Get_Data(list);
		if (I->address1 == NULL){
			s1 = (char*) I->address2;
		} else {
			s1 = (getValue((char*) I->address1)).value.ch;
		}
		DEBUGGING("I_CONCAT <- CHAR")
		// pokud na adrese 1 je NULL tak na druhe adrese je konstanta / literar
		instruction_list_Activate_Next(instrList);
		I = instruction_list_Get_Data(list);
		if (I->address1 == NULL){
			s2 = (char*) I->address2;
		} else {
			s2 = (getValue((char*) I->address1)).value.ch;
		}
		DEBUGGING("I_CONCAT <- CHAR")

		uVal.ch = concat(s1, s2);
		Return_value = uVal;
      break;

    case I_FIND:
    	// pokud na adrese 1 je NULL tak na druhe adrese je konstanta / literar
		instruction_list_Activate_Next(instrList);
		I = instruction_list_Get_Data(list);
		if (I->address1 == NULL){
			s1 = (char*) I->address2;
		} else {
			s1 = (getValue((char*) I->address1)).value.ch;
		}
		DEBUGGING("I_FIND <- CHAR")
		// pokud na adrese 1 je NULL tak na druhe adrese je konstanta / literar
		instruction_list_Activate_Next(instrList);
		I = instruction_list_Get_Data(list);
		if (I->address1 == NULL){
			s2 = (char*) I->address2;
		} else {
			s2 = (getValue((char*) I->address1)).value.ch;
		}
		DEBUGGING("I_FIND <- CHAR")

		uVal.uin = find(s1, s2);
		Return_value = uVal;
      break;

    case I_SORT:
    	// pokud na adrese 1 je NULL tak na druhe adrese je konstanta / literar
		instruction_list_Activate_Next(instrList);
		I = instruction_list_Get_Data(list);
		if (I->address1 == NULL){
			s1 = (char*) I->address2;
		} else {
			s1 = (getValue((char*) I->address1)).value.ch;
		}
		DEBUGGING("I_FIND <- CHAR")

		uVal.ch = sort(s1);
		Return_value = uVal;
      break;


    }
//

    // prejdeme na dalsi instrukci
    // POZOR! Pokud byl proveden skok, nemelo by se posouvat na dalsi instrukci!
    // Protoze ale vime, ze skok mohl byt proveden pouze na navesti a v ramci
    // navesti se nic neprovadi, muzeme si dovolit prejit na dalsi instrukci i
    // v tomto pripade, pouze tim urychlime beh programu
    instruction_list_Activate_Next(instrList);
  }

}

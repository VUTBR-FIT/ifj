/*
 * expressions.h
 *
 *  Created on: Dec 3, 2015
 *      Author: leo
 */

#ifndef EXPRESSIONS_H_
#define EXPRESSIONS_H_

//#include "ial.h"
//#include "syntax_analyzator.h"

#include "instruction_list.h"
// OBECNY ZÁSOBNÍK
typedef struct stack{
	struct stackElem *First;
	struct stackElem *Act;
	struct stackElem *Last;
}tStack;
typedef struct stackElem tStackElem;
struct stackElem{
	void *data;
	tStackElem *before;
	tStackElem *next;
};
// KONEC

typedef struct exprData{
	int type;
	char *data;
	tStackElem *list;
} tExprData;


enum{
	EX_E = -1,	//-/ Nelze - NEDEFINOVÁNO
	EX_R,		//</ Opak EX_R
	EX_L,		//=/ To co je vlevo má větší prioritu
	EX_S,		//>/ Operace jsou si rovny

} exprState;

enum {
	X_ADD,
	X_SUBB,
	X_MUL,
	X_DIV,
	X_E,
	X_NE,
	X_L,
	X_G,
	X_LE,
	X_GE,
	X_LB,
	X_RB
};
void pushStack(tStack *stack, tStackElem *elem);
tStackElem *initStackElem();
tStack *initStack();
tStackElem *createElem(int type, void *name, tStackElem *ptr);
tExprData *initData(int type, char *name, tStackElem *ptr);
void disposeData(tExprData *data);


void printStack(tStack *stack);

void disposeExpr(tStackElem *stack);
tStackElem *popExpr(tStackElem *stack);
tStackElem *pushExpr(tStackElem *stack, int type, char *value, tStackElem *ptr);
int getExpressionType(int state);

int isDouble(int var);
int isInt(int var);
int expression(int preState, int mode);
void parseExpresision(tStack *stack, tStack *op);
void calledBuildInFuction(int state);
void calledFunction(tBTNode *stack);
tKNST_list *exprGet3AInstruction(int instruction, void *adr1, void *adr2);



#endif /* EXPRESSIONS_H_ */

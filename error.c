/*
 * Projekt: IFJ15 2015
 * Autoři:	Blašková Adriana (xblask02)
 * 			Čech Ondřej (xcecho03)
 * 			Černý Lukáš (xcerny63)
 * 			Dokoupil Václav (xdokuo12)
 * 			Gábrle Martin (xgabrl01)
 */
#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>
#include <errno.h>
#include "error.h"
#include "global_constants.h"

const char *ERROR_TEMPLATE[] = {
		[E_AUTHOR] = "Author make same mistake in %s\n",
		[E_LEXICAL] = "Unknown lexem in line %d\n",
		[E_SYNTAX] = "Error on line %d : %s\n",
		[E_SEMANTIC] = "Semantic error in line %d - undefined (redefined) function or variable.\n",
		[E_SEMANTIC_TYPE_COMPATIBILTY] = "Semantic error in row %d - Bad type\n",
		[E_SEMANTIC_UTYPE] = "Semantic error: undefined\n",
		[E_SEMANTIC_OTHER] = "Error undefined error in line %d\n",
		[E_RUN_INPUT] = "Error loading the number of the input %s\n",
		[E_RUN_VARIABLE] = "Error manipulation of an uninitialized variable in line %d\n",
		[E_RUN_DIV_ZERO] = "Error in divide by zero in line %d\n",
		[E_RUN_OTHER] = "Error: %s\n",
		[E_INTERNAL] = "Interpret failed in %s.\n", //in line // malloc
};

void errSyntax(char *msg){
	errorMsg(ERROR_TEMPLATE[E_SYNTAX], currentRow, msg);
	syntaxError = E_SYNTAX;
}
void errLexical(){
	errorMsg(ERROR_TEMPLATE[E_LEXICAL], currentRow);
	syntaxError = E_LEXICAL;
}
void errInternal(char *msg){
	errorMsg(ERROR_TEMPLATE[E_INTERNAL], msg);
	syntaxError = E_INTERNAL;
}
void errSemantic(){
	errorMsg(ERROR_TEMPLATE[E_SEMANTIC], currentRow);
	syntaxError = E_SEMANTIC;
}
void errSemanticUType(){
	errorMsg(ERROR_TEMPLATE[E_SEMANTIC_UTYPE]);
	syntaxError = E_SEMANTIC_TYPE_COMPATIBILTY;
}
void errSemanticBadType(){
	errorMsg(ERROR_TEMPLATE[E_SEMANTIC_TYPE_COMPATIBILTY], currentRow);
	syntaxError = E_SEMANTIC_TYPE_COMPATIBILTY;
}
void errSemanticOther(){
	errorMsg(ERROR_TEMPLATE[E_SEMANTIC_OTHER], currentRow);
	syntaxError = E_SEMANTIC_OTHER;
}

/**
 * Napíše chybovou zpravu na chybovy vystup
 */
void errorMsg(const char *template, ...){
	va_list arguments;
	va_start(arguments, template);
	vfprintf(stderr, template, arguments);
	va_end(arguments);
}

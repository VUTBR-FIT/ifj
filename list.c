#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include "ial.h"
#include "lexical_analyzator.h"
#include "error.h"
#include "global_constants.h"
#include "list.h"


tLParamElem *LInit()
{
	/*vytvori sa prazdna polozka*/

	tLParamElem *UKList;

	if((UKList = (tLParamElem *)malloc(sizeof(tLParamElem))) == NULL)
	{
		errorMsg(ERROR_TEMPLATE[E_INTERNAL], "malloc failed");
		syntaxError = E_INTERNAL;
		return (NULL);
	}
	UKList->level = 0;
	UKList->name = NULL;
	UKList->type = 0;
	UKList->NextItem = NULL;
	UKList->BTBlock = NULL;



	return (UKList);		//vrati ukazatel na prvu polozku
}

tLParamElem *LInsert(tLParamElem *UKList, char *name, int type, uValue var)
{
	if(UKList == NULL){
		UKList = LInit();
	}

	if(UKList->name == NULL)
	{
		/*vkladame prvy krat*/

		UKList->name = name;
		UKList->type = type;
		UKList->value = var;
		UKList->NextItem = NULL;
/*

		 * //SantaIsComing == 0 tak zoznam parametrov
		 * inak zoznam blokov


		if(SantaIsComing == 0)		UKList->BTBlock = NULL;
			else  			UKList->BTBlock = BTInit();
*/
		return (UKList);
	}
	else
	{

		/*nevkladame do prazdneho zoznamu*/

		tLParamElem *UKNew, *POM;

		if((UKNew = LInit()) == NULL)
		{
			return (NULL);
		}

		UKNew->name = name;
		UKNew->type = type;
		UKNew->value = var;
		UKNew->NextItem = NULL;

		POM = UKList;

		while(POM->NextItem != NULL)
		{
			POM = POM->NextItem;
		}
		POM->NextItem = UKNew;
		return (UKNew);
	}

}

/*
 * posielaj tam ukazatel_na_polozku_v_strome->metadata.LParamItems
 * a nazov premennej, kt. chces najst
 *
 * ak tam je vrati ukazatel na polozku ak nie vrati NULL
 */
tLParamElem *LFind(tLParamElem *UKList, char *name)
{
	if(UKList == NULL)		//vyhladavanie v prazdnom zozname
	{
		return NULL;
	}
	else
	{
		tLParamElem *Pom;
		Pom = UKList;
		int n;

		while(Pom != NULL)
		{
			n = strcmp(Pom->name, name);

			if(n == 0){
				DEBUGGING("LFind() - Nalezl v LParamItem")
				return (Pom);
			}

			else		Pom = Pom->NextItem;
		}
		DEBUGGING("LFind() - Nenalezl v LParamItem")
		return NULL;
	}
}

void LDelete (tLParamElem *UKList)
{
	tLParamElem * pom;

	while(UKList != NULL)
	{

		pom = UKList->NextItem;
 		if (UKList->name != NULL){
 			free(UKList->name);
 		}

		if(UKList->BTBlock != NULL)
		{
			BTDeleteT(UKList->BTBlock);		//uvolnenie bin stromu
		}

		free(UKList);
		UKList = pom;
	}
}
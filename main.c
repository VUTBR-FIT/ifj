/*
 * Projekt: IFJ15 2015
 * Autoři:	Blašková Adriana (xblask02)
 * 			Čech Ondřej (xcecho03)
 * 			Černý Lukáš (xcerny63)
 * 			Dokoupil Václav (xdokou12)
 * 			Gábrle Martin (xgabrl01)
 */
#include <stdio.h>
#include <stdlib.h>
#include "error.h"
#include "ial.h"
#include "global_constants.h"
#include "syntax_analyzator.h"
#include "interpret.h"

int main(int argc, char *argv[]) {
	if (argc != 2) {
		errInternal("Run interpret with wrong number of arguments");
		return (E_INTERNAL);
	}
	DEBUGGING("Byl zadan vstupni parametr:")
	DEBUGGING(argv[1])
	if ((sourceFile = fopen(argv[1], "r")) == NULL) {
		errInternal("Opening source file");
		return (E_INTERNAL);
	}
	DEBUGGING("Soubor byl uspesne otevren.")

	syntaxError = E_OK;
	out = NO_INPUT;
	currentRow = 1;

	int result;
	globalTree = BTInit();
	instruction_list_Init();
	KNST_List_Init();
	if (syntaxError == E_OK){
		if (globalTree != NULL) {
			result = parse();
			switch (result) {
			case 0:
				DEBUGGING("Vsechno v poradku")

				checkFunctionInBS(globalTree);
				break;
			default:
				break;
			}
		} else {
			syntaxError = E_RUN_OTHER;
			result = syntaxError;
		}
		if (syntaxError == E_OK){
			PTR_List_Init();
			inter(list);
			KNST_list_Free(KNSTlist);
		}
	}
	KNST_list_Free(KNSTlist);
	BTDeleteT(globalTree);
	instruction_list_Free(list);
	if ((syntaxError != E_OK) && (syntaxError != result)){
		result = syntaxError;
	}
	DEBUGGING("Soubor je uspesne zavreny")
	fclose(sourceFile);
	return (result);
}


/*
 * Projekt: IFJ15 2015
 * Autoři:	Blašková Adriana (xblask02)
 * 			Čech Ondřej (xcecho03)
 * 			Černý Lukáš (xcerny63)
 * 			Dokoupil Václav (xdokou12)
 * 			Gábrle Martin (xgabrl01)
 */

//autor této části Ondřej Čech
#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include "error.h"

#ifndef LEXICAL_ANALYZATOR_H
#define	LEXICAL_ANALYZATOR_H

#define START_ALOC_STRING 7
#define ADD_ALOC_STRING 8 // Velikost o kterou zvětší alokovaná pamět

enum{
	ST_START = 100,
	ST_EOF,
	ST_ID,
	ST_NUMBER,
	ST_NUMBER_EXPONENT,
        IS_NUMBER_EXPONENT,
	ST_DOUBLE_NUMMBER,
        IS_DOUBLE_NUMMBER,
    ST_DOUBLE_EXPONENT,
        IS_DOUBLE_EXPONENT,
	ST_LITERAL,
        IS_START_ASCII,
        IS_ASCII,
	IS_COMMENT_OR_DIV,
	ST_LINE_COMMENT,
	ST_BLOCK_COMMENT,
	ST_BLOCK_COMMENT_END,
	ST_MATH_OPERAND,
	ST_ADD,
	ST_SUBB,
	ST_DIV,
	ST_MULTI,
	ST_EQUAL,
	ST_ABOVE,
	ST_BELOW,
	ST_ABOVE_EQUAL,
	ST_BELOW_EQUAL,
	ST_NOT_EQUAL,
	ST_ASSIGNMENT,
	ST_COMMA,
	ST_SEMICOLON,
	ST_RBRACKET_L,      // (
	ST_RBRACKET_R,      // )
	ST_CBRACKET_L,      // {
	ST_CBRACKET_R,      // }
    ST_AUTO,
    ST_CIN,
    ST_COUT,
    ST_DOUBLE,
    ST_ELSE,
    ST_FOR,
    ST_IF,
    ST_INT,
    ST_RETURN,
    ST_STRING,
};

typedef struct actString {
	int strLen;
	char *string;
	int allocedSize;
} tActString;

ERROR_TYPE initString();
void freeString();
bool isMathOperand(int c);
bool isCommaOrSemicolon(int c);
bool isEndIDorNo(int c);
bool isHexadecimal(int c);
int HexToInt(int FirstHex, int SecondHex);
void actString(int c, bool clear);

int getToken();

#endif	/* LEXICAL_ANALYZATOR_H */


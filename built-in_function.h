/*
 * Projekt: IFJ15 2015
 * Autoři:		Blašková Adriana (xblask02)
 * 			Čech Ondřej (xcecho03)
 * 			Černý Lukáš (xcerny63)
 * 			Dokoupil Václav (xdokuo12)
 * 			Gábrle Martin (xgabrl01)
 */

#ifndef BUILT_IN_FINCTION_H
#define BUILT_IN_FINCTION_H

int length(char *s);
char *concat(char *s1, char *s2);
char *substr(char *s, int i, int n);

#endif /* BUILD-IN_FINCTION_H */
/*
 * Projekt: IFJ15 2015
 * Autoři:	Blašková Adriana (xblask02)
 * 			Čech Ondřej (xcecho03)
 * 			Černý Lukáš (xcerny63)
 * 			Dokoupil Václav (xdokou12)
 * 			Gábrle Martin (xgabrl01)
 */
#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>
#include <string.h>
#include "expressions.h"
#include "ial.h"
#include "instruction_list.h"
#include "syntax_analyzator.h"
#include "lexical_analyzator.h"
#include "global_constants.h"
#include "error.h"
#include "list.h"

bool isTypeFunction(int type) {
	return (((type == ST_INT) || (type == ST_DOUBLE) || (type == ST_STRING)) ?
			TRUE : FALSE);
}
bool isTypeVar(int type) {
	return (((type == ST_INT) || (type == ST_DOUBLE) || (type == ST_STRING)
			|| (type == ST_AUTO)) ? TRUE : FALSE);
}
bool isExpressionOperan(int type) { //nedokoncene
	return ((type == ST_ADD) || (type == ST_SUBB) || (type == ST_DIV)
			|| (type == ST_MULTI) || (ST_RBRACKET_L) || (type == ST_RBRACKET_R)
			|| (type == ST_LITERAL) || (type == ST_NUMBER)
			|| (type == ST_DOUBLE_NUMMBER) || (type == ST_DOUBLE_EXPONENT)
			|| (type == ST_ID));
}

char *copyString(char *source){
	int lenght = (int) strlen(source);
	lenght++;
	char *result = (char *) malloc(lenght* 3* sizeof(char));

	if (result == NULL){
		errInternal("malloc new char (copyString)");
		return (NULL);
	} else {
		INFO("Kopirovani textu uspelo")
		strcpy(result, source);
		return (result);
	}
}
uValue setUValue(int type, char *value){
	uValue result;
	char *err = NULL;
	switch (type) {
		case ST_INT:
			result.uin = strtol(value, &err, 10);
		break;
		case ST_DOUBLE:
			result.fl = strtod(value, &err);
			break;
		default:
			result.ch = value;
		break;
	}
	if ((err != NULL) && (strlen(err) > 0)){
		errSemanticUType();
	}
	return (result);
}

ERROR_TYPE parse() {
	int currentToken = getToken(); // Get the first token
	freeString();
	if (syntaxError == E_OK){
		if (currentToken == E_LEXICAL) { // Unknow token --> E_SYNTAX
			errLexical();
		} else if (isTypeFunction(currentToken)) { // First token is EOF --> E_SYNTAX
			DEBUGGING("PROGRAM - START")
			program(currentToken); // Token is valid  --> CALL program()
			DEBUGGING("PROGRAM - END")
			if (syntaxError == E_OK){
				checkFunctionInBS(globalTree);
			}
		} else {
			if (currentToken == ST_EOF) {
				errSyntax("The file is empty");
			} else {
				errSyntax("Bad structure");
			}
		}
	}
	return (syntaxError);
}
void program(int currentToken) {
	function(currentToken, FALSE);
}

// bool je TRUE je treba nacist token
// bool je FALSE obshuje dalsi parameter a je potrebny
void function(int preState, bool get) {
	if (syntaxError != E_OK) return;
	int state = 0;
	int type = -1;
	while (TRUE) {
		if (token != NULL){
			freeToken(token);
		}
		level = 0;
		DEBUGGING("FUNKCE - NEW")
		if (get) {
			type = getToken();
		} else {
			type = preState;
		}
		if (syntaxError != E_OK) break;
		freeString();
		if (type == E_LEXICAL){
			errLexical();
		} else if (type == ST_EOF){
			DEBUGGING("Funkce <- EOF")
			break;
		} else if (!isTypeFunction(type)) {
			errSyntax("Unknown type");
		}
		if (syntaxError != E_OK) break; // <func_start> -> <type_func> ...
		DEBUGGING("Funkce <- Typ")


		state = getToken();
		if (syntaxError != E_OK) break;
		if (state == E_LEXICAL){
			freeString();
			errLexical();
		} else if ((state == ST_EOF) && (get == TRUE)){
			freeString();
			break;
		} else if (state != ST_ID) {
			freeString();
			errSyntax("Name of function is not valid");
		} else { // <func_start> -> <type_func> func_id...
			DEBUGGING("Funkce <- Id")
			if (globalTree == NULL){
				globalTree = BTInit();
			}
			if ((strcmp("concat", actualString->string) ==0)
					|| (strcmp("length", actualString->string) == 0)
					|| (strcmp("substr", actualString->string) == 0)
					|| (strcmp("find", actualString->string) == 0)
					|| (strcmp("sort", actualString->string) == 0)){
				errSemantic();
				break;
			}

			if ((localTree = BTFind(globalTree, actualString->string)) != NULL){
				DEBUGGING("Funkce - Nalezena v binarnim strome")
				freeString();
				// Founded
				if (localTree->metadata.defined == TRUE){
					errSemantic();
					break;
				}
			} else {
				DEBUGGING("Funkce - Nenalezena v binarnim strome")
				// Not founded
				uValue var;
				var.uin = 0;
				BTInsert(globalTree, copyString(actualString->string),
						currentRow, type, FALSE, FALSE, var);
				localTree = BTFind(globalTree, actualString->string);
				freeString();
				INFO(globalTree->metadata.name);
			}
		}
		if (syntaxError != E_OK) break;
		if (strcmp(localTree->metadata.name, "main") == 0){
			if (type != ST_INT){
				errSemantic();
			}
			localTree->metadata.LParamItems = NULL;
			localTree->metadata.declared = TRUE;
			generateInstr(I_START, NULL, NULL, NULL);
		} else {
			generateInstr(I_STARTFUNCTION, localTree->metadata.name, NULL, NULL);
		}
		// <func_start> -> <type_func> func_id ( ...
		state = getToken();
		if (syntaxError != E_OK) break;
		freeString();
		if (state == E_LEXICAL){
			errLexical();
		} else if (state != ST_RBRACKET_L) {
			errSyntax("Unexpced token");
		}
		if (syntaxError != E_OK) break;

		// <func_start> -> <type_func> func_id ( <arguments> ) ...
		DEBUGGING("Funkce - Argumenty --- START")
		arguments(localTree);
		DEBUGGING("Funkce - Argumenty --- END")
		if (syntaxError != E_OK) break;

		// <func_start> -> <type_func> func_id ( <arguments> ) { <block> } ...
		state = getToken();
		if (syntaxError != E_OK) break;
		freeString();
		if (state == E_LEXICAL){
			errLexical();
		} else if (state == ST_CBRACKET_L) {
			DEBUGGING("Funkce - definujeme funkci")
			localTree->metadata.defined = TRUE;
			DEBUGGING("Funkce - Blok - START")
			block(FALSE, TRUE);
			if (syntaxError != E_OK) break;
			DEBUGGING("Funkce - Blok - END")
			if (strcmp(localTree->metadata.name, "main") == 0){
				generateInstr(I_STOP, NULL, NULL, NULL);
			} else {
				generateInstr(I_ENDFUNCTION, NULL, NULL, NULL);
			}
			get = TRUE;
		} else if (state == ST_SEMICOLON) {
			preState = -1;
			get = TRUE;
			INFO("Byla nalezena hlavicka funkce.");
		} else {
			errSyntax("Unexpected token");
		}
		if (syntaxError != E_OK) break;
		DEBUGGING("FUNKCE - END")
	}
}
//3AD OK
//OK syntax + binTree
//konci ST_RBRACEKT_R
void arguments(tBTNode *tree) { // <rguments> --> <arguments> )
	int state = TRUE;
	int type = -1;
	tLParamElem *lastAgr = tree->metadata.LParamItems;
	while (TRUE){
		state = getToken();
		if (syntaxError != E_OK) break;
		freeString();
		if (state == E_LEXICAL){
			errLexical();
		} else if (state == ST_RBRACKET_R) { //<arguments> -> )

			INFO("Prijata prava zavorka")
			if (tree->metadata.declared == TRUE) {
				INFO("Funkce je jiz declarovana")
				if (lastAgr == NULL) {
					INFO("Funkce nema obsahovat prarametry OK")
				} else {
					errSemantic();
				}
			} else {
				tree->metadata.declared = TRUE;
				DEBUGGING("Arguments - Probehla dekalrace funkce")
			}
			break;
		} else if (!isTypeFunction(state)) {
			errSyntax("Expected type");
		}
		if (syntaxError != E_OK) break;
		type = state;
		DEBUGGING("Arguments() <- type")

		// <arguments> -> <type_func> id ...
		state = getToken();
		if (syntaxError != E_OK) break;
		if (state == E_LEXICAL){
			errLexical();
			freeString();
		} else if (state != ST_ID){
			errSyntax("Unexpected token (expected type)");
			freeString();
		} else {
			DEBUGGING("Arguments() <- ID")
		}
		if (syntaxError != E_OK) break;


		if (tree->metadata.declared == FALSE) { //definujeme
			if (tree->metadata.LParamItems == NULL){
				DEBUGGING("Arguments() - FIRST agument")
				tree->metadata.LParamItems = LInit();
			}
			tLParamElem *pom = tree->metadata.LParamItems;
			while (pom != NULL){
				if (pom->name != NULL){
					if (strcmp(pom->name, actualString->string) == 0){
						errSemanticOther();
						freeString();
						break;
					}
				}
				pom = pom->NextItem;
			}
			DEBUGGING("Arguments() - Definujeme")
					uValue var;
			LInsert(tree->metadata.LParamItems, copyString(actualString->string), type, var);
			freeString();

			state = getToken();
			if (syntaxError != E_OK) break;
			freeString();
			if (state == E_LEXICAL){
				errLexical();
			} else if (state == ST_RBRACKET_R) { //<arguments> -> <type_func> id )
				tree->metadata.declared = TRUE;
				break;
			} else if (state != ST_COMMA){
				errSyntax("Unexpected token (expected ID)");
			}
		} else {
			DEBUGGING("Arguments() - Porovnavame")
			if ((lastAgr == NULL)){
				freeString();
				errSemantic();
			} else {
				if (syntaxError != E_OK) break;
				if ((lastAgr->type == type) && (strcmp(actualString->string, lastAgr->name) == 0)){
					DEBUGGING("Arguments() - Porovnavame OK")
					lastAgr = lastAgr->NextItem;
				} else {
					errSyntax("Incopatible type or name of argument");
				}
				freeString();
			}
			if (syntaxError != E_OK) break;


			state = getToken();
			if (syntaxError != E_OK) break;
			freeString();
			if (state == E_LEXICAL){
				errLexical();
			} else if (state == ST_RBRACKET_R) { //<arguments> -> <type_func> id )
				if (lastAgr == NULL) {
					tree->metadata.declared = TRUE;
					break;
				} else {
					errSyntax("Unexpected token");
				}
			} else if (state != ST_COMMA){
				if (lastAgr == NULL) {
					errSyntax("Unexpected token");
				}
			}
			if (syntaxError != E_OK) break;
		}

	}
	if (syntaxError == E_OK){
		DEBUGGING("Arguments - END - OK")
	} else {
		DEBUGGING("Arguments - END - ERROR")
	}

}
//ok
//konci to }
void block(int get, int preState) {
	if (syntaxError != E_OK) return;
	int state = -1;
	int result;
	int mode = 1;
	//tLParamElem *param = NULL;
	tBTNode *pom = NULL;
	tLParamElem *param;
	level++;
	while (TRUE) {
		if (token != NULL){
			InsertToBS(mode, copyString(token->name), token->row, token->type, -1, -1, setUValue(token->type, "0"));
			freeToken(token);
			token = NULL;
		}
		DEBUGGING("Block - new CYCLE")
		if (syntaxError != E_OK) break;
		if (get) {
			state = preState;
			get = FALSE;
		} else if (out != NO_INPUT) {
			state = out;
			out = NO_INPUT;
		} else {
			state = getToken();
		}
		if (syntaxError != E_OK) break;
		switch (state) {
		case E_LEXICAL:
			freeString();
			errLexical();
			break;
		case ST_CBRACKET_R:
			level--;
			DEBUGGING("Block - END")
			return;
			break;
		case ST_AUTO:
		case ST_DOUBLE:
		case ST_INT:
		case ST_STRING:
			DEBUGGING("Block - TYPE_ID - START")
			freeString();
			DEBUGGING("Block <- Type")
			int type = state;

			if (syntaxError != E_OK) break;
			state = getToken();
			if (state == E_LEXICAL){
				errLexical();
				freeString();
			} else if (state != ST_ID) {
				errSyntax("Unexpected token (expected ID)");
				freeString();
			}
			if (syntaxError != E_OK) break;
			if ((strcmp("concat", actualString->string) ==0)
					|| (strcmp("length", actualString->string) == 0)
					|| (strcmp("substr", actualString->string) == 0)
					|| (strcmp("find", actualString->string) == 0)
					|| (strcmp("sort", actualString->string) == 0)){
				errSemantic();
				freeString();
				break;
			}
			DEBUGGING("Block <- ID")
			if ((BTFind(globalTree, actualString->string) == NULL) && (BTFind(globalTree, actualString->string) == NULL) && (LFind(localTree->metadata.LParamItems, actualString->string) == NULL)
					&& ((FindInBS(actualString->string, mode) == NULL))){
				InsertToBS(mode, copyString(actualString->string), currentRow, type, -1, -1, setUValue(type, "0"));
			} else {
				freeString();
				errSemantic();
			}
			if (syntaxError != E_OK) break;
			pom = FindInBS(actualString->string, FALSE);
			mode = 0;
			freeString();
			if (syntaxError != E_OK) break;
			state = getToken();
			if (syntaxError != E_OK) break;
			if (state == E_LEXICAL){
				freeString();
				errLexical();
			} else if (state == ST_SEMICOLON) {
				DEBUGGING("Block - TYPE_ID - END")
				break;
			} else if (state == ST_ASSIGNMENT) {
				DEBUGGING("Block - TYPE_ID - Expression - START")
				result = expression(ST_SEMICOLON, FALSE);
				if (result == 1){
					generateInstr(I_RETURN_SAVE, pom->metadata.name, NULL, NULL);
				} else if (result == 0){
					generateInstr(I_ASSIGN, pom->metadata.name, KNSTlist->Last->data, NULL);
				}

				DEBUGGING("Block - TYPE_ID - Expression - END")
				DEBUGGING("Block - TYPE_ID - END")
				pom->metadata.defined = TRUE;
				break;
			} else {
				errSyntax("Unexpected token (expected var)");
				freeString();
			}
			break;
		case ST_ID:
			DEBUGGING("Block - ID - START")

			if (((param = LFind(localTree->metadata.LParamItems, actualString->string)) == NULL) && ((pom = FindInBS(actualString->string, 0)) == NULL)) {
				//nenalezeno
				freeString();
				errSemantic();
			}
			if (syntaxError != E_OK) break;
			state = getToken();
			if (syntaxError != E_OK) break;
			freeString();
			if (state == E_LEXICAL){
				errLexical();
			} else if (state == ST_ASSIGNMENT) {
				DEBUGGING("Block - ID - Expression - START")
				result = expression(ST_SEMICOLON, FALSE);
				DEBUGGING("Block - ID - Expression - END")
				if (pom != NULL){
					pom->metadata.defined = TRUE;
				}
				if (pom == NULL){
					if (result == 1){
						generateInstr(I_RETURN_SAVE, param->name, NULL, NULL);
					} else if (result == 0){
						generateInstr(I_ASSIGN, param->name, KNSTlist->Last->data, NULL);
					}
				} else {
					if (result == 1){
						generateInstr(I_RETURN_SAVE, pom->metadata.name, NULL, NULL);
					} else if (result == 0){
						generateInstr(I_ASSIGN, pom->metadata.name, KNSTlist->Last->data, NULL);
					}
				}
			} else {
				errSyntax("Unexpected token");
			}
			if (syntaxError != E_OK) break;
			DEBUGGING("Block - ID - END")
			break;
		case ST_IF:
			DEBUGGING("Block - IF - START")
			freeString();
			block_if();
			DEBUGGING("Block - IF - END")
			break;
		case ST_FOR:
			DEBUGGING("Block - FOR - START")
			freeString();
			block_for();
			DEBUGGING("Block - FOR - END")
			break;
		case ST_CIN:
			DEBUGGING("Block - CIN - START")
			freeString();
			block_cin();
			DEBUGGING("Block - CIN - END")
			break;
		case ST_COUT:
			DEBUGGING("Block - COUT - START")
			freeString();
			block_cout();
			DEBUGGING("Block - COUT - END")
			break;
		case ST_CBRACKET_L:
			DEBUGGING("Block - new Block - START")
			block(FALSE, FALSE);
			DEBUGGING("Block - new Block - END")
			break;
		case ST_RETURN:
			DEBUGGING("Block - RETURN - START")
			freeString();
			result = expression(ST_SEMICOLON, FALSE);
			if (result == 1){
				generateInstr(I_RETURN_SAVE, pom->metadata.name, NULL, NULL);
			} else if (result == 0){
				generateInstr(I_ASSIGN, pom->metadata.name, KNSTlist->Last->data, NULL);
			}
			DEBUGGING("Block - RETURN - END")
			break;
		default:
			errSyntax("Undefined token");
			syntaxError = E_SYNTAX;
			break;
		}
		if (syntaxError != E_OK) break;
		if ((state == ST_CBRACKET_R)) break;
	}
	if (syntaxError != E_OK){
		DEBUGGING("Block - type ID - CHYBA")
	} else {
		DEBUGGING("Block - type ID - OK")
	}
}
//3AD OK
// ok syntax
// cin >> a >> b; // nacitani
void block_cin() {
	DEBUGGING("Block_cin() --- START")
	int state;
	int first = TRUE;	// indikace jestli nacitam poprve
	while (TRUE){
		state = getToken(); 	//
		freeString();
		if (state == E_LEXICAL){
			errLexical();
		} else if (state == ST_SEMICOLON){
			if (first){
				errSyntax("Unexpected token");
			} else {
				break;
			}
		} else if (state != ST_ABOVE) {
			errSyntax("Unexpected token");
		}
		if (syntaxError != E_OK) break;

		first = FALSE;
		state = getToken(); 	//
		freeString();
		if (state == E_LEXICAL){
			errLexical();
		} else if (state != ST_ABOVE) {
			errSyntax("Unexpected token");
		}
		if (syntaxError != E_OK) break;

		state = getToken();
		if (syntaxError != E_OK) break;
		if (state == E_LEXICAL){
			freeString();
			errLexical();
		} else if (state != ST_ID) {
			freeString();
			errSyntax("Unexpected token");
		}
		if (syntaxError != E_OK) break;
		tBTNode *pomTree = NULL;
		tLParamElem *pomParam = NULL;
		if (((pomTree = FindInBS(actualString->string, 0)) == NULL) && ((pomParam = LFind(localTree->metadata.LParamItems, actualString->string)) == NULL)){
			freeString();
			errSemantic();
		} else {
			if (pomTree != NULL){
				pomTree->metadata.defined = TRUE;
				generateInstr(I_READ, pomTree->metadata.name, NULL, NULL);
			} else {
				generateInstr(I_READ, pomParam->name, NULL, NULL);
			}
		}
	}
	if (syntaxError == E_OK){
		DEBUGGING("Block_cin() --- END")
	} else {
		DEBUGGING("Block_cin() - CHBYBA")
	}
}


//3AK
// ok syntax
// cout << "literar k vypsani" << a;
void block_cout() {
	DEBUGGING("Block_cout() --- START")
	int state;
	int first = TRUE;	// indikace jestli nacitam poprve
	while (TRUE){
		state = getToken(); 	//
		if (state == E_LEXICAL){
			errLexical();
		} else if (state == ST_SEMICOLON){
			if (first){
				errSyntax("Unexpected token");
			} else {
				break;
			}
		} else if (state != ST_BELOW) {
			errSyntax("Unexpected token");
		}
		freeString();
		if (syntaxError != E_OK) break;

		INFO ("Dostal jsem <")
		first = FALSE;
		state = getToken(); 	//
		if (state == E_LEXICAL){
			errLexical();
			freeString();
		} else if (state != ST_BELOW) {
			errSyntax("Unexpected token");
			freeString();
		}
		freeString();
		if (syntaxError != E_OK) break;
		INFO ("Dostal jsem <")
		state = getToken();
		if (syntaxError != E_OK) break;
		if (state == E_LEXICAL){
			freeString();
			errLexical();
		} else if (!((state == ST_ID) || (state == ST_LITERAL))) {
			freeString();
			errSyntax("Unexpected token");
		}
		if (syntaxError != E_OK) break;
		// pokud se jedná o promenou tak ->> Vyhledání v binarním stromě .... pokud nenajde tak chyba...
		if (state == ST_ID){
			DEBUGGING("Block_cout() <- ID")
			tBTNode *pomTree = NULL;
			tLParamElem *pomParam = NULL;
			if (((pomTree = FindInBS(actualString->string, 0)) == NULL) && ((pomParam = LFind(localTree->metadata.LParamItems, actualString->string)) == NULL)){
				errSemantic();
			} else {
				freeString();
				if (pomTree != NULL){
					if (pomTree->metadata.defined != TRUE){
						errSemantic();
					}
					generateInstr(I_WRITE, pomTree->metadata.name, NULL, NULL);
				} else {
					generateInstr(I_WRITE, pomParam->name, NULL, NULL);
				}
			}
			if (syntaxError != E_OK) break;
		} else {
			generateInstr(I_WRITE, NULL, copyString(actualString->string), NULL);
			DEBUGGING("Block_cout() <- Literar")
			freeString();
		}
	}
	if (syntaxError == E_OK){
		DEBUGGING("Block_cout() --- END")
	} else {
		DEBUGGING("Block_cout() - CHBYBA")
	}
}

void build_in_func(int state) {
	state++;
}

void block_if() {
	tLParamElem *pom = localTree->metadata.LBlockItems;
	if (pom != NULL){
		while (pom->NextItem != NULL){
			pom = pom->NextItem;
		}
	} else {
		localTree->metadata.LBlockItems = LInit();
	}

	DEBUGGING("Block_if() - START")
	int repeat = TRUE;
	while(repeat){
		repeat = FALSE;
		// if ( ...
		int state = getToken();
		if (syntaxError != E_OK) break;
		freeString();
		if (state == E_LEXICAL){
			errLexical();
		} else if (state != ST_RBRACKET_L) {
			errSyntax("Unexpected token (expected ()");
		}
		if (syntaxError != E_OK) break;
		// if ( <E> ) ...
		DEBUGGING("block_if() -  EXPRESSION - START")
		expression(ST_RBRACKET_R, FALSE);


		tInstr *ifStart = generateInstr(I_IFSTART, KNSTlist->Last, NULL, NULL);
		tInstr *blockStart = generateInstr(I_BLOCK_START, NULL, localTree->metadata.name, NULL);

		DEBUGGING("block_if() -  EXPRESSION - END")
		if (syntaxError != E_OK) break;
		// if ( <E> ) { ...
		state = getToken();
		freeString();
		if (syntaxError != E_OK) break;
		if (state == E_LEXICAL){
			errLexical();
		} else if (state != ST_CBRACKET_L) {
			errSyntax("Unexpected token (expected {)");
		}
		if (syntaxError != E_OK) break;
		// if ( <E> ) { <block> } ...
		DEBUGGING("Block_if() - BLOCK - START")
		block(FALSE, FALSE);
		DEBUGGING("Block_if() - BLOCK - END")
		if (syntaxError != E_OK) break;
		// if ( <E> ) { <block> } else...
		state = getToken();
		//printf("%d\t%d\n", state, ST_ELSE);
		if (syntaxError != E_OK) break;
		freeString();
		if (state == E_LEXICAL){
			errLexical();
		} else if (state != ST_ELSE) { // if ( <E> ) { <block> } else ...
			errSyntax("Unexpected token (expected else)");
		}

		blockStart->address3 = getLastBlock(level + 1);
		generateInstr(I_BLOCK_END, NULL, NULL, NULL);
		tInstr *goTo = generateInstr(I_GOTO, NULL, NULL, NULL);
		tInstr *pElse = generateInstr(I_ELSE, NULL, localTree->metadata.name, NULL);
		setSecondADR(ifStart, pElse);


		if (syntaxError != E_OK) break;
		DEBUGGING("block_if() <- else")
		state = getToken();
		if (syntaxError != E_OK) break;
		freeString();
		if (state == E_LEXICAL){
			errLexical();
		} else if (state != ST_CBRACKET_L) { // if ( <E> ) { <block> } else { ...
			errSyntax("Unexpected token (expected {)");
		}
		if (syntaxError != E_OK) break;
		DEBUGGING("Block_if() - BLOCK - START")
		block(FALSE, FALSE); // if ( <E> ) { <block> } else { <block> } ...
		DEBUGGING("Block_if() - BLOCK - END")
		if (syntaxError != E_OK) break;

		pElse->address3 = getLastBlock(level + 1);

		state = getToken();
		if (syntaxError != E_OK) break;
		if (state == E_LEXICAL){
			freeString();
			errLexical();
		} else { // if ( <E> ) { <block> } else { <block> }...
			out = state;
		}


		tInstr *ifEnd = generateInstr(I_IFEND, NULL, NULL, NULL);
		setSecondADR(goTo, ifEnd);


	}
	if (syntaxError == E_OK){
		DEBUGGING("Block_if() - END")
	} else {
		DEBUGGING("Block_if() - CHBYBA")
	}

}
// ok syntax
void block_for() {
	DEBUGGING("Block_for() - START")
	int repeat = TRUE;
	while(repeat){
		repeat = FALSE;
		int state = getToken();
		if (syntaxError != E_OK) break;
		freeString();
		if (state == E_LEXICAL){
			errLexical();
		} else if (state != ST_RBRACKET_L) { // for ( ...
			errSyntax("Unexpected token (expected ()");
		}
		if (syntaxError != E_OK) break;
		int type = getToken();
		if (syntaxError != E_OK) break;
		freeString();
		if (type == E_LEXICAL){
			errLexical();
		} else if (!isTypeVar(type)) { // for (<type> ...
			errSyntax("Unexpected token (expected type)");
		}
		if (syntaxError != E_OK) break;
		DEBUGGING("Block_for() <- type")
		state = getToken();
		if (syntaxError != E_OK) break;
		if (state == E_LEXICAL){
			freeString();
			errLexical();
		} else if (state != ST_ID) { // for (<type> id ...
			errSyntax("Unexpected token (expected var)");
			freeString();
		}
		if (syntaxError != E_OK) break;
		token = initToken();
		if (syntaxError != E_OK) break;
		DEBUGGING("Block_for() <- ID")

		state = getToken();
		if (syntaxError != E_OK) break;
		if (state == E_LEXICAL){
			errLexical();
			freeString();
		} else if (state == ST_ASSIGNMENT) { // for (<type> id = ...
			DEBUGGING("block_for() <- =")
			DEBUGGING("block_for() <- EXPRESSION - START")
			expression(ST_SEMICOLON, FALSE); // for (<type> id = <E> ; ...
			DEBUGGING("block_for() <- EXPRESSION - END")
		} else if (state != ST_SEMICOLON) { // for (<type> id; ...
			freeString();
			errSyntax("Unexpected token");
		}
		if (syntaxError != E_OK) break;
		DEBUGGING("block_for() - - - DEFINITION - END")
		DEBUGGING("block_for() - EXPRESSION - START")
		expression(ST_SEMICOLON, FALSE); // for (<type> id = id ; <E> ; ...
		DEBUGGING("block_for() - EXPRESSION - END")
		freeString();
		if (syntaxError != E_OK) break;
		state = getToken();
		if (syntaxError != E_OK) break;
		if (state == E_LEXICAL){
			errLexical();
		} else if (state != ST_ID) { // for (<type> id = id ; <E> ; id ...
			freeString();
			errSyntax("Unexpected token (expected var)");
		}
		DEBUGGING("block_for() <- ID")
		if (syntaxError != E_OK) break;
		state = getToken();
		if (syntaxError != E_OK) break;
		if (state == E_LEXICAL){
			freeString();
			errLexical();
		} else if (state != ST_ASSIGNMENT) { // for (<type> id = id ; <E> ; id = ...
			freeString();
			errSyntax("Unexpected token (expected =)");
		}
		if (syntaxError != E_OK) break;
		DEBUGGING("block_for() <- =")
		DEBUGGING("block_for() - EXPRESSION - START")
		expression(ST_RBRACKET_R, FALSE); // for (<type> id = id ; <E> ; id = <E> ) ...
		if (syntaxError != E_OK) break;
		DEBUGGING("block_for() - EXPRESSION - END")
		state = getToken();
		if (syntaxError != E_OK) break;
		freeString();
		if (state == E_LEXICAL){
			errLexical();
		} else if (state != ST_CBRACKET_L) { // for (<type> id = id ; <E> ; id = <E> ) { ...
			errSyntax("Unexpected token (expected {)");
		}

		//generateInstr()


		if (syntaxError != E_OK) break;
		DEBUGGING("Block_for() - BLOCK - START")
		block(FALSE, TRUE); // for (<type> id = id ; <E> ; id = <E> ) { <block> }...
		DEBUGGING("Block_for() - BLOCK - END")
		if (syntaxError != E_OK) break;
		state = getToken();
		if (syntaxError != E_OK) break;
		DEBUGGING("Block_for() - END")
		if (state == E_LEXICAL){
			freeString();
			errLexical();
		} else if (state == ST_SEMICOLON) {
			out = NO_INPUT;
			break;
		} else {
			// je oštřeno v Block()
			out = state;
			break;
		}
	}
}


void checkFunctionInBS(tBTNode *tree){
	static int issetMain = FALSE;
	static int allDefined = TRUE;
	static int printed = FALSE;
	if (tree == NULL){
		return;
	} else {
		if (tree->Lptr != NULL){
			checkFunctionInBS(tree->Lptr);
		}
		if (tree->Rptr != NULL){
			checkFunctionInBS(tree->Rptr);
		}
		if (tree->metadata.name != NULL){
			if (strcmp(tree->metadata.name, "main") == 0){
				issetMain = TRUE;
			}
		}
		if ((tree->metadata.defined != TRUE) && (allDefined == TRUE)){
			allDefined = FALSE;
		}
	}
	if ((issetMain != TRUE) || (allDefined != TRUE)){
		if (printed == FALSE){
			printed = TRUE;
			errSemantic();
		}
	}
}

tBTNode *getLastBlock(int searcheLevel){
	if (localTree == NULL){
		return (NULL);
	} else {
		if (searcheLevel < 2){
			return (NULL);
		} else {
			if (localTree->metadata.LBlockItems == NULL){
				tLParamElem *last = localTree->metadata.LBlockItems;
				tLParamElem *searched = NULL;
				while (last != NULL){
					if (last->level == searcheLevel){
						searched = last;
					}
					last = last->NextItem;
				}
				if (searched->BTBlock != NULL){
					return (searched->BTBlock);
				} else {
					return (NULL);
				}
			} else {
				return (NULL);
			}
		}
	}
}
tBTNode *FindInBS(char *subject, int mode){
	if (localTree == NULL){
		return (NULL);
	} else {
		tBTNode *pom = NULL;
		if (localTree->metadata.BTtabsym != NULL){
			if ((pom = BTFind(localTree->metadata.BTtabsym, subject)) != NULL){
				DEBUGGING("FindInBS() - Nalezeno v BTtabsym");
				return (pom);
			}
		}
		DEBUGGING("FindInBS() - Nenalezl v BTtabSym");
		if (level == 1){
			DEBUGGING("FindInBS() - Nebyla deklarovana");
			return (NULL);
		} else {
			if (localTree->metadata.LBlockItems == NULL){
				DEBUGGING("FindInBS() - prazdne bloky");
				return (NULL);
			} else {
				tLParamElem *last = localTree->metadata.LBlockItems;
				tLParamElem *searched = NULL;
				for (int count = 2; count <= level; count++){
					if (searched != NULL){
						last = searched;
					}
					while (last != NULL){
						if (last->level == count){
							searched = last;
						}
						last = last->NextItem;
					}
					if ((searched != NULL)){
						DEBUGGING("FindInBS() - nalezl se podstrom")
						if (searched->level == level){
							if (mode == TRUE){
								return (NULL);
							}
						}
						if (searched->BTBlock != NULL){
							if ((pom = BTFind(searched->BTBlock, subject)) != NULL){
								DEBUGGING("FindInBS() - Nalezl v LBlock")
								return (pom);
							}
						}

					}
				}
				DEBUGGING("FinInBS() - Nenalezl v LBlock");
				return (NULL);
			}
		}
	}
}
tBTNode *InsertToBS(int new, char *name, int row, int type, int defined, int declared, uValue var){
	DEBUGGING("InserToBS() - START");
	if (localTree == NULL){
		return (NULL);
	} else {
		if (level == 1){
			if (localTree->metadata.BTtabsym == NULL){
				DEBUGGING("InserToBS() <- Vkladani do BTtabsym FIRST");
				return (localTree->metadata.BTtabsym = BTInsert(localTree->metadata.BTtabsym, name, row, type, defined, declared, var));
			} else {
				DEBUGGING("InserToBS() <- Vkladani do BTtabsym LAST");
				return (BTInsert(localTree->metadata.BTtabsym, name, row, type, defined, declared, var));
			}
		} else {
			tLParamElem *pom = localTree->metadata.LBlockItems;
			if (localTree->metadata.LBlockItems == NULL){
				if ((localTree->metadata.LBlockItems = LInit()) == NULL){
					return (NULL);
				}
				localTree->metadata.LBlockItems->level = level;
				DEBUGGING("InserToBS() <- Vkladani do BTBlock FIRST");
				localTree->metadata.LBlockItems->BTBlock = BTInsert(localTree->metadata.LBlockItems->BTBlock, name, row, type, defined, declared, var);
				return (localTree->metadata.LBlockItems->BTBlock);
			} else {
				if (new == TRUE){
					// provede se inicializace noveho
					tLParamElem *newList = NULL;
					if ((newList = LInit()) == NULL){
						return (NULL);
					}
					newList->level = level;
					DEBUGGING("InserToBS() <- Vkladani do LBlock NEW LIST");
					if ((newList->BTBlock = BTInsert(NULL, name, row, type, defined, declared, var)) == NULL){
						LDelete(newList);
						return (NULL);
					} else {
						if (localTree->metadata.LBlockItems != NULL){
							while (pom->NextItem != NULL){
								pom = pom->NextItem;
							}
							pom->NextItem = newList;
							} else {
							DEBUGGING("FindInBS() - Vkladani do LBlock FIRST")
							localTree->metadata.LBlockItems = pom;
						}

						return (newList->BTBlock);
					}
				} else {
					DEBUGGING("InserToBS() <- Vkladani do LBlock EXIST LIST");
					while (pom->NextItem != NULL){
						pom = pom->NextItem;
					}
					if (pom->BTBlock == NULL){
						DEBUGGING("InserToBS() <- Vkladani do BTtabsym EXIST LIST FIRST");
						return (pom->BTBlock = BTInsert(pom->BTBlock, name, row, type, defined, declared, var));
					} else {
						DEBUGGING("InserToBS() <- Vkladani do BTtabsym EXIST LIST LAST");
						return (BTInsert(pom->BTBlock, name, row, type, defined, declared, var));
					}
				}
			}
		}
	}
}

/*
 * Projekt: IFJ15 2015
 * Autoři:	Blašková Adriana (xblask02)
 * 			Čech Ondřej (xcecho03)
 * 			Černý Lukáš (xcerny63)
 * 			Dokoupil Václav (xdokou12)
 * 			Gábrle Martin (xgabrl01)
 */
#include <stdio.h>
#include <stdlib.h>
#include "token_functions.h"
#include "global_constants.h"


tToken *initToken(){
	tToken *newToken = (tToken *) malloc(sizeof(tToken));
	if (newToken != NULL){
		newToken->declared = false;
		newToken->defined = false;
		newToken->name = actualString->string;
		actualString->string = NULL;
		freeString();
		newToken->params = NULL;
		newToken->row = currentRow;
		newToken->type = -1;
		INFO("Inicializace tokenu probehla OK")
		return (newToken);
	} else {
		errInternal("malloc failed (allocation structure token)");
		return (NULL);
	}
}

void freeToken(tToken *pToken){
	if (pToken == NULL){
		return;
	}
	free(pToken->name);
	tToken *pom = pToken->params;
	while(pom != NULL){
		freeToken(pom);
		pom = pom->params;
	}
	pom = NULL;
	free(pToken);
	DEBUGGING("Token byl uspesne smazan");
	return;
}

void PRINT_TOKEN(tToken *pToken){
	if (X_DEBUG == 0) return;
	if(pToken == NULL){
		DEBUGGING("Ukazatel NULL -- test_SA")
		return;
	}
	puts("********* tToken ***********");
	printf("\tName: %s\n", pToken->name);
	printf("\tType: %d\n", pToken->type);
	printf("\tRow: %d\n", pToken->row);
	printf("\tDefined: %d\n", pToken->defined);
	printf("\tDeclared: %d\n", pToken->declared);
	switch (pToken->type) {
		case ST_INT:
			printf("\tValue: %d\n", pToken->tokenValue.uin);
			break;
		case ST_DOUBLE:
			printf("\tValue: %f\n", pToken->tokenValue.fl);
			break;
		case ST_STRING:
			printf("\tValue: %s\n", pToken->tokenValue.ch);
			break;
		break;
		default:
			printf("Neznámy typ\n");
		break;
	}

	tToken *pom = pToken->params;

	if (pom != NULL){
		int interace = 0;
		printf("ARGUMENTS:\n");
		while(pom != NULL){
			interace++;
			printf("\t %d ARGUMENT: %s --- %d\n",interace, pom->name, pom->type);
			pom = pom->params;
		}
	}

	puts("********* END tToken *******");
}

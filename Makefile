CC=gcc
CFLAGS=-std=c99 -Wall -Wextra -pedantic -g
OUT=ifj15
OUTS=$(OUT) test_lexical_analyzator test_binTree
SOURCES=main.c error.c lexical_analyzator.c syntax_analyzator.c token_functions.c ial.c expressions.c instruction_list.c list.c interpret.c built-in_function.c
OBJECTS=$(SOURCES:.c=.o)
RM=rm -f

all: $(SOURCES)
	$(CC) $(CFLAGS) -o $(OUT) $(SOURCES)
	
	
test_lex: test_lexical_analyzator.c error.c lexical_analyzator.c 
	$(CC) $(CFLAGS) -o test_lexical_analyzator test_lexical_analyzator.c error.c lexical_analyzator.c

test_binTree: bs_tuturial_Luki.c error.c lexical_analyzator.c ial.c
	$(CC) $(CFLAGS) -o test_binTree bs_tuturial_Luki.c error.c lexical_analyzator.c ial.c

clean:
	$(RM) *.o $(BIN)
	$(RM) $(OUTS)
	$(RM) out errors.output
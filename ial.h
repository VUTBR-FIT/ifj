/*
 * Projekt: IFJ15 2015
 * Autoři:	Blašková Adriana (xblask02)
 * 			Čech Ondřej (xcecho03)
 * 			Černý Lukáš (xcerny63)
 * 			Dokoupil Václav (xdokou12)
 * 			Gábrle Martin (xgabrl01)
 */
//
#ifndef IAL_H
#define IAL_H

#include "ial.h"

#define SizeOfJump 256

typedef union value {
	char *ch;
	unsigned int uin;
	double fl;
} uValue;

typedef struct BTMetaData
{
	char *name;
	int row;				//cislo radku na kterem se lexem nacetl (deklaroval)
	int type;				//typ promenne nebo navratova hodnota funkce
	int defined;			//0 - nedefinovano, 1 - definovano, -1 premenna
	int declared;			//0 - nedeklarovano, 1 - deklarovano, -1 premenna

	struct BTNode *BTtabsym;		//ukazatel na binarny strom tabulky symbolov
	struct LParamElem *LParamItems;		//ukazatel na zoznam parametrov
	struct LParamElem *LBlockItems;		//ukazatel na zoznam binarnych strom

	uValue value;

} tBTMetaData;

typedef struct BTNode
{
	struct BTMetaData metadata;		//odkaz na strukturu metadata
	struct BTNode *Lptr;			//lavy podstrom
	struct BTNode *Rptr;			//pravy podsrom


} tBTNode;

typedef int tCharJump[SizeOfJump];
typedef int* tMatchJump;

/*
 * Inicializace prazdne struktury
 * 1x ALOKACE
 * @return (tBTNode *) - Ukazatel na vytvořenou strukturu
 */
tBTNode *BTInit();

/*
 * Smaže celý binární strom
 */
void BTDeleteT( tBTNode *UKtree);

/*
 * Vhledá položku v binárním stromě
 * @return NULL - Nenalezla
 * @return (tBTNode *) - ukazatel na nalezenou položku
 */
tBTNode *BTFind(tBTNode *UKtree, char *name);

/*
 * Vloží do binárního stromu položku
 * 1x ALOKACE
 * @return (tBTNode *) - Ukazatel na vloženou položku
 */
tBTNode *BTInsert(tBTNode *UKtree, char *name, int row, int type, int defined, int declared, uValue var);
void setData(tBTNode *UKtree, char *name, int row, int type, int defined, int declared, uValue var);


int Max(int a , int b);
int Min(int a , int b);
int find(char *text, char *pattern);
char *sort(char *s);
void ComputeCharJump(char *pattern, tCharJump CharJump, int PLength);
void ComputeMatchJump(char *pattern, tMatchJump MatchJump, int PLength);



#endif /* IAL_H */

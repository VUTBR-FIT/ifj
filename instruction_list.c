/*
 * Projekt: IFJ15 2015
 * Autoři:	Blašková Adriana (xblask02)
 * 			Čech Ondřej (xcecho03)
 * 			Černý Lukáš (xcerny63)
 * 			Dokoupil Václav (xdokou12)
 * 			Gábrle Martin (xgabrl01)
 */

#include "instruction_list.h"
#include "error.h"
#include "global_constants.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
//

void List_ERROR(){
	// @ E_TERMINAL
	//			-chyba interpretu
	//	errorMsgvolání funkce která vypisuje chybu zadanou v argumentech
		errInternal("Malloc FAILED");
	}

void instruction_list_Init(){
	// @ pom            -- pomocná proměná do které uložím alokované místo v paměti pro strukturu
	// @ pom->Act    	-- ukazatel na aktuální pvek v seznamu
	// @ pom->First     -- ukazatel na první prvek v seznamu
	// @ pom->Last 		-- ukazatel na poslední prvek v seznamu
	// @ list           -- globální proměná v které je uložen jednosměrný seznam pro instrukce
	//
	//  v případě chyby alokování paměti při zavolání malloc se do pom uloží NULL a zavolá funkci List_ERROR
	tListOfInstruction *pom = (tListOfInstruction *) malloc(sizeof(tListOfInstruction));

	if (pom == NULL){
		List_ERROR();
		return;
	}
	pom->Act = NULL;
    pom->First = NULL;
    pom->Last = NULL;
    list = pom;
 }

tInstr *generateInstr(int Instr , void *address1, void *address2, void *address3) {
 	// funkce pro vložení informací do struktury a zavolání funkce instruction_list_Insert_Last přiřadí strukturu na konec seznamu
 	// @ I            -- proměná do které uložím alokované místo v paměti pro strukturu
	// @ Instr     -- Proměná do které ukládáme určitou instrukci
	// @ addres1   -- Proměná v které je uložena první adresa se kterou případně pracuje daná instrukce
	// @ addres2   -- Proměná v které je uložena druhá adresa se kterou případně pracuje daná instrukce
	// @ addres3   -- Proměná v které je uložena třetí adresa se kterou případně pracuje daná instrukce
	// @ list         -- globální proměná v které je uložen jednosměrný seznam pro instrukce
	//
	//  v případě chyby alokování paměti při zavolání malloc se do pom uloží NULL a zavolá funkci List_ERROR
	uValue pom;
	pom.uin = 0;

	if (list == NULL){
		return (NULL);
	}
 	tInstr *I = (tInstr *) malloc(sizeof(tInstr));
 	if (I == NULL){
		List_ERROR();
		return NULL;
		}
 	I->Instr = Instr;
 	if (address1 == NULL){
 		if ((Instr == I_ADD) || (Instr == I_SUB)||(Instr == I_MOV)||(Instr == I_DIV)||(Instr == I_SUB)||(Instr == I_EQUAL)||(Instr == I_LESSOREQUAL)||(Instr == I_GREATOREQUAL)||(Instr == I_NONEQUAL)||(Instr == I_LESS)||(Instr == I_GREATER)){
 			I->address1 = KNST_list_Insert_Last(KNSTlist,-1,pom);
 		} else
 			I->address1 = address1;
 	} else
 		I->address1 = address1;
 	I->address2 = address2;
 	I->address3 = address3;
 	instruction_list_Insert_Last(list ,I);
 	return (I);

 }

void instruction_list_Free(tListOfInstruction *I){
	// funkce která uvolní celý seznam z paměti
	// @ tmp            -- pomocná proměnná do které se ukládá struktura
	// @ tmp->Act    	-- ukazatel na aktuální pvek v seznamu
	// @ tmp->First     -- ukazatel na první prvek v seznamu
	// @ tmp->Last 		-- ukazatel na poslední prvek v seznamu
	// funkce free uvolní strukturu z paměti

	if (I == NULL){
		return;
	}

	tListItem *tmp;
	I->Act = I->First;
	while(I->Act != NULL)
	{
		tmp = I->Act->next;
		if (I->Act->data != NULL){
			//free(I->Act->data);
		}
		if (I->Act != NULL)
		free(I->Act);
		I->Act = tmp;
	}

	I->First = NULL;
	I->Last = NULL;
	I->Act =NULL;
	free(I);

}

void instruction_list_Insert_Last(tListOfInstruction *I, tInstr *D){
		//vezme strukturu z argumentu a celý alokovaný seznam a uloží strukturu na konec seznamu
		// @ new_item				-- pomocná proměnná do které se ukládá proměnná
		// @ data 					-- ukazatel do struktury který ukazuje na užitečná data
		// @ next 					-- ukazatel do struktury který ukazuje na další strukturu v seznamu
		// @ new_item->Act    		-- ukazatel na aktuální pvek v seznamu
		// @ new_item->First   		-- ukazatel na první prvek v seznamu
		// @ new_item->Last 		-- ukazatel na poslední prvek v seznamu
		//  v případě chyby alokování paměti při zavolání malloc se do pom uloží NULL a zavolá funkci List_ERROR
		//funkce která vezme

	if (I == NULL){
		return;
	}

	tListItem *new_item =(tListItem *) malloc(sizeof(tListItem));

		if(new_item == NULL)
			{
			List_ERROR();
			return;
			}

		new_item->data = D;
		new_item->next = NULL;

		if (I->Last == NULL)
			{
			I->First = new_item;
			}
		else
			{
			I->Last->next = new_item;
			}
		I->Last = new_item;
		if (D->Instr == I_START){
				I->Act = new_item;
			}

}

void instruction_list_Activate_First(tListOfInstruction *I){
	//do aktuálniho ukzatatele vloží ukazatel na první prvek seznamu
	// @ I->Act    			-- ukazatel na aktuální pvek v seznamu
	// @ I->First   		-- ukazatel na první prvek v seznamu
	//  v případě chyby alokování paměti při zavolání malloc se do pom uloží NULL a zavolá funkci List_ERROR

	if (I->Act == NULL)
	{
		List_ERROR();
		return;
	}
	else{
		I->Act = I->First;
	}
}

void instruction_list_Activate_Next(tListOfInstruction *I){
	// posune aḱtivní prvek na náseledující prvek v seznamu
	// @ I->Act    			-- ukazatel na aktuální pvek v seznamu
	// @ I->Act->next		-- ukazatel na následující prvek seznamu který se nachází za aktuálním prvkem v seznamu seznamu
	//  v případě chyby alokování paměti při zavolání malloc se do pom uloží NULL a zavolá funkci List_ERROR


	if(I->Act != NULL){
		I->Act = I->Act->next;
	}
	else{
		List_ERROR();
		return;
	}
}


void instruction_list_GO_TO(tListOfInstruction *I, void *gotoInstruction){
	//vloží do aktuálního prvku ukazatel který dostane v argumentu a tím aplikuje skok v seznamu
	// @ I->Act    			-- ukazatel na aktuální pvek v seznamu
	// @gotoInstruction		-- Proměná v které je uložen ukazatel do paměti
	//						-- slouží k předání adresy kam má aktuální prvek ukazovat ("skočit")
	I->Act = gotoInstruction;
}

void instruction_list_Activate_Last(tListOfInstruction *I){
	//Actuální prvek bude ukazovat na poslední prvek v seznamu
	// @ I->First   		-- ukazatel na první prvek v seznamu
	// @ I->Last 			-- ukazatel na poslední prvek v seznamu
	I->Act = I->Last;

}

void setSecondADR(tInstr *I, tInstr *pom){
	// do addresy 2 ve struktuře kterou dostane z argumentu uloží ukazatel na strukturu který získá z argumentu
	// @ I->addres2  		-- Proměná v které je uložena druhá adresa se kterou případně pracuje daná instrukce
	// @ pom 				-- pomocná struktura v které je uložen ukazatel na určitou strukturu v paměti

	I->address2 = pom;
}

void setThirdADR(tInstr *I,  tInstr *pom){
	// do addresy 3 ve struktuře kterou dostane z argumentu uloží ukazatel na strukturu který získá z argumentu
	// @ I->addres3  		-- Proměná v které je uložena třetí adresa se kterou případně pracuje daná instrukce
	// @ pom 				-- pomocná struktura v které je uložen ukazatel na určitou strukturu v paměti

	I->address3 = pom;
}

tInstr *instruction_list_Get_Data(tListOfInstruction *I){
	// funkce která vrátí tu strukturu na kterou ukzauje aktuální prvek
	// @ I->Act    			-- ukazatel na aktuální pvek v seznamu
	// @ I->Act->data 		-- ukazatel do struktury který ukazuje na užitečná data
	//  v případě chyby alokování paměti při zavolání malloc se do pom uloží NULL a zavolá funkci List_ERROR

		if(I->Act == NULL)
		{
			List_ERROR();
			return NULL;

		}
		else return (I->Act->data);

}


// -----------------------------------------------------------------------------------------------------

void List_Var_ERROR(){
	// @ E_RUN_OTHER 		--	chyba
	//	errorMsgvolání funkce která vypisuje chybu zadanou v argumentech

	return;
}



void Var_List_Init(){

	// @ pom            -- pomocná proměná do které uložím alokované místo v paměti pro strukturu
	// @ pom->Act    	-- ukazatel na aktuální pvek v seznamu
	// @ pom->First     -- ukazatel na první prvek v seznamu
	// @ pom->Last 		-- ukazatel na poslední prvek v seznamu
	// @ VarList        -- globální proměná v které je uložen jednosměrný seznam pro proměnné
	//
	//  v případě chyby alokování paměti při zavolání malloc se do pom uloží NULL a zavolá funkci List_Var_ERROR

	tListOfVar *pom = (tListOfVar *) malloc(sizeof(tListOfVar));

		if (pom == NULL){
			List_Var_ERROR();
			return;
		}
	pom->Act = NULL;
    pom->First = NULL;
    pom->Last = NULL;
    pom->Block = NULL;
    pom->Blockend = NULL;
    VarList = pom;
 }


void Var_list_Free(tListOfVar *V){
	// funkce která uvolní celý seznam z paměti
	// @ tmp            -- pomocná proměnná do které se ukládá struktura
	// @ tmp->Act    	-- ukazatel na aktuální pvek v seznamu
	// @ tmp->First     -- ukazatel na první prvek v seznamu
	// @ tmp->Last 		-- ukazatel na poslední prvek v seznamu
	// funkce free uvolní strukturu z paměti

	tListVar *tmp;
		V->Act = V->First;
		while(V->Act != NULL)
		{
			tmp = V->Act->next;
			free(V->Act);
			V->Act = tmp;
		}

	V->First = NULL;
	V->Last = NULL;
	V->Act =NULL;
	free(V);

}

void Var_list_Insert_Last(tListOfVar *V, tDekVar *D){
		//vezme strukturu z argumentu a celý alokovaný seznam a uloží strukturu na konec seznamu
		// @ new_item				-- pomocná proměnná do které se ukládá proměnná
		// @ data 					-- ukazatel do struktury který ukazuje na užitečná data
		// @ next 					-- ukazatel do struktury který ukazuje na další strukturu v seznamu
		// @ new_item->Act    		-- ukazatel na aktuální pvek v seznamu
		// @ new_item->First   		-- ukazatel na první prvek v seznamu
		// @ new_item->Last 		-- ukazatel na poslední prvek v seznamu
		//  v případě chyby alokování paměti při zavolání malloc se do pom uloží NULL a zavolá funkci List_Var_ERROR

	tListVar *new_item =(tListVar *) malloc(sizeof(tListVar));

		if(new_item == NULL)
			{
			List_Var_ERROR();
			return;
			}

		new_item->data = D;
		new_item->next = NULL;

		if (V->Last == NULL)
			{
			V->First = new_item;
			}
		else
			{
			V->Last->next = new_item;
			}
		V->Last = new_item;

}


void Var_list_Activate_First(tListOfVar *V){
	//do aktuálniho ukzatatele vloží ukazatel na první prvek seznamu
	// @ V->Act    			-- ukazatel na aktuální pvek v seznamu
	// @ V->First   		-- ukazatel na první prvek v seznamu
	//  v případě chyby alokování paměti při zavolání malloc se do pom uloží NULL a zavolá funkci List_Var_ERROR

	if (V == NULL)
	{
		List_Var_ERROR();
		return;
	}
	else{
		V->Act = V->First;
	}
}

void Var_list_Activate_Next(tListOfVar *V){
	// posune aḱtivní prvek na náseledující prvek v seznamu
	// @ V->Act    			-- ukazatel na aktuální pvek v seznamu
	// @ V->Act->next		-- ukazatel na následující prvek seznamu který se nachází za aktuálním prvkem v seznamu seznamu
	//  v případě chyby alokování paměti při zavolání malloc se do pom uloží NULL a zavolá funkci List_Var_ERROR
	if(V->Act != NULL){
		V->Act = V->Act->next;
	}
	else{
		List_Var_ERROR();
		return;
	}
}

tDekVar *Var_list_Get_Data(tListOfVar *V){
	// funkce která vrátí tu strukturu na kterou ukzauje aktuální prvek
	// @ V->Act    			-- ukazatel na aktuální pvek v seznamu
	// @ V->Act->data 		-- ukazatel do struktury který ukazuje na užitečná data
	//  v případě chyby alokování paměti při zavolání malloc se do pom uloží NULL a zavolá funkci List_Var_ERROR

		if(V->Act == NULL)
		{
			List_Var_ERROR();
			return NULL;

		}
		else return (V->Act->data);
}


void generateVar(tBTNode *N) {


 	Var_List_Init();


		tLParamElem *P;
		P=N->metadata.LParamItems;
		while (P!= NULL){
				tDekVar *V = (tDekVar *) malloc(sizeof(tDekVar));
 				if (V == NULL){
					List_Var_ERROR();
					return;
					}
				V->type = P->type;
				V->ID = P->name;
				V->value.value = P->value;
				V->defined = 1;//----------------------------------------------------------------------------------------
				Var_list_Insert_Last(VarList ,V);
				P = P->NextItem;
				}


				Rekurze(N->metadata.BTtabsym);
 		}

 void Rekurze(tBTNode *N){
	 if (N == NULL) return;
 	tDekVar *V = (tDekVar *) malloc(sizeof(tDekVar));
 	if (V == NULL){
		List_Var_ERROR();
		return;
		}

		V->ID = N->metadata.name;
		V->type = N->metadata.type;
		V->value.type = N->metadata.type;
		if (N->metadata.type == ST_INT)
			V->value.value.uin = N->metadata.value.uin;
		else if (N->metadata.type == ST_DOUBLE)
			V->value.value.fl = N->metadata.value.fl;
		else if (N->metadata.type == ST_STRING)
			V->value.value.ch = N->metadata.value.ch;

 		if (N->Rptr != NULL)
				Rekurze(N->Rptr);
			if (N->Lptr != NULL)
				Rekurze(N->Lptr);
			Var_list_Insert_Last(VarList ,V);


 }

//---------------------------------------------------------------------------------------------------------------------------------------------------------

void PTR_Var_ERROR(){


			errorMsg(ERROR_TEMPLATE[E_INTERNAL], "selhání alokace struktury pro seznam ukazatelů na seznamy");
}



void PTR_List_Init(){

	tListPtrVar *pom = (tListPtrVar *) malloc(sizeof(tListPtrVar));

		if (pom == NULL){
			PTR_Var_ERROR();
		}

    pom->First = NULL;
    pom->Last = NULL;
    PTRlist = pom;
}


void PTR_list_Free(tListPtrVar *P){

	tPTRVar *tmp;

		while(P->First != NULL)
		{
			tmp = P->First->Rptr;
			Var_list_Free(P->First->PtrVAR);
			free(P->First);
			P->First = tmp;
		}
	P->First = NULL;
	P->Last = NULL;
	free(P);
}



void PTR_list_Insert_Last(tListPtrVar *P, tBTNode *BT){  // zavolat jen tohle

	tPTRVar *pom;
	tPTRVar *new_item =(tPTRVar *) malloc(sizeof(tPTRVar));

		if(new_item == NULL) {
			PTR_Var_ERROR();
			return;
		}
		generateVar(BT);
		new_item->PtrVAR = VarList;
		if (P->First == P->Last) {
			new_item->Lptr = NULL;
			new_item->Rptr = NULL;
			P->First = new_item;
		} else {
			pom = P->Last;
			P->Last->Rptr = new_item;
			new_item->Lptr= pom;
			new_item->Rptr=	NULL;
		}

		P->Last = new_item;
		VarList = P->Last->PtrVAR	;

}


void PTR_list_Delete_Last(tListPtrVar *P){
		tPTRVar *pom;
		if(PTRlist == NULL){
			PTR_Var_ERROR();
		}
		else
		{
			pom = P->Last;
			Var_list_Free(VarList);

			P->Last = P->Last->Lptr;
			P->Last->Rptr = NULL;
			VarList = P->Last->PtrVAR;

			free(pom);

		}


}
tListOfVar *PTR_list_Get_Data(tListPtrVar *P){
	// funkce která vrátí tu strukturu na kterou ukzauje aktuální prvek
	// @ V->Act    			-- ukazatel na aktuální pvek v seznamu
	// @ V->Act->data 		-- ukazatel do struktury který ukazuje na užitečná data
	//  v případě chyby alokování paměti při zavolání malloc se do pom uloží NULL a zavolá funkci List_Var_ERROR

		if(VarList == NULL)
		{
			PTR_Var_ERROR();
			return NULL;

		}
		else return (P->Last->PtrVAR);
}



tValue getValue(char *D){

	tValue result;
	result.value.uin = 0;


	if( D == NULL){
		return (result);
	}
	else{
		Var_list_Activate_First(VarList);

		while(strcmp(D, VarList->Act->data->ID) != 0){
			Var_list_Activate_Next(VarList);

			if(VarList->Act == NULL){
				List_Var_ERROR();
				return (result);
			}
		}
		return (Var_list_Get_Data(VarList)->value);
	}
}

void setValue(char *I, tValue D){
	if ((I == NULL) ){
		List_Var_ERROR();
		return;
	}
	else{
		Var_list_Activate_First(VarList);

		while ((strcmp(I, VarList->Act->data->ID)) != 0){
			Var_list_Activate_Next(VarList);

			if(VarList->Act == NULL){
				List_Var_ERROR();
				return;
			}

		}
		VarList->Act->data->value = D;
	}
}


tListOfStackP *SInitP(){
	tListOfStackP *pom = (tListOfStackP *) malloc(sizeof(tListOfStackP));

		if (pom == NULL){
			PTR_Var_ERROR();
			return NULL;
		}
    pom->First = NULL;
    pom->Last = NULL;
    return (pom);
}

void SPushP(tListOfStackP *S, tListVar *ptr){

	//tLStackB *pom;
	tListVar *new_item =(tListVar *) malloc(sizeof(tListVar));

		if(new_item == NULL) {
			List_Var_ERROR();
			return;
		}
		new_item = ptr;
		if(S->First == S->Last){
			new_item->next = NULL;
			S->Last = new_item;
		}
		else{
			new_item->next = S->First;
		}


		S->First = new_item;

  }

tListVar *STopP(tListOfStackP *S){
	if(S->First == NULL){
		List_Var_ERROR();
		return (NULL);
	}
	else{
		return (S->First);

	}
}

tListVar *STopPopP(tListOfStackP *S){
	if (S->First == NULL)  {

		List_Var_ERROR();
		return(NULL);
	}
	else{
		tListVar *pom;
		tListVar *suicide;
		suicide = S->First;
		pom = S->First;

		if (S->First->next == NULL){
			S->First = NULL;
			S->Last = NULL;
		}
		else{
			S->First = S->First->next;
		}

		free(suicide);
		return (pom);
	}
}

bool SEmptyP(tListOfStackP *S){
	if (S->First == NULL){
		return TRUE;
	}
	else{
		return FALSE;
	}
}

//=========================================================


tListOfStackB *SInitB(){
	tListOfStackB *pom = (tListOfStackB *) malloc(sizeof(tListOfStackB));

		if (pom == NULL){
			PTR_Var_ERROR();
			return (NULL);
		}
    pom->First = NULL;
    pom->Last = NULL;
    return (pom);
}

void SPushB(tListOfStackB *S, tListItem *ptr){

	//tLStackB *pom;
	tListItem *new_item =(tListItem *) malloc(sizeof(tListItem));


		if(new_item == NULL) {
			List_Var_ERROR();
			return;
		}
		new_item = ptr;
		if(S->First == S->Last){
			new_item->next = NULL;
			S->Last = new_item;
		}
		else{
			new_item->next = S->First;
		}


		S->First = new_item;

  }

tListItem *STopB(tListOfStackB *S){
	if(S->First == NULL){
		List_Var_ERROR();
		return (NULL);
	}
	else{
		return (S->First);

	}
}

tListItem *STopPopB(tListOfStackB *S){
	if (S->First == NULL)  {

		List_Var_ERROR();
		return(NULL);
	}
	else{
		tListItem *pom;
		tListItem *suicide;
		suicide = S->First;
		if (S->First->next == NULL){
			S->First = NULL;
			S->Last = NULL;
		}
		else{
			S->First = S->First->next;
		}

		pom = S->First;
		free(suicide);
		return (pom);
	}
}

bool SEmptyB(tListOfStackB *S){
	if (S->First == NULL){
		return TRUE;
	}
	else{
		return FALSE;
	}
}

tDekVar *initTDekVar(char *name, int type, uValue value){
	tDekVar *new = (tDekVar *) malloc(sizeof(tDekVar));
	if (new == NULL){
		errInternal("Malloc failed in tdekvar");
		return (NULL);
	}
	new->type = type;
	new->ID = name;
	new->value.type = type;
	new->value.value = value;

	return (new);
}

void Block_Init(tListOfVar *V){
	V->Block = NULL;
	V->Blockend = NULL;
}

void Block_Inser_Last(tListOfVar *V, tDekVar *D){

tListVar *new_item =(tListVar *) malloc(sizeof(tListVar));

		if(new_item == NULL)
			{
			List_Var_ERROR();
			return;
			}

			new_item->data = D;
			new_item->next = NULL;

				if (V->Block == NULL){
					V->Block = new_item;
				}
				else
					{
						V->Blockend->next = new_item;
					}
				V->Blockend = new_item;

}

void connect(tListOfVar *V, tListOfStackP *Stack){

	if(SEmptyP(Stack)){
		V->Blockend->next = V->First;
	}
	else{
		V->Blockend->next = STopP(Stack);
	}
	SPushP (Stack,V->Block);

}

void Block_Delete_Last(tListOfVar *V, tListOfStackP *Stack){

	tListVar *suicide;
	if(V == NULL){
		List_Var_ERROR();
		return;
	} 
	if(SEmptyP(Stack)){
		List_Var_ERROR();
		return;
	}
	V->Block = STopPopP(Stack);

	while(V->Block != STopP(Stack) || V->Block != V->First){
		suicide = V->Block;
		V->Block= V->Block->next;

		free(suicide);
		}
	}
void Block_Free(tListOfVar *V, tListOfStackP *Stack){
	tListVar *suicide;

	while(V->Block != V->First){
		suicide = V->Block;
		V->Block= V->Block->next;
		free(suicide);
	}
	while(!SEmptyP(Stack)){
		suicide = STopPopP(Stack);
	}
}

void Block_Var_Insert(tBTNode *B, tLParamElem *block, tListOfStackP *Stack){

	Block_Init(VarList);
	static tLParamElem *pom;
	static int defined = FALSE;
	if(!defined){
		defined = TRUE;
		pom = B->metadata.LBlockItems;
	}
	if (block != NULL){
		pom = block;
	}
	if(pom == NULL){
		List_Var_ERROR();
		return;
	}
	Block_rekurze(pom->BTBlock);
	connect(VarList,Stack);
	pom = pom->NextItem;

}

void Block_rekurze(tBTNode *B){


	tDekVar *V = (tDekVar *) malloc(sizeof(tDekVar));
		if (V == NULL){
			List_Var_ERROR();
			return;
		}
		if (B == NULL){
			Block_Inser_Last(VarList,V);
				return;
			}
		V->ID = B->metadata.name;
		V->type = B->metadata.type;
		V->value.type = B->metadata.type;
		if (B->metadata.type == ST_INT)
			V->value.value.uin = B->metadata.value.uin;
		else if (B->metadata.type == ST_DOUBLE)
			V->value.value.fl = B->metadata.value.fl;
		else if (B->metadata.type == ST_STRING)
			V->value.value.ch = B->metadata.value.ch;


		if (B->Rptr != NULL)
				Block_rekurze(B->Rptr);
		if (B->Lptr != NULL)
				Block_rekurze(B->Lptr);
			Block_Inser_Last(VarList,V);
}

void Block_list_Activate_First(tListOfVar *V){
	//do aktuálniho ukzatatele vloží ukazatel na první prvek seznamu
	// @ V->Act    			-- ukazatel na aktuální pvek v seznamu
	// @ V->Block   		-- ukazatel na první prvek bloku
	//  v případě chyby alokování paměti při zavolání malloc se do pom uloží NULL a zavolá funkci List_Var_ERROR

	if (V == NULL)
	{
		List_Var_ERROR();
		return;
	}
	else{
		V->Act = V->Block;
	}
}



void List_KNST_ERROR(){
	// @ E_RUN_OTHER 		--	chyba
	//	errorMsgvolání funkce která vypisuje chybu zadanou v argumentech

			errInternal("Malloc failed in KNST list");
}



void KNST_List_Init(){

	// @ pom            -- pomocná proměná do které uložím alokované místo v paměti pro strukturu
	// @ pom->Act    	-- ukazatel na aktuální pvek v seznamu
	// @ pom->First     -- ukazatel na první prvek v seznamu
	// @ pom->Last 		-- ukazatel na poslední prvek v seznamu
	// @ VarList        -- globální proměná v které je uložen jednosměrný seznam pro proměnné
	//
	//  v případě chyby alokování paměti při zavolání malloc se do pom uloží NULL a zavolá funkci List_Var_ERROR

	tListOfKNST *pom = (tListOfKNST *) malloc(sizeof(tListOfKNST));

		if (pom == NULL){
			List_Var_ERROR();
			return;
		}
	pom->Act = NULL;
    pom->First = NULL;
    pom->Last = NULL;

    KNSTlist = pom;
 }


void KNST_list_Free(tListOfKNST *K){
	// funkce která uvolní celý seznam z paměti
	// @ tmp            -- pomocná proměnná do které se ukládá struktura
	// @ tmp->Act    	-- ukazatel na aktuální pvek v seznamu
	// @ tmp->First     -- ukazatel na první prvek v seznamu
	// @ tmp->Last 		-- ukazatel na poslední prvek v seznamu
	// funkce free uvolní strukturu z paměti

	tKNST_list *tmp;
		K->Act = K->First;
		while(K->Act != NULL)
		{
			tmp = K->Act->next;
			free(K->Act);
			K->Act = tmp;
		}

	K->First = NULL;
	K->Last = NULL;
	K->Act =NULL;
	free(K);

}

tKNST_list *KNST_list_Insert_Last(tListOfKNST *K, int type, uValue V){
		//vezme strukturu z argumentu a celý alokovaný seznam a uloží strukturu na konec seznamu
		// @ new_item				-- pomocná proměnná do které se ukládá proměnná
		// @ data 					-- ukazatel do struktury který ukazuje na užitečná data
		// @ next 					-- ukazatel do struktury který ukazuje na další strukturu v seznamu
		// @ new_item->Act    		-- ukazatel na aktuální pvek v seznamu
		// @ new_item->First   		-- ukazatel na první prvek v seznamu
		// @ new_item->Last 		-- ukazatel na poslední prvek v seznamu
		//  v případě chyby alokování paměti při zavolání malloc se do pom uloží NULL a zavolá funkci List_Var_ERROR

	if (K == NULL){
		DEBUGGING("KNST INSERT NULL")
		return (NULL);
	}
	DEBUGGING("KNST <- insert last")

	tKNST_list *new_item =(tKNST_list *) malloc(sizeof(tKNST_list));

		if(new_item == NULL)
			{
			DEBUGGING("KNST failed")
			List_Var_ERROR();
			return NULL;
			}
		tDekVar *values = (tDekVar *) malloc(sizeof(tDekVar));
		if (values == NULL){
			errInternal("Malloc failed in tdekvar");
			return (NULL);
		}
		new_item->data = values;
		new_item->data->ID = NULL;
		new_item->data->type = type;
		new_item->data->value.type= type;
		if (type == ST_INT){
			new_item->data->value.value.uin = V.uin;
		}
		else if (type == ST_DOUBLE){
			new_item->data->value.value.fl = V.fl;
		}
		else if (type == ST_STRING){
			new_item->data->value.value.ch = V.ch;
		}


		new_item->next = NULL;


		if (K->Last == NULL) {
			K->First = new_item;
		} else {
			K->Last->next = new_item;
		}
		K->Last = new_item;

		return (K->Last);

}


void KNST_list_Activate_First(tListOfKNST *K){
	//do aktuálniho ukzatatele vloží ukazatel na první prvek seznamu
	// @ V->Act    			-- ukazatel na aktuální pvek v seznamu
	// @ V->First   		-- ukazatel na první prvek v seznamu
	//  v případě chyby alokování paměti při zavolání malloc se do pom uloží NULL a zavolá funkci List_Var_ERROR

	if (K == NULL)
	{
		List_Var_ERROR();
		return;
	}
	else{
		K->Act = K->First;
	}
}

void KNST_list_Activate_Next(tListOfKNST *K){
	// posune aḱtivní prvek na náseledující prvek v seznamu
	// @ V->Act    			-- ukazatel na aktuální pvek v seznamu
	// @ V->Act->next		-- ukazatel na následující prvek seznamu který se nachází za aktuálním prvkem v seznamu seznamu
	//  v případě chyby alokování paměti při zavolání malloc se do pom uloží NULL a zavolá funkci List_Var_ERROR
	if(K->Act != NULL){
		K->Act = K->Act->next;
	}
	else{
		List_Var_ERROR();
		return;
	}
}

tDekVar *KNST_list_Get_Data(tListOfKNST *K){
	// funkce která vrátí tu strukturu na kterou ukzauje aktuální prvek
	// @ V->Act    			-- ukazatel na aktuální pvek v seznamu
	// @ V->Act->data 		-- ukazatel do struktury který ukazuje na užitečná data
	//  v případě chyby alokování paměti při zavolání malloc se do pom uloží NULL a zavolá funkci List_Var_ERROR
	if (K == NULL){
		return (NULL);
	}
	if(K->Act == NULL) {
		List_Var_ERROR();
		return NULL;

	}
	else {
		return (K->Act->data);
	}
}

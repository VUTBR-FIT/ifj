
#include <stdio.h>
#include <stdlib.h>
#include "expressions.h"
#include "lexical_analyzator.h"
#include "global_constants.h"
#include "syntax_analyzator.h"
#include "error.h"
#include "instruction_list.h"
#include <string.h>

int const precTable [][12] = {
		/*	+		-		/		*		==		!=		<		>		<=		>=		(		)	*/
/*  +  */	{EX_S,	EX_S,	EX_R, 	EX_R, 	EX_L, 	EX_L, 	EX_L, 	EX_L,	EX_L,	EX_L,	EX_R,	EX_L},
/*  -  */	{EX_S,	EX_S,	EX_R, 	EX_R, 	EX_L, 	EX_L, 	EX_L, 	EX_L,	EX_L,	EX_L,	EX_R,	EX_L},
/*  /  */	{EX_L,	EX_L,	EX_S, 	EX_S, 	EX_L, 	EX_L, 	EX_L, 	EX_L,	EX_L,	EX_L,	EX_R,	EX_L},
/*  *  */	{EX_L,	EX_L,	EX_S, 	EX_S, 	EX_L, 	EX_L, 	EX_L, 	EX_L,	EX_L,	EX_L,	EX_R,	EX_L},
/* ==  */	{EX_R,	EX_R,	EX_R, 	EX_R, 	EX_L, 	EX_S, 	EX_S, 	EX_S,	EX_S,	EX_S,	EX_R,	EX_L},
/* !=  */	{EX_R,	EX_R,	EX_R, 	EX_R, 	EX_L, 	EX_S, 	EX_S, 	EX_S,	EX_S,	EX_S,	EX_R,	EX_L},
/*  <  */	{EX_R,	EX_R,	EX_R, 	EX_R, 	EX_L, 	EX_S, 	EX_S, 	EX_S,	EX_S,	EX_S,	EX_R,	EX_L},
/*  >  */	{EX_R,	EX_R,	EX_R, 	EX_R, 	EX_L, 	EX_S, 	EX_S, 	EX_S,	EX_S,	EX_S,	EX_R,	EX_L},
/* <=  */	{EX_R,	EX_R,	EX_R, 	EX_R, 	EX_L, 	EX_S, 	EX_S, 	EX_S,	EX_S,	EX_S,	EX_R,	EX_L},
/* >=  */	{EX_R,	EX_R,	EX_R, 	EX_R, 	EX_L, 	EX_S, 	EX_S, 	EX_S,	EX_S,	EX_S,	EX_R,	EX_L},
/*  (  */	{EX_R,	EX_R,	EX_R, 	EX_R, 	EX_L, 	EX_R, 	EX_R, 	EX_R,	EX_R,	EX_R,	EX_R,	EX_S},
/*  )  */	{EX_L,	EX_L,	EX_L, 	EX_L, 	EX_L, 	EX_L, 	EX_L, 	EX_L,	EX_L,	EX_L,	EX_E,	EX_L},
};

tStack *initStack(){
	tStack *stack = NULL;
	if ((stack = (tStack *) malloc(sizeof(tStack))) == NULL){
		errInternal("malloc failed in struct stack");
		return (NULL);
	} else {
		stack->Act = NULL;
		stack->First = NULL;
		stack->Last = NULL;
		return (stack);
	}
}
void pushStack(tStack *stack, tStackElem *elem){
	if ((stack == NULL) || (elem == NULL)){
		DEBUGGING("EXPR - Ukladani na zasobnik se nezdarilo")
		return;
	} else {
		DEBUGGING("EXPR - Ukladam na zasobnik")
		if (stack->First == NULL){
			stack->First = elem;
			stack->Last = elem;
		} else {
			tStackElem *pom = stack->First;
			while (pom->next != NULL){
				pom = pom->next;
			}
			pom->next = elem;
			elem->before = pom;
			stack->Last = elem;
		}
	}
}
void deleteFromStack(tStack *stack, tStackElem *elem){
	if ((stack == NULL) || (elem == NULL)){
		return;
	}
	if (stack->First == elem){
		stack->First = elem->next;
		if (elem->next != NULL){
			elem->next->before = NULL;
		}
	} else if (stack->Last == elem){
		stack->Last = elem->before;
		if (elem->before != NULL){
			elem->before->next = NULL;
		}
	} else {
		elem->before->next = elem->next;
		elem->next->before = elem->before;
	}

}

tStackElem *initStackElem(){
	tStackElem *stack = NULL;
	if ((stack = (tStackElem *) malloc(sizeof(tStackElem))) == NULL){
		errInternal("Malloc in struct stack FAILED");
		return (NULL);
	} else {
		stack->before = NULL;
		stack->data = NULL;
		stack->next = NULL;
		return (stack);
	}
}
void disposeStack(tStack *stack){
	if (stack != NULL){
		tStackElem *pom;
		tExprData *data;
		while (stack->First != NULL){
			pom = stack->First;
			if (pom->data != NULL){
				data = (tExprData *) pom->data;

				free(data);
			}
			stack->First = pom->next;
			free(pom);
		}
		free(stack);
	}
}
int stackIsEmpty(tStack *stack){
	if (stack == NULL){
		return (TRUE);
	} else {
		if (stack->First == NULL){
			return (TRUE);
		} else {
			return (FALSE);
		}
	}
}
void stackActiveLast(tStack *stack){
	if (stack != NULL){
		stack->Act = stack->Last;
	}
}
void stackActiveFirst(tStack *stack){
	if (stack != NULL){
		stack->Act = stack->First;
	}
}
void stackActiveNext(tStack *stack){
	if (stack != NULL){
		stack->Act = stack->Act->next;
	}
}
tStackElem *stackLast(tStack *stack){
	if (stack == NULL){
		return (NULL);
	} else {
		return (stack->Last);
	}
}
// KONEC

tStackElem *createElem(int type, void *name, tStackElem *ptr){
	tExprData *data = initData(type, name, ptr);
	if (data == NULL){
		return (NULL);
	}
	tStackElem *elem = NULL;
	elem = initStackElem();
	if (elem == NULL){
		return (NULL);
	}
	elem->data = data;
	return (elem);
}


void insertPost(tStack *stack, tStackElem *elem){
	if (elem == NULL){
		return;
	} else {
		tStackElem *new = createElem(1, "TERM", NULL);
		if (elem->next == NULL){
			pushStack(stack, new);
		}
		new->next = elem->next;
		new->before = elem;
		if (elem->next != NULL) elem->next->before = new;
		elem->next = new;
	}
}

void replaceElems(tStack *stack, tStackElem *from, tStackElem *to, tKNST_list *data){
	if ((stack == NULL) || (from == NULL) || (to == NULL)){
		return;
	} else {
		tStackElem *new = createElem(-1, data, NULL);
		if (new == NULL){
			return;
		}
		if (stack->First == from){// Je prvni
			stack->First = new;
			new->next = to->next;
			if (to->next != NULL){
				to->next->before = new;
			}
		} else if (stack->Last == to){
			stack->Last = new;
			new->before = from->before;
			if (from->before != NULL){
				from->before->next = new;
			}
		} else {
			if (from->before != NULL){
				from->before->next = new;
				new->before = from->before;
			}
			if(to->next != NULL){
				to->next->before = new;
				new->next = to->next;
			}
		}
	}
}


tExprData *initData(int type, char *name, tStackElem *ptr){
	tExprData *data = NULL;
	if ((data = (tExprData *) malloc(sizeof(tExprData))) == NULL){
		errInternal("Malloc in struct exprData FAILED");
		return (NULL);
	} else {
		data->data = name;
		data->type = type;
		data->list = ptr;
		return (data);
	}
}

void disposeData(tExprData *data){
	if (data == NULL){
		return;
	} else {
		if (data->data != NULL){
			free(data->data);
		}
		free(data);
	}
}

void disposeExpr(tStackElem *stack){
	if (stack == NULL){
		return;
	} else {
		tStackElem *pom = NULL;
		while(stack != NULL){
			if (stack->next == NULL){
				free(stack);
			} else {
				pom = stack;
				stack = stack->next;
				free(pom);
			}
		}
	}
}
tStackElem *popExpr(tStackElem *stack){
	if (stack == NULL){
		return (NULL);
	} else {
		tStackElem *result = stack;
		stack = stack->next;
		stack->before = NULL;
		return (result);
	}
}
// Vraci ukazatel na vlozeny

int isInt(int var){
	return (((var == ST_NUMBER) || (var == ST_NUMBER_EXPONENT)) ? 1 : 0);
}
int isDouble(int var){
	return (((var == ST_DOUBLE_EXPONENT) || (var == ST_DOUBLE_EXPONENT)) ? 1 : 0);
}
int isComma(){
	if (syntaxError != E_OK) return FALSE;
	int state = getToken();
	if (state == E_LEXICAL){
		errLexical();
		freeString();
		return (FALSE);
	} else if (state != ST_COMMA){
		errSyntax("Expected ,");
		freeString();
		return (FALSE);
	} else {
		return (TRUE);
	}
}
int getExpressionType(int state){
	switch (state) {
		case ST_DIV: return (X_DIV); break;
		case ST_ADD: return (X_ADD); break;
		case ST_SUBB: return (X_SUBB); break;
		case ST_MULTI: return (X_MUL); break;
		case ST_EQUAL: return (X_E); break;
		case ST_NOT_EQUAL: return (X_NE); break;
		case ST_ABOVE: return (X_G); break;
		case ST_BELOW: return (X_L); break;
		case ST_ABOVE_EQUAL: return (X_GE); break;
		case ST_BELOW_EQUAL: return (X_LE); break;
		case ST_RBRACKET_L: return (X_LB); break;
		case ST_RBRACKET_R: return (X_RB); break;
		default:
			DEBUGGING("EXPR - Nebyl nalezen vyraz")
			return (-1);
		break;
	}
	return (-1);
}
int isBuildInFunction(){
	if (strcmp("length", actualString->string) == 0){
		return (I_LENGTH);
	} else if (strcmp("substr", actualString->string) == 0){
		return (I_SUBSTR);
	} else if (strcmp("concat", actualString->string) == 0){
		return (I_CONCAT);
	} else if (strcmp("find", actualString->string) == 0){
		return (I_FIND);
	} else if (strcmp("sort", actualString->string) == 0){
		return (I_SORT);
	} else {
		return (-1);
	}
}

void printStack(tStack *stack){
	if (X_DEBUG == FALSE) return;
	tStackElem *pom = stack->First;
	tExprData *data = NULL;
	while (pom != NULL){
			data = (tExprData *) pom->data;
			printf("%d\t", data->type);
			pom = pom->next;
		}
	printf("\n");
}
void printStackPREV(tStack *stack){
	if (X_DEBUG == FALSE) return;
	tStackElem *pom = stack->Last;
	tExprData *data = NULL;
	while (pom != NULL){
		data = (tExprData *) pom->data;
		printf("%d\t", data->type);
		pom = pom->before;
	}
	printf("\n");
}

// end - cím se expression ukoncuje
// mode = TRUE //muze se pouzit funkce
// vraci 1 pokud je vysledek KONST
int expression(int preState, int mode) {
	tStack *stack = initStack();
	tStack *op = initStack();
	int state = -1;
	int countBracket = 0;
	tKNST_list *konst = NULL;
	tBTNode *function = NULL;
	tBTNode *pom = NULL;
	tLParamElem *param = NULL;
	int counter = 0;
	tExprData *data = NULL;
	// Přečte celý výraz až po ukončovaci symbol
	while (1) {
		state = getToken();
		if (stackLast(stack) != NULL){
			data = (tExprData *) stackLast(stack)->data;

			if (((state == ST_ID)
					|| (isInt(state) == TRUE)
					|| (isDouble(state) == TRUE))
					&& ((data->type == ST_ID)
					|| (isInt(data->type) == TRUE)
					|| (isDouble(data->type) == TRUE))){
				errSyntax("Bad expression");
				freeString();
				disposeStack(stack);
				disposeStack(op);
				break;
			}
		}
		if (state == E_LEXICAL){
			freeString();
			errLexical();
			break;
		} else if ((state == preState) && (countBracket == 0)) {
			freeString();
			break;
		} else if (ST_ID == state){
			if ((BTFind(globalTree, actualString->string) == NULL) && ((param = LFind(localTree->metadata.LParamItems, actualString->string)) == NULL) && ((pom = FindInBS(actualString->string, 0)) == NULL) && (isBuildInFunction() == -1)) {
				if (!((token != NULL) && (token->name != NULL) && (strcmp(token->name, actualString->string) == 0))){
					//nenalezeno
					freeString();
					errSemantic();
					return (-1);
				} else {
					pushStack(stack, createElem(state, token->name, NULL));
				}
			} else {
				if ((function = BTFind(globalTree, actualString->string)) != NULL){
					calledFunction(function);
					if (syntaxError != E_OK) return (-1);
					state = getToken();
					if (syntaxError != E_OK) return (-1);
					freeString();
					if (state == ST_SEMICOLON){
						return (1);
					} else if (state == E_LEXICAL){
						errLexical();
						return (-1);
					} else {
						errSyntax("Bad token");
						return (-1);
					}
					break;
				} else if (isBuildInFunction() != -1){
					calledBuildInFuction(isBuildInFunction());
					if (syntaxError != E_OK) return (-1);
					state = getToken();
					if (syntaxError != E_OK) return (-1);
					freeString();
					if (state == ST_SEMICOLON){
						return (1);
					} else if (state == E_LEXICAL){
						errLexical();
						return (-1);
					} else {
						errSyntax("Bad token");
						return (-1);
					}
					break;
				} else {
					if (pom != NULL){
						pushStack(stack, createElem(state, pom->metadata.name, NULL));
					} else {
						pushStack(stack, createElem(state, param->name, NULL));
					}
				}
			}
		} else {
			if (state == ST_LITERAL){
				konst = KNST_list_Insert_Last(KNSTlist, ST_STRING, setUValue(ST_STRING, copyString(actualString->string)));
				if (syntaxError != E_OK) return (-1);
				pushStack(stack, createElem(state, konst, NULL));
			} else if (isInt(state)){
				konst = KNST_list_Insert_Last(KNSTlist, ST_INT, setUValue(ST_INT, actualString->string));
				if (syntaxError != E_OK) return (-1);
				counter++;
				pushStack(stack, createElem(state, konst, NULL));
			} else if (isDouble(state)){
				konst = KNST_list_Insert_Last(KNSTlist, ST_DOUBLE, setUValue(ST_DOUBLE, actualString->string));
				if (syntaxError != E_OK) return (-1);
				pushStack(stack, createElem(state, konst, NULL));
			} else if (getExpressionType(state) >= 0){
				if (state == ST_RBRACKET_L){
					countBracket++;
				} else if (state == ST_RBRACKET_R){
					countBracket--;
				}
				if ((stackIsEmpty(stack) == TRUE) && (state != ST_RBRACKET_L)){
					errSyntax("expression start with operand");
					break;
				}
				if (stackLast(stack) != NULL){
					data = (tExprData *) stackLast(stack)->data;
					if (((data->type == X_RB) && (getExpressionType(state) == X_LB)) || ((getExpressionType(state) < X_LB) && (data->type < X_LB))){
						errSyntax("Bad expression");
						break;
					}
				}
				pushStack(stack, createElem(getExpressionType(state), "", NULL));
				pushStack(op, createElem(getExpressionType(state), "", stackLast(stack)));

			} else {
				errSyntax("Unknown token in EXPRESSION");
				break;
			}
		}
	}
	if (countBracket != 0){
		errSyntax("Bad count of bracket in expression");
	}
	if (syntaxError != E_OK){
		freeString();
		disposeStack(stack);
		disposeStack(op);
		return (-1);
	}
	if (mode != TRUE){
		if (stackIsEmpty(stack)){
			errSyntax("Empty return");
			return (-1);
		}
		parseExpresision(stack, op);
	}
	int result = ((stackIsEmpty(stack))? -1 : 0);
	disposeStack(stack);
	disposeStack(op);
	return (result);
}


void parseExpresision(tStack *stack, tStack *op){
	// INicializace pomocných proměnných
	tStackElem *pom = NULL;
	tExprData *data = NULL;
	tExprData *data1 = NULL;
	tExprData *data2 = NULL;
	tKNST_list *konst = NULL;
	// --- KONEc INICIALIZACE
	printStack(stack);
	printStack(op);

	if (op != NULL){
		pom = op->First;
		while ((op->First != NULL)){
			data = (tExprData *) pom->data;
			if ((data->type == X_LB)){
				DEBUGGING("EXPR - LEVA ZAVORKA")
				if (((tExprData *)data->list->next->next->data)->type == X_RB){
					replaceElems(stack, data->list, data->list->next->next, (tKNST_list *)((tExprData *)data->list->next->data)->data);

					deleteFromStack(op, pom);
					deleteFromStack(op, pom->next);
					DEBUGGING("EXPR - odstraneni zavorek kolem termu")
					if(!stackIsEmpty(op)){
						if (pom->before != NULL){
							DEBUGGING("EXPR - Jde se vlevo")
							pom = pom->before;
						} else{
							DEBUGGING("EXPR - Jde se vpravo")
							pom = pom->next->next;
						}
					}
				} else {
					pom = pom->next;
				}
				printStack(stack);

				continue;
			} else if (data->type == X_RB){
				DEBUGGING("EXPR - PRAVA ZAVORKA")
				pom = pom->before;
				printStack(stack);
				printStack(op);
				continue;
			}
			if (pom->before == NULL){
				DEBUGGING("EXPR - Vlevo NULL")
				if (pom->next == NULL){
					DEBUGGING("EXPR - Vlevo NULL")
					data1 = (tExprData *) data->list->before->data;
					data2 = (tExprData *) data->list->next->data;
					konst = exprGet3AInstruction(data->type, data1->data, data2->data);
					replaceElems(stack, data->list->before, data->list->next, konst);
					deleteFromStack(op, pom);
				} else {
					DEBUGGING("EXPR - Vpravo NOT NULL")
					data1 = (tExprData *) pom->next->data;
					if (precTable[data->type][data1->type] >= EX_L){
						data1 = (tExprData *) data->list->before->data;
						data2 = (tExprData *) data->list->next->data;
						konst = exprGet3AInstruction(data->type, data1->data, data2->data);
						replaceElems(stack, data->list->before, data->list->next, konst);
						deleteFromStack(op, pom);
						DEBUGGING("EXPR - Jdeme vpravo")
						pom = pom->next;
					} else {
						DEBUGGING("EXPR - Jdeme vpravo - nic nedelame")
						pom = pom->next;
					}
				}
			} else {
				DEBUGGING("EXPR - Vlevo NOT NULL")
				if(pom->next == NULL){
					DEBUGGING("EXPR - Vpravo NULL")
					data1 = (tExprData *) pom->before->data;
					if ((precTable[data1->type][data->type] == EX_S)
							|| (precTable[data1->type][data->type] == EX_R)){
						data1 = (tExprData *) data->list->before->data;
						data2 = (tExprData *) data->list->next->data;
						konst = exprGet3AInstruction(data->type, data1->data, data2->data);
						replaceElems(stack, data->list->before, data->list->next, konst);
						deleteFromStack(op, pom);
						DEBUGGING("EXPR - Jdeme vlevo")
						pom = pom->before;
					} else {
						DEBUGGING("EXPR - Jdeme vlevo - nic nedelame")
						pom = pom->before;
					}
				} else {
					DEBUGGING("EXPR - Vpravo NOT NULL")
					data1 = (tExprData *) pom->before->data;
					data2 = (tExprData *) pom->next->data;
					if (((precTable[data1->type][data->type] == EX_S)
							|| (precTable[data1->type][data->type] == EX_R))
							&& ((precTable[data->type][data2->type] == EX_S)
							|| (precTable[data->type][data2->type] == EX_L))){
						konst = exprGet3AInstruction(data->type, data1->data, data2->data);
						replaceElems(stack, data->list->before, data->list->next, konst);
						deleteFromStack(op, pom);
						if (precTable[data1->type][data2->type] >= EX_L){
							DEBUGGING("EXPR - Jdeme vlevo")
							pom = pom->before;
						} else {
							DEBUGGING("EXPR - Jdeme vpravo")
							pom = pom->next;
						}
					} else {
						DEBUGGING("EXPR - Jdeme vlevo - nic nedelame")
						pom = pom->before;
					}
				}
			}
			printStack(stack);
			printStack(op);
		}
	}
}

void calledFunction(tBTNode *function){
	int state = getToken();
	tBTNode *pom = NULL;
	tKNST_list *konst = NULL;
	if (syntaxError != E_OK) return;
	freeString();
	if (state == E_LEXICAL){
		errLexical();
	} else if (state != ST_RBRACKET_L){
		errSyntax("Unexcepted token");
	}
	if (syntaxError != E_OK) return;

	tLParamElem *lastAgr = function->metadata.LParamItems;
	state = ST_CBRACKET_L;
	while (state != ST_RBRACKET_R){
		state = getToken();
		if (syntaxError != E_OK) return;
		if (state == E_LEXICAL){
			freeString();
			errLexical();
		} else if (state == ST_RBRACKET_R){
			if (lastAgr == NULL){
				generateInstr(I_ARG_END, NULL, NULL, function->metadata.name);
				return;
			} else {
				freeString();
				errSemanticBadType();
				return;
			}
		} else if (state == ST_ID){
			if (lastAgr == NULL){
				errSemantic();
				freeString();
				return;
			}
			if ((pom = FindInBS(actualString->string, 0)) != NULL){
				freeString();
				if (pom->metadata.defined == TRUE){
					if ((pom->metadata.type == lastAgr->type) || (pom->metadata.type == ST_AUTO)){
						generateInstr(I_ARG, pom->metadata.name, NULL, NULL);
					} else {
						errSemanticBadType();
						return;
					}
				} else {
					errSemanticBadType();
					return;
				}

			} else {
				freeString();
				errSemantic();
				return;
			}
		} else if (state == ST_LITERAL){
			if (lastAgr == NULL){
				errSemantic();
				freeString();
				return;
			}
			if (lastAgr->type == ST_STRING){
				generateInstr(I_ARG_KONST, NULL, copyString(actualString->string), NULL);
			} else {
				freeString();
				errSemantic();
				return;
			}
		} else if ((state == ST_NUMBER) || (state == ST_NUMBER_EXPONENT)){
			if (lastAgr == NULL){
				errSemantic();
				freeString();
				return;
			}
			if (lastAgr->type == ST_INT){
				konst = KNST_list_Insert_Last(KNSTlist, ST_INT, setUValue(ST_INT, copyString(actualString->string)));
				if (syntaxError != E_OK) return;
				generateInstr(I_ARG, konst, NULL, NULL);
			} else {
				freeString();
				errSemantic();
				return;
			}
		} else if ((state == ST_DOUBLE) || (state == ST_DOUBLE_EXPONENT)){
			if (lastAgr == NULL){
				errSemantic();
				freeString();
				return;
			}
			if (lastAgr->type == ST_DOUBLE){
				konst = KNST_list_Insert_Last(KNSTlist, ST_DOUBLE, setUValue(ST_DOUBLE, copyString(actualString->string)));
				if (syntaxError != E_OK) return;
				generateInstr(I_ARG, konst, NULL, NULL);
			} else {
				freeString();
				errSemantic();
				return;
			}
		} else {
			errSyntax("Unexcepted token");
		}
		freeString();

		if (syntaxError != E_OK) return;
		lastAgr = lastAgr->NextItem;
	}
	return;


}
void calledBuildInFuction(int state){
	freeString();
	tBTNode *pom;
	tLParamElem *param;
	int state2 = getToken();
	if (syntaxError != E_OK) return;
	freeString();
	if (state2 == E_LEXICAL){
		errLexical();
		freeString();
		return;
	} else if (state2 != ST_RBRACKET_L){
		errSyntax("expected (");
		freeString();
		return;
	}
	switch (state) {
		case I_LENGTH:
			DEBUGGING("I_LENGTH")
			state = getToken();
			if (syntaxError != E_OK) return;
			if (state == ST_ID){
				DEBUGGING("I_LENGTH <- ID")
				pom = FindInBS(actualString->string, 0);
				if (pom == NULL){
					param = LFind(localTree->metadata.LParamItems, actualString->string);
					if (param == NULL){
						errSemantic();
						freeString();
						return;
					} else {
						if (param->type != ST_STRING){
							errSemanticBadType();
							return;
						}
						generateInstr(I_LENGTH, param->name, NULL, NULL);
					}
				} else {
					if (pom->metadata.type != ST_STRING){
						errSemanticBadType();
						return;
					}
					generateInstr(I_LENGTH, pom->metadata.name, NULL, NULL);
				}
				state = getToken();
				if (syntaxError != E_OK) return;
				if (state == E_LEXICAL){
					errLexical();
					freeString();
					return;
				} else if (state != ST_RBRACKET_R){
					errSyntax("bad token");
					freeString();
					return;
				}
			} else if (state == ST_LITERAL){
				DEBUGGING("I_LENGTH <- LITERAR")
				generateInstr(I_LENGTH, NULL, copyString(actualString->string), NULL);
				freeString();
				state = getToken();
				if (syntaxError != E_OK) return;
				if (state == E_LEXICAL){
					errLexical();
					freeString();
					return;
				} else if (state != ST_RBRACKET_R){
					errSyntax("bad token");
					freeString();
					return;
				}
				return;
			} else {
				errSyntax("bad token");
				return;
			}
		break;
		case I_SUBSTR:
			generateInstr(I_SUBSTR, NULL, NULL, NULL);
			state = getToken();
			if (syntaxError != E_OK) return;
			if (state == ST_ID){
				pom = FindInBS(actualString->string, 0);
				if (pom == NULL){
					param = LFind(localTree->metadata.LParamItems, actualString->string);
					if (param == NULL){
						errSemantic();
						freeString();
						return;
					} else {
						if (param->type != ST_STRING){
							errSemanticBadType();
							return;
						}
						generateInstr(I_SUBSTR, param->name, NULL, NULL);
					}
				} else {
					if (pom->metadata.type != ST_STRING){
						errSemanticBadType();
						return;
					}
					generateInstr(I_SUBSTR, pom->metadata.name, NULL, NULL);
				}
			} else if (state == ST_LITERAL){
				DEBUGGING("I_SUBSTR <- LITERAR")
				generateInstr(I_SUBSTR, NULL, copyString(actualString->string), NULL);
				freeString();
			} else {
				errSyntax("bad token");
				return;
			}
			isComma();
			if (syntaxError != E_OK) return;
			state = getToken();
			if (syntaxError != E_OK) return;
			if (state == ST_ID){
				pom = FindInBS(actualString->string, 0);
				if (pom == NULL){
					param = LFind(localTree->metadata.LParamItems, actualString->string);
					if (param == NULL){
						errSemantic();
						freeString();
						return;
					} else {
						if (param->type != ST_INT){
							errSemanticBadType();
							return;
						}
						generateInstr(I_SUBSTR, param->name, NULL, NULL);
					}
				} else {
					if (pom->metadata.type != ST_INT){
						errSemanticBadType();
						return;
					}
					generateInstr(I_SUBSTR, pom->metadata.name, NULL, NULL);
				}
			} else if (isInt(state) == TRUE){
				DEBUGGING("I_SUBSTR <- INT")
				generateInstr(I_SUBSTR, NULL, initTDekVar(NULL, ST_INT, setUValue(ST_INT, actualString->string)), NULL);
				freeString();
			} else {
				errSyntax("bad token");
				return;
			}
			isComma();
			state = getToken();
			if (syntaxError != E_OK) return;
			if (state == ST_ID){
				pom = FindInBS(actualString->string, 0);
				if (pom == NULL){
					param = LFind(localTree->metadata.LParamItems, actualString->string);
					if (param == NULL){
						errSemantic();
						freeString();
						return;
					} else {
						if (param->type != ST_INT){
							errSemanticBadType();
							return;
						}
						generateInstr(I_SUBSTR, param->name, NULL, NULL);
					}
				} else {
					if (pom->metadata.type != ST_INT){
						errSemanticBadType();
						return;
					}
					generateInstr(I_SUBSTR, pom->metadata.name, NULL, NULL);
				}
			} else if (isInt(state)){
				DEBUGGING("I_SUBSTR <- INT")
				generateInstr(I_SUBSTR, NULL, initTDekVar(NULL, ST_INT, setUValue(ST_INT, actualString->string)), NULL);
				freeString();
			} else {
				errSyntax("bad token");
				return;
			}
			if (syntaxError != E_OK) return;
			state = getToken();
			if (syntaxError != E_OK) return;
			if (state == E_LEXICAL){
				errLexical();
				freeString();
				return;
			} else if (state != ST_RBRACKET_R){
				errSyntax("bad token");
				freeString();
				return;
			}
			return;
				break;
		case I_CONCAT:
			generateInstr(I_CONCAT, NULL, NULL, NULL);
			state = getToken();
			if (syntaxError != E_OK) return;
			if (state == ST_ID){
				pom = FindInBS(actualString->string, 0);
				if (pom == NULL){
					param = LFind(localTree->metadata.LParamItems, actualString->string);
					if (param == NULL){
						errSemantic();
						freeString();
						return;
					} else {
						if (param->type != ST_STRING){
							errSemanticBadType();
							return;
						}
						generateInstr(I_CONCAT, param->name, NULL, NULL);
					}
				} else {
					if (pom->metadata.type != ST_STRING){
						errSemanticBadType();
						return;
					}
					generateInstr(I_CONCAT, pom->metadata.name, NULL, NULL);
				}
			} else if (state == ST_LITERAL){
				DEBUGGING("I_CONCAT <- LITERAR")
				generateInstr(I_CONCAT, NULL, copyString(actualString->string), NULL);
				freeString();
			} else {
				errSyntax("ddbad token");
				return;
			}
			if (syntaxError != E_OK) return;
			isComma();
			if (syntaxError != E_OK) return;
			state = getToken();
			if (syntaxError != E_OK) return;
			if (state == ST_ID){
				pom = FindInBS(actualString->string, 0);
				if (pom == NULL){
					param = LFind(localTree->metadata.LParamItems, actualString->string);
					if (param == NULL){
						errSemantic();
						freeString();
						return;
					} else {
						if (param->type != ST_STRING){
							errSemanticBadType();
							return;
						}
						generateInstr(I_CONCAT, param->name, NULL, NULL);
					}
				} else {
					if (pom->metadata.type != ST_STRING){
						errSemanticBadType();
						return;
					}
					generateInstr(I_CONCAT, pom->metadata.name, NULL, NULL);
				}
			} else if (state == ST_LITERAL){
				DEBUGGING("I_CONCAT <- LITERAR")
				generateInstr(I_CONCAT, NULL, copyString(actualString->string), NULL);
				freeString();
			} else {
				errSyntax("sssbad token");
				return;
			}
			if (syntaxError != E_OK) return;
			state = getToken();
			if (syntaxError != E_OK) return;
			if (state == E_LEXICAL){
				errLexical();
				freeString();
				return;
			} else if (state != ST_RBRACKET_R){
				errSyntax("ggbad token");
				freeString();
				return;
			}
			return;
				break;
		case I_FIND:
			generateInstr(I_FIND, NULL, NULL, NULL);
			state = getToken();
			if (syntaxError != E_OK) return;
			if (state == ST_ID){
				pom = FindInBS(actualString->string, 0);
				if (pom == NULL){
					param = LFind(localTree->metadata.LParamItems, actualString->string);
					if (param == NULL){
						errSemantic();
						freeString();
						return;
					} else {
						if (param->type != ST_STRING){
							errSemanticBadType();
							return;
						}
						generateInstr(I_FIND, param->name, NULL, NULL);
					}
				} else {
					if (pom->metadata.type != ST_STRING){
						errSemanticBadType();
						return;
					}
					generateInstr(I_FIND, pom->metadata.name, NULL, NULL);
				}
			} else if (state == ST_LITERAL){
				DEBUGGING("I_FIND <- LITERAR")
				generateInstr(I_FIND, NULL, copyString(actualString->string), NULL);
				freeString();
			} else {
				errSyntax("ddbad token");
				return;
			}
			if (syntaxError != E_OK) return;
			isComma();
			if (syntaxError != E_OK) return;
			state = getToken();
			if (syntaxError != E_OK) return;
			if (state == ST_ID){
				pom = FindInBS(actualString->string, 0);
				if (pom == NULL){
					param = LFind(localTree->metadata.LParamItems, actualString->string);
					if (param == NULL){
						errSemantic();
						freeString();
						return;
					} else {
						if (param->type != ST_STRING){
							errSemanticBadType();
							return;
						}
						generateInstr(I_FIND, param->name, NULL, NULL);
					}
				} else {
					if (pom->metadata.type != ST_STRING){
						errSemanticBadType();
						return;
					}
					generateInstr(I_FIND, pom->metadata.name, NULL, NULL);
				}
			} else if (state == ST_LITERAL){
				DEBUGGING("I_FIND <- LITERAR")
				generateInstr(I_FIND, NULL, copyString(actualString->string), NULL);
				freeString();
			} else {
				errSyntax("sssbad token");
				return;
			}
			if (syntaxError != E_OK) return;
			state = getToken();
			if (syntaxError != E_OK) return;
			if (state == E_LEXICAL){
				errLexical();
				freeString();
				return;
			} else if (state != ST_RBRACKET_R){
				errSyntax("ggbad token");
				freeString();
				return;
			}
			return;
				break;
		case I_SORT:
			generateInstr(I_SORT, NULL, NULL, NULL);
			state = getToken();
			if (syntaxError != E_OK) return;
			if (state == ST_ID){
				pom = FindInBS(actualString->string, 0);
				if (pom == NULL){
					param = LFind(localTree->metadata.LParamItems, actualString->string);
					if (param == NULL){
						errSemantic();
						freeString();
						return;
					} else {
						if (param->type != ST_STRING){
							errSemanticBadType();
							return;
						}
						generateInstr(I_SORT, param->name, NULL, NULL);
					}
				} else {
					if (pom->metadata.type != ST_STRING){
						errSemanticBadType();
						return;
					}
					generateInstr(I_SORT, pom->metadata.name, NULL, NULL);
				}
			} else if (state == ST_LITERAL){
				DEBUGGING("I_SORT <- LITERAR")
				generateInstr(I_SORT, NULL, copyString(actualString->string), NULL);
				freeString();
			} else {
				errSyntax("ddbad token");
				return;
			}
			if (syntaxError != E_OK) return;
			state = getToken();
			if (syntaxError != E_OK) return;
			if (state == E_LEXICAL){
				errLexical();
				freeString();
				return;
			} else if (state != ST_RBRACKET_R){
				errSyntax("ggbad token");
				freeString();
				return;
			}
			break;

		default:
		break;
	}
	return;
}


tKNST_list *exprGet3AInstruction(int instruction, void *adr1, void *adr2){
	uValue val;
	val.uin = 0;
	tKNST_list *konst = KNST_list_Insert_Last(KNSTlist, -1, val);

	switch (instruction) {
		case X_ADD:
			generateInstr(I_ADD, konst, adr1, adr2);
			break;
		case X_SUBB:
			generateInstr(I_SUB, konst, adr1, adr2);
			break;
		case X_MUL:
			generateInstr(I_MOV, konst, adr1, adr2);
			break;
		case X_DIV:
			generateInstr(I_DIV, konst, adr1, adr2);
			break;
		case X_NE:
			generateInstr(I_NONEQUAL, konst, adr1, adr2);
			break;
		case X_L:
			generateInstr(I_LESS, konst, adr1, adr2);
			break;
		case X_G:
			generateInstr(I_GREATER, konst, adr1, adr2);
			break;
		case X_LE:
			generateInstr(I_LESSOREQUAL, konst, adr1, adr2);
			break;
		case X_GE:
			generateInstr(I_GREATOREQUAL, konst, adr1, adr2);
			break;
		default:
		break;
	}
	return (konst);
}

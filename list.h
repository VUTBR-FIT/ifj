/*
 * Projekt: IFJ15 2015
 * Autoři:	Blašková Adriana (xblask02)
 * 			Čech Ondřej (xcecho03)
 * 			Černý Lukáš (xcerny63)
 * 			Dokoupil Václav (xdokou12)
 * 			Gábrle Martin (xgabrl01)
 */

#ifndef LIST_H
#define LIST_H

typedef struct LParamElem
{
	char *name;
	int type;
	uValue value;
	int level;
	struct LParamElem *NextItem;
	struct BTNode *BTBlock;			//ukazatel na binarny strom

} tLParamElem;

tLParamElem *LInit();
tLParamElem *LInsert(tLParamElem *UKList, char *name, int type, uValue var);
tLParamElem *LFind(tLParamElem *UKList, char *name);
void LDelete (tLParamElem *UKList);

#endif
/*
 * Projekt: IFJ15 2015
 * Autoři:	Blašková Adriana (xblask02)
 * 			Čech Ondřej (xcecho03)
 * 			Černý Lukáš (xcerny63)
 * 			Dokoupil Václav (xdokou12)
 * 			Gábrle Martin (xgabrl01)
 */
#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include "ial.h"
#include "lexical_analyzator.h"
#include "error.h"
#include "global_constants.h"
#include "list.h"

tBTNode *BTInit()
{
	tBTNode *UKtree;

	if((UKtree = (tBTNode *)malloc(sizeof(tBTNode))) == NULL)
	{
		errorMsg(ERROR_TEMPLATE[E_INTERNAL], "malloc failed");
		syntaxError = E_INTERNAL;
		return (NULL);
	}

	//vytvori sa prazdna polozka

	UKtree->Lptr = NULL;
	UKtree->Rptr = NULL;
	UKtree->metadata.name = NULL;
	UKtree->metadata.BTtabsym = NULL;
	UKtree->metadata.LParamItems = NULL;
	UKtree->metadata.LBlockItems = NULL;

	return (UKtree);		//vrati ukazatel na prvu polozku == koren
}
void BTDeleteT( tBTNode *UKtree)
{
	if (UKtree != NULL)		//kym nie je strom prazdny
	{
		if (UKtree->metadata.name != NULL){		//uvolnenie premennej name
 			free(UKtree->metadata.name);
		}
		if(UKtree->metadata.LParamItems != NULL)	//uvolnenie zoznamu parametrov
		{
			LDelete(UKtree->metadata.LParamItems);
		}
		if(UKtree->metadata.LBlockItems != NULL)	//uvolnenie zoznamu parametrov
		{
			LDelete(UKtree->metadata.LBlockItems);
		}
		if(UKtree->metadata.BTtabsym != NULL)		//uvolnenie tabulky symbolov
		{
			BTDeleteT(UKtree->metadata.BTtabsym);
		}

		//rekurzivne uvolnenie bin. stromu

		BTDeleteT(UKtree->Lptr);

		BTDeleteT(UKtree->Rptr);

		free(UKtree);

	}
}
tBTNode *BTFind(tBTNode *UKtree, char *name)
{
	if (UKtree == NULL){
		return (NULL);
	}
	int n;

	if(UKtree->metadata.name == NULL)		//ak je strom prazdny vrati null
	{
		return (NULL);
	}

	tBTNode *pom = UKtree;

	while (pom != NULL)		//kym nepridem na koniec
	{
		n = strcmp(name, pom->metadata.name);		//porovnanie identifikatorov



		if (n < 0)
		{
			//LEFT
			pom = pom->Lptr;
		}
		else if (n > 0)
		{
			//RIGHT
			pom = pom->Rptr;
		}
		else
		{
			//nasli sme danu polozku
			return (pom);
		}
	}

	return (pom);		//nenasiel sa == null
}
tBTNode *BTInsert(tBTNode *UKtree, char *name, int row, int type, int defined, int declared, uValue var)
{
	if (UKtree == NULL) {
		UKtree = BTInit();
	}

	if(UKtree->metadata.name == NULL)		//vkladanie do prazdneho stromu
	{
		setData(UKtree, name, row, type, defined, declared, var);

		//rozlisenie ci to bude tab. symbolov alebo tab. funkcii
/*		if((UKtree->metadata.defined) != -1)
		{
			UKtree->metadata.BTtabsym = BTInit();
			UKtree->metadata.LParamItems = LInit();
			UKtree->metadata.LBlockItems = LInit();
		}
*/
		return (UKtree);		//vraciam vzdy ukazatel na koren

	}
	else		//vkladanie do neprazdneho stromu
	{
		tBTNode *PomUk;

		PomUk = BTFind(UKtree, name);		//vyhladanie ci uz polozka existuje

		if (PomUk == NULL)
		{
			//nenasiel, vytvori sa novy
			int n;
			tBTNode *PomUk1;

			PomUk = UKtree;

			while(1)		//hlada sa miesto kam sa ulozi polozka
			{
				n = strcmp(name, UKtree->metadata.name);	//left or right


				if (n < 0)
				{
						/*LEFT*/

					PomUk1 = UKtree;
					if ((UKtree = UKtree->Lptr) != NULL)
					{
						continue;	//posuniem sa s znova cyklim
					}

					if((PomUk1->Lptr = BTInit()) == NULL)
					{
						return (NULL);
					}

					setData(PomUk1->Lptr, name, row, type, defined, declared, var);


					/* TF or TS*/

					/*if((PomUk1->metadata.defined) != -1)
					{
						PomUk1->metadata.BTtabsym = BTInit();
						PomUk1->metadata.LParamItems = LInit();
						PomUk1->metadata.LBlockItems = LInit();
					}
					else
					{
						PomUk1->metadata.BTtabsym = NULL;
						PomUk1->metadata.LParamItems = NULL;
						PomUk1->metadata.LBlockItems = NULL;

					}*/

					return (PomUk1->Lptr);		//vraciam vzdy ukazatel na koren

				}
				else if (n > 0)
				{
					/*RIGHT*/

					PomUk1 = UKtree;
					if ((UKtree = UKtree->Rptr) != NULL)
					{
						continue;		//posuniem a cyklim
					}


					if((PomUk1->Rptr = BTInit()) == NULL)
					{
						return (NULL);
					}
					setData(PomUk1->Rptr, name, row, type, defined, declared, var);

					/* TF or TS*/
					/*if((PomUk1->metadata.defined) != -1)
					{
						PomUk1->metadata.BTtabsym = BTInit();
						PomUk1->metadata.LParamItems = LInit();
						PomUk1->metadata.LBlockItems = LInit();
					}
					else
					{
						PomUk1->metadata.BTtabsym = NULL;
						PomUk1->metadata.LParamItems = NULL;
						PomUk1->metadata.LBlockItems = NULL;
					}*/

					return (PomUk1->Rptr);		//vraciam vzdy ukazatel na koren
				}
				else	//sem sa nedostane
				{
					return (PomUk);		//vraciam vzdy ukazatel na koren
				}

			}
			return (PomUk);
		}
		else
		{
			//nasiel, prepise sa
			setData(UKtree, name, row, type, defined, declared, var);
			return (UKtree);		//vraciam vzdy ukazatel na koren
		}
	}

}
void setData(tBTNode *UKtree, char *name, int row, int type, int defined, int declared, uValue var){
	if (UKtree == NULL){
		return;
	} else {
		UKtree->metadata.declared = declared;
		UKtree->metadata.defined = defined;
		UKtree->metadata.name = name;
		UKtree->metadata.row = row;
		UKtree->metadata.type = type;
		UKtree->metadata.value = var;
	}
}

/****************************************************************************/
/****************** Boyer Moore algorithm ***********************************/
/****************************************************************************/


int Max(int a , int b)
{
	return a > b ? a : b;
}

int Min(int a , int b)
{
	return a > b ? b : a;
}

int find(char *text, char *pattern)
{
	if(pattern == NULL)
	{
		return 0;
	}
	
	int TIndex, PIndex, TLength, PLength;
	
	tCharJump CharJump;
	tMatchJump MatchJump;
	
	
	TLength = strlen(text);
	PLength = strlen(pattern);
	
	TIndex = PLength -1;
	PIndex = PLength -1;
	
	if((MatchJump = (tMatchJump)malloc(PLength * sizeof(tMatchJump))) == NULL)
	{
		errInternal("malloc failed");
		return (-2);
	}
	
	
	
	ComputeCharJump(pattern, CharJump, PLength);
	ComputeMatchJump(pattern, MatchJump, PLength);
	
	while((TIndex < TLength && PIndex >= 0))
	{
		// 		printf("asjda\n");
		if(text[TIndex] == pattern[PIndex])
		{
			TIndex--;
			PIndex--;
		}
		else
		{
			
			TIndex += Max(CharJump[(int)text[TIndex]], MatchJump[PIndex]);
			PIndex = PLength - 1;
		}
	}
	
	free(MatchJump);
	
	// 	printf("%d\n" )
	if(PIndex <= 0)
	{
		return (TIndex+1);
	}
	else
	{
		return (-1);		//nenasiel sa
	}
	
	return 0;
}

void ComputeCharJump(char *pattern, tCharJump CharJump, int PLength)
{
	
	for(int i = 0; i < SizeOfJump; i++)
	{
		CharJump[i] = PLength;
	}
	
	for(int i = 0; i < PLength - 1; i++)
	{
		CharJump[(int) pattern[i]] = PLength - i -1;
	}
	
}

void ComputeMatchJump(char *pattern, tMatchJump MatchJump, int PLength)
{
	int k, q, qq, Last;
	tMatchJump Backup;
	
	if((Backup = (tMatchJump)malloc(PLength * sizeof(tMatchJump))) == NULL)
	{
		errInternal("malloc failed");
		return;
	}
	
	Last = PLength -1;
	
	for(k = 0;k <= Last; k++)
	{
		MatchJump[k] = 2*Last-k;
	}
	
	k = Last;
	q = PLength;
	
	while(k > 0)
	{
		Backup[k] = q;
		
		while(q <= Last && pattern[k] != pattern[q])
		{
			MatchJump[q] = Min(MatchJump[q], q-k);
			q = Backup[q];
		}
		
		k--;
		q--;
		
	}
	
	for(k = 0;k <= q; k++)
	{
		MatchJump[k] = Min(MatchJump[k], (Last + q - k));
	}
	
	qq = Backup[q];
	
	while(q < Last)
	{
		while(q <= qq)
		{
			MatchJump[q] = Min(MatchJump[q], (qq-q+Last));
			q++;
		}
		
		qq = Backup[qq];
	}
	
	free(Backup);
	
}

/****************************************************************************/
/************************* Shell sort ***************************************/
/****************************************************************************/

char *sort(char *s)
{
	int step,i,j,num;
	char pom;
	
	num = strlen(s);
	
	step = num / 2;
	
	while(step > 0)
	{
		for(i = step; i < num; i++)
		{
			j = i- step;
			
			while((j >= 0) && (s[j] > s[j+step]))
			{
				pom = s[j];
				s[j] = s[j+step];
				s[j+step] = pom;
				
				j = j-step;
			}
			
		}
		step /= 2;
	}
	
	return s;
}
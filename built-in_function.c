/*
 * Projekt: IFJ15 2015
 * Autoři:		Blašková Adriana (xblask02)
 * 			Čech Ondřej (xcecho03)
 * 			Černý Lukáš (xcerny63)
 * 			Dokoupil Václav (xdokuo12)
 * 			Gábrle Martin (xgabrl01)
 */
#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include "error.h"
#include "built-in_function.h"

/* int length(string s) – Vrátí délku (pocet znaku) retezce zadaného jediným pa- rametrem s. Napr. length("x\nz") == 3 */

int length(char *s)
{
	return(strlen(s));
}

/* string concat(string s1, string s2) – Vrátí ˇretezec, který je konkatenací ˇ
 ˇretezce ˇ s1 a ˇretezce ˇ s2* */

char *concat(char *s1, char *s2)
{
	strcat(s1, s2);
	return s1;
}


/*string substr(string s, int i, int n) – Vrátí podˇretezec zadaného ˇ ˇretez- ˇ
 c e s. Druhým parametrem i je dán zacátek požadovaného p*od ˇ ˇretezce (po ˇ cítáno od ˇ
 nuly) a tˇretí parametr n urcuje délku pod ˇ ˇretezce. V okrajových p ˇ ˇrípadech simulujte
 stejnojmennou metodu tˇrídy basic_string z jazyka C++11*/

// const char *substring(const char *string, int position, int length)
char *substr(char *s, int i, int n)
{
	char *str;
	int c;


	if ((str = (char *)malloc(n+1)) == NULL)
	{
		errInternal("Unable to allocate memory.\n");
	}

	for (c = 0 ; c < n ; c++)
	{
		*(str+c) = *(s+i);
		s++;
	}

	*(str+c) = '\0';

	return (str);
}



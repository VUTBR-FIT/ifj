#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include "ial.h"

int main()
{

	tBTNode *KOREN,*AKT_POLOZKA;


	KOREN = BTInit();		//inicializacia bin stromu, vytvori sa prazdna polozka
					//vrati ukazatel na polozku

	uValue var;
	var.uin = 1;
	BTInsert(KOREN,		//ukazatel na BS
			       "1.item_fun",	//name
				1,		//row
				1,		//type
				1,		//defined !! -1 => premenna | 0/1 => funkcia !!
				1,		//declared
				var);
	printf("PRES KOREN: %s\n", KOREN->metadata.name);
	AKT_POLOZKA = BTInsert(KOREN,	//ukazatel na BS
							  "1.item_sym",
						   1,1,-1,0,var);
	printf("%s\n", BTFind(KOREN, "1.item_sym")->metadata.name);
	/*
	 * ak sa ukalada funkciu tak sa vytvoria aj dve prazdne polozky
	 * 1) tabulka symbolov (BS) -ukazatel == ukazatel_na_polozku->metadata.BTtabsym
	 * 2) zoznam parametrov -ukazatel == ukazatel_na_polozku->metadata.LParamItems
	 */

		//vlozi sa do tab. sym. prvok a vrati ukazatel na nu
	AKT_POLOZKA->metadata.BTtabsym = BTInsert(AKT_POLOZKA->metadata.BTtabsym,	//ukazatel na BS
						  "2.item_sym",
					   1,1,-1,0,var);
	BTInsert(AKT_POLOZKA->metadata.BTtabsym,	//ukazatel na BS
							  "3.item_sym2",
						   1,1,-1,0,var);
	printf("%s\n", AKT_POLOZKA->metadata.BTtabsym->metadata.name);

	BTInsert(AKT_POLOZKA->metadata.BTtabsym,	//ukazatel na BS
						  "4.item_",
					   1,1,-1,0,var);

		//vlozi sa do zoznamu prvok a vrati ukazatel na zaciatok zoznamu
	AKT_POLOZKA->metadata.LParamItems = LInsert(AKT_POLOZKA->metadata.LParamItems,	//ukazatel na zoznam
						    "1.item_par", 			//name
					     1,
					     var);				//type
	LInsert(AKT_POLOZKA->metadata.LParamItems,	//ukazatel na zoznam
							    "2.item_par", 			//name
						     1,
						     var);				//type

	AKT_POLOZKA->metadata.LBlockItems = LInsert(AKT_POLOZKA->metadata.LBlockItems,	//ukazatel na zoznam
						    "1.item_", 			//name
					     1,
					     var);
	printf("PRIMO: %s\n", AKT_POLOZKA->metadata.LBlockItems->name);
	printf("FIND: %s\n", BTFind(KOREN, "1.item_sym")->metadata.LBlockItems->name);
	LInsert(AKT_POLOZKA->metadata.LBlockItems,	//ukazatel na zoznam
						    "2.item", 			//name
					     1,
					     var);


	AKT_POLOZKA->metadata.LBlockItems->BTBlock = BTInsert(AKT_POLOZKA->metadata.LBlockItems->BTBlock,	//ukazatel na BS
						  "1.item_",
					   1,1,-1,0,var);
	BTInsert(AKT_POLOZKA->metadata.LBlockItems->BTBlock,	//ukazatel na BS
							      "1.item",
						       1,1,-1,0,var);


	/*
	 * LInsert - polozky sa ukladaju do jednosmerne viazaneho zoznamu
	 * 	   - vzdy sa uklada na zaciatok
	 */

 	BTDeleteT(KOREN);

	/*
	 * BTDeleteT -uvolni BS aj s podstromami, tab. symbolov a zoznam parametrov
	 */
}





// gcc -std=c99 -Wall -Wextra -pedantic proj1.c ial.c error.c lexical_analyzator.c -o proj1

/*
 * Projekt: IFJ15 2015
 * Autoři:	Blašková Adriana (xblask02)
 * 			Čech Ondřej (xcecho03)
 * 			Černý Lukáš (xcerny63)
 * 			Dokoupil Václav (xdokou12)
 * 			Gábrle Martin (xgabrl01)
 */

#ifndef TOKEN_FUNCTIONS_H
#define TOKEN_FUNCTIONS_H


#include <stdlib.h>
#include <stdio.h>
#include "lexical_analyzator.h"

typedef struct sToken tToken;
struct sToken{
	char *name;				//hodnota kterou nacetl lexikalni analyzator / identifikator
	int row;				//cislo radku na kterem se lexem nacetl (deklaroval)
	int type;				//typ promenne nebo navratova hodnota funkce
	int defined;			//0 - nedefinovano, 1 - definovano	 -- TELO
	int declared;			//0 - nedeklarovano, 1 - deklarovano -- HLAVICKA
	union tokenValue{
		char *ch;
		unsigned int uin;
		float fl;
	}tokenValue;			//hodnota proměnné
	tToken *params;			//v pripade promenne NULL, kdyz funce tak obsahuje seznam parametru
};
/**
 * Inicializace nového tokenu
 * a uložení ho do globalního ukozatele "token"
 */
tToken *initToken();

void freeToken(tToken *pToken);
/**
 * Vypíše token
 *
 * @param tToken* pToken
 */
void PRINT_TOKEN(tToken *pToken);


#endif /* TOKEN_FUNCTIONS_H */

#!/bin/sh

DIR= "./tests/"
make clean > out
echo "make clean - OK"
make > out
echo "make - OK"
rm -f result
rm -f proResults.output

count=0
e_ok=0

state=1
result=0


for file in ./tests/lexical/*; do
	./ifj15 "$file" > out 2>> errors.output
	result=$?
	if [ $result -eq 1 ]; then
		e_ok=$((e_ok + 1))
	fi
	echo "$file $result" >> proResults.output
	count=$((count + 1))	
done
rm -f out
echo "Laxical analyzator: " $e_ok "/" $count

count=0
e_ok=0

for file in ./tests/syntax/*; do
	./ifj15 "$file" > out 2>> errors.output
	result=$?
	if [ $result -eq 2 ]; then
		e_ok=$((e_ok + 1))
	fi
	echo "$file $result" >> proResults.output
	count=$((count + 1))	
done
rm -f out
echo "Syntax analyzator: " $e_ok "/" $count

count=0
e_ok=0

for file in ./tests/semantic/*; do
	./ifj15 "$file" > out 2>> errors.output
	result=$?
	if [ $result -eq 3 ]; then
		e_ok=$((e_ok + 1))
	fi
	echo "$file $result" >> proResults.output
	count=$((count + 1))	
done
rm -f out
echo "Semantic analyzator: " $e_ok "/" $count



/*
 * Projekt: IFJ15 2015
 * Autoři:	Blašková Adriana (xblask02)
 * 			Čech Ondřej (xcecho03)
 * 			Černý Lukáš (xcerny63)
 * 			Dokoupil Václav (xdokou12)
 * 			Gábrle Martin (xgabrl01)
 */
// hlavicka pro interpret


 #ifndef interpret
#define	interpret

#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>
#include <stdbool.h>


#include "error.h"
#include "ial.h"
#include "list.h"


int inter(tListOfInstruction *instrList);

#endif
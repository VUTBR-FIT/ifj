/*
 * Projekt: IFJ15 2015
 * Autoři:	Blašková Adriana (xblask02)
 * 			Čech Ondřej (xcecho03)
 * 			Černý Lukáš (xcerny63)
 * 			Dokoupil Václav (xdokou12)
 * 			Gábrle Martin (xgabrl01)
 */
#include <stdio.h>
#include <stdlib.h>
#include "error.h"
#include "ial.h"
#include "global_constants.h"
#include "lexical_analyzator.h"

#ifndef SYNTAX_ANALYZATOR_H
#define SYNTAX_ANALYZATOR_H

/*
 * Definice maker
 */
#define SET_ERROR if (syntaxError == E_OK) syntaxError = ((state == E_LEXICAL)? E_LEXICAL : E_SYNTAX);


enum expressionState{
	NO_INPUT = -10,
	EXP_EXP = -1,
	EXP_START = ST_START,
};
typedef struct data{
	tBTNode *tree;
	tBTNode *subTree;
	int level;
} tData;

/*
 * vystup z nejake funkce, ktera ma navratovy typ void
 */
int out;
bool isTypeFunction(int type);
bool isTypeVar(int type);
bool isExpressionOperan(int type);
uValue setUValue(int type, char *value);
char *copyString(char *source);
/**
 * Zjisti prvni token a podle toho se zachova
 *
 * @return int syntaxError
 */
ERROR_TYPE parse();


/**
 * Implementace pravidel LL-Gramatiky, kde ne leve strane je  <program>
 */
void program(int currentToken);
void checkFunctionInBS(tBTNode *tree);
tBTNode *InsertToBS(int new, char *name, int row, int type, int defined, int declared, uValue var);
tBTNode *getLastBlock(int searcheLevel);
tBTNode *FindInBS(char *subject, int mode);

/**
 * Implementace pravidel LL-Gramatiky, kde ne leve strane je  <function>
 *
 * @param int state - Obsahuje stav, do kteho se prepne switch
 * @param bool [get]- Indikuje, zda predchozi token patril do soucasneho stavu
 * @patam int [type]- Obshauje typ predchoziho tokenu
 */
void function(int state, bool get);
void arguments(tBTNode *tree);


/**
 * Implementace pravidel LL-Gramatiky, kde na leve strane je <block>
 *
 * @param int state
 * @param bool
 * @param int
 */
void block(int get, int preState);
void if_statement();
void block_cin();
void block_cout();
void block_assign(int state);
void build_in_func(int state);

void block_if();
void block_for();
#endif /* SYNTAX_ANALYZATOR_H */

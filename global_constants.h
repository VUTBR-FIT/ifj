/*
 * Projekt: IFJ15 2015
 * Autoři:	Blašková Adriana (xblask02)
 * 			Čech Ondřej (xcecho03)
 * 			Černý Lukáš (xcerny63)
 * 			Dokoupil Václav (xdokou12)
 * 			Gábrle Martin (xgabrl01)
 */
#ifndef GLOBAL_CONSTANTS_H
#define	GLOBAL_CONSTANTS_H

#include <stdio.h>
#include <stdlib.h>
#include "lexical_analyzator.h"
#include "token_functions.h"
#include "instruction_list.h"
#include "ial.h"
#include "expressions.h"


#define TRUE 1
#define FALSE 0

#define X_DEBUG 0
#define X_INFO 0

#define DEBUGGING(msg); if( X_DEBUG) printf("DEGUGGING: %s\n", msg);
#define INFO(msg); if (X_INFO) printf("INFO: %s\n", msg);

/*
 * Otevřený soubor
 */
FILE *sourceFile;

/*
 * Naposledny načtený / načítaný řetězec LEXem
 */
tActString *actualString;

/*
 * Seznam instrukcí
 */
tListOfInstruction *list;

/*
 * Seznam alokovaných proměných
 */
 tListOfVar *VarList;

/*
 * Seznam konstant
 */
tListOfKNST *KNSTlist;

/*
 * Globální binární strom
 */
 tBTNode *globalTree;

/*
 * Lokaní binární strom
 */
 tBTNode *localTree;
/*
 * Proměná do které se vrací návratová hodnota
 */

/*
 * Hodnota navratové proměnné
 */


uValue Return_value;


/*
 * Ukazatel na token
 */
 tToken *token;

 tListPtrVar * PTRlist;

/*
 * Úroveň zanoření akuálního bloku
 */
int level;

/*
 * Řádek vstupního souboru ze kterého nyní čte LEX
 */
int currentRow;

/**
 * Chybový stav
 * Pokud je vše v poředku tak
 * 		syntaxError = E_OK
 */
int syntaxError;

#endif	/* GLOBAL_CONSTANTS_H */


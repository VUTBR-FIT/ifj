/*
 * Projekt: IFJ15 2015
 * Autoři:	Blašková Adriana (xblask02)
 * 			Čech Ondřej (xcecho03)
 * 			Černý Lukáš (xcerny63)
 * 			Dokoupil Václav (xdokou12)
 * 			Gábrle Martin (xgabrl01)
 */
 //autor této části Ondřej Čech

#include <stdio.h>
#include <stdbool.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include "lexical_analyzator.h"
#include "global_constants.h"
#include "error.h"

//Komentare v leve casti kodu pouzivam pro oznaceni, ze je switch nebo mozna vetev uplne naprogramovana
//Komentare v prave casti kodu pouzivam jako poznamky, ktere jsou treba poresit

//Komentar "je celej" pouzivam pro oznaceni swtichte, ve kterem jsem udelal vsechyn moznosti, ktere mohou nastat. Mam to pro sebe pro kontrolo
//Komentar "kompletni" oznacuje uplne vyresenou jednu vetev moznych znaku dle koncoveho automatu

ERROR_TYPE initString() {
	freeString();
	tActString *newString = (tActString *) malloc(sizeof(tActString));
	if (newString != NULL) {
		char *string = (char *) malloc(START_ALOC_STRING * sizeof(char));
		if (string == NULL) {
			errInternal("malloc failed (allocation structure char)");
			return (E_INTERNAL);
		}
		newString->strLen = 0;
		newString->string = string;
		newString->allocedSize = START_ALOC_STRING;
		actualString = newString;
		INFO("Inicializace actString probehla OK")
		return (E_OK);
	} else {
		errInternal("malloc failed (allocation structure tActString)");
		return (E_INTERNAL);
	}
}

void freeString() {
	if (actualString != NULL) {
		if (actualString->string != NULL) {
			free(actualString->string);
		}
		free(actualString);
		actualString = NULL;
	}
}

bool isMathOperand(int c) {
	if ((c == '+') || (c == '-') || (c == '*') || (c == '=') || (c == '<')
			|| (c == '>') || (c == '/')) {
		//      if(c=='/')
		//           ungetc(c,sourceFile);
		return (true);
	} else
		return (false);
}

bool isCommaOrSemicolon(int c) {
	if ((c == ',') || (c == ';'))
		return (true);
	else
		return (false);
}

bool isEndIDorNo(int c) {
	if ((isCommaOrSemicolon(c)) || (c == '(') || (c == ')') || (isspace(c))
			|| (isMathOperand(c)) || (c == '{') || (c == '}'))
            {
            ungetc(c, sourceFile);
		    return (true);
            }
	else
		return (false);
}

bool isHexadecimal(int c) {
	if ((isdigit(c)) || ((c >= 'a') && (c <= 'f'))
			|| ((c >= 'A') && (c <= 'F')))
		return (true);
	else
		return (false);
}

int HexToInt(int FirstHex, int SecondHex) {
	int prvni = 0, druha = 0;

	if (((FirstHex - 48) >= 0) && ((FirstHex - 48) <= 9))
		prvni = (FirstHex - 48);
	else {
		if ((FirstHex == 'a') || (FirstHex == 'A'))
			prvni = 10;
		if ((FirstHex == 'b') || (FirstHex == 'B'))
			prvni = 11;
		if ((FirstHex == 'c') || (FirstHex == 'C'))
			prvni = 12;
		if ((FirstHex == 'd') || (FirstHex == 'D'))
			prvni = 13;
		if ((FirstHex == 'e') || (FirstHex == 'E'))
			prvni = 14;
		if ((FirstHex == 'f') || (FirstHex == 'F'))
			prvni = 15;
	}
	if (((SecondHex - 48) >= 0) && ((SecondHex - 48) <= 9))
		druha = (SecondHex - 48);
	else {
		if ((SecondHex == 'a') || (SecondHex == 'A'))
			druha = 10;
		if ((SecondHex == 'b') || (SecondHex == 'B'))
			druha = 11;
		if ((SecondHex == 'c') || (SecondHex == 'C'))
			druha = 12;
		if ((SecondHex == 'd') || (SecondHex == 'D'))
			druha = 13;
		if ((SecondHex == 'e') || (SecondHex == 'E'))
			druha = 14;
		if ((SecondHex == 'f') || (SecondHex == 'F'))
			druha = 15;
	}

	return (prvni * 16 + druha);
}

void actString(int c, bool clear) {
	if (clear == true) {
		if (initString() != E_OK)
			return;
	}
	if ((actualString->strLen + 1) >= actualString->allocedSize) {
		if ((actualString->string = (char*) realloc(actualString->string,
				actualString->strLen + ADD_ALOC_STRING)) == NULL) {
                errorMsg(ERROR_TEMPLATE[E_INTERNAL],
					"malloc failed (allocation structure char)");
                syntaxError = E_INTERNAL;
			return;
		}
		actualString->allocedSize = actualString->allocedSize
				+ START_ALOC_STRING;
	}
	actualString->string[actualString->strLen] = (char) c;
	actualString->strLen++;
	actualString->string[actualString->strLen] = '\0';
	return;
}
//v parseru se musí vytvorit gl
int getToken() {
	int c = 0;
	int Asciikod = 0;
	int i = 0;
	int state = ST_START;
	int firstHex = 0, secondHex = 0;
	bool StartLite = true;
	bool EXP_SIGN = false; //pomocna promena na sledovani, zda je za exponentem uvedeno znamenko
	while (true) {
		if (c == '\n') {
			currentRow++;
		}
		c = getc(sourceFile);
		switch (state) {
		case ST_START:
			if (c == EOF) {
				return (ST_EOF);
			} else if (isspace(c)) {
				state = ST_START; //\t, \n, \v, \f, \r
			} else if (isdigit(c)) {
				state = ST_NUMBER; //[0-9]
				actString(c, true); //tuto funkci přepiš a použivej tu funkci actString(int c bool clear)...
			} else if (isalpha(c) || (c == '_')) {
				state = ST_ID; //identifier ^[a-zA-Z] || ^_
				actString(c, true);
			} else if (c == '"') { //literar
				state = ST_LITERAL;
				actString(c, true);
			} else if (c == '/') { //comment or div
				state = IS_COMMENT_OR_DIV;
			} else if (c == '+') {
				return (ST_ADD);
			} else if (c == '-') {
				return (ST_SUBB);
			} else if (c == '*') {
				return (ST_MULTI);
			} else if (c == '=') {
				state = ST_EQUAL;
			} else if (c == '>') {
				state = ST_ABOVE;
			} else if (c == '!') {
				state = ST_NOT_EQUAL;
			} else if (c == '<') {
				state = ST_BELOW;
			} else if (c == ',') {
				return (ST_COMMA);
			} else if (c == ';') {
				return (ST_SEMICOLON);
			} else if (c == '(') {
				return (ST_RBRACKET_L);
			} else if (c == ')') {
				return (ST_RBRACKET_R);
			} else if (c == '{') {
				return (ST_CBRACKET_L);
			} else if (c == '}') {
				return (ST_CBRACKET_R);
			} else {
				return (E_LEXICAL);
			}
			break;
		case ST_NUMBER:
			if (isdigit(c)) {
				actString(c, false);
			} else if (c == '.') {
				state = IS_DOUBLE_NUMMBER;
				actString(c, false);
			} else if ((c == 'e') || (c == 'E')) {
				state = IS_NUMBER_EXPONENT;
				actString(c, false);
			} else if(isEndIDorNo(c))
                return (ST_NUMBER);
			else
				return (E_LEXICAL);
			break;
		case IS_DOUBLE_NUMMBER:
			if (isdigit(c)) {
				state = ST_DOUBLE_NUMMBER;
				actString(c, false);
			} else
				return (E_LEXICAL);
			break;
		case ST_DOUBLE_NUMMBER:
			if (isdigit(c))
				actString(c, false);
			else if ((c == 'e') || (c == 'E')) {
				state = IS_DOUBLE_EXPONENT;
				actString(c, false);
			} else if(isEndIDorNo(c))
                return (ST_DOUBLE_NUMMBER);
			else
				return (E_LEXICAL);
			break;
		case IS_NUMBER_EXPONENT:
			if (isdigit(c)) {
				actString(c, false);
				state = ST_NUMBER_EXPONENT;
			} else if ((c == '+') || (c == '-')) {
				if (EXP_SIGN == false) {
					actString(c, false);
					EXP_SIGN = true;
				} else
					return (E_LEXICAL);
			} else
				return (E_LEXICAL);
			break;
		case IS_DOUBLE_EXPONENT:
			if (isdigit(c)) {
				actString(c, false);
				state = ST_DOUBLE_EXPONENT;
			} else if ((c == '+') || (c == '-')) {
				if (EXP_SIGN == false) {
					actString(c, false);
					EXP_SIGN = true;
				} else
					return (E_LEXICAL);
			} else
				return (E_LEXICAL);
			break;
		case ST_NUMBER_EXPONENT:
			if (isdigit(c))
				actString(c, false);
			else if(isEndIDorNo(c))
                return (ST_NUMBER_EXPONENT);
			else
				return (E_LEXICAL);
			break;
		case ST_DOUBLE_EXPONENT:
			if (isdigit(c))
				actString(c, false);
			else if(isEndIDorNo(c))
                return (ST_DOUBLE_EXPONENT);
			else
				return (E_LEXICAL);
			break;
		case ST_ID:
			if ((isalpha(c)) || (isdigit(c)) || (c == '_'))
				actString(c, false);
			else {
				if (isEndIDorNo(c)) {
					INFO("ID == auto???")
					if (strcmp(actualString->string, "auto") == 0)
						return (ST_AUTO);INFO("ID == cin???")
					if (strcmp(actualString->string, "cin") == 0)
						return (ST_CIN);INFO("ID == cout???")
					if (strcmp(actualString->string, "cout") == 0)
						return (ST_COUT);INFO("ID == double???")
					if (strcmp(actualString->string, "double") == 0)
						return (ST_DOUBLE);INFO("ID == else???")
					if (strcmp(actualString->string, "else") == 0)
						return (ST_ELSE);INFO("ID == for???")
					if (strcmp(actualString->string, "for") == 0)
						return ((ST_FOR));INFO("ID == if???")
					if (strcmp(actualString->string, "if") == 0)
						return (ST_IF);INFO("ID == int???")
					if (strcmp(actualString->string, "int") == 0)
						return (ST_INT);INFO("ID == return???")
					if (strcmp(actualString->string, "return") == 0)
						return (ST_RETURN);INFO("ID == string???")
					if (strcmp(actualString->string, "string") == 0)
						return (ST_STRING);INFO("ID == id?")
					return (ST_ID);
				} else {
					return (E_LEXICAL);
				}
			}
			break;
		case ST_LITERAL:
			if (StartLite) {
				if (initString() != E_OK)
					return (E_INTERNAL);
				StartLite = false;
			}
			if (c == '"')
				return (ST_LITERAL);
			else if (c== '\n')
                return (E_LEXICAL);
            else if (c == '\\') {
				state = IS_START_ASCII;
			} else if (c != EOF)
				actString(c, false);
			else
				return (E_LEXICAL);
			break;
		case IS_START_ASCII:
			if (c == '\\') {
				actString(c, false);
				state = ST_LITERAL;
			} else if (c == 'x') {
				state = IS_ASCII;
			} else if (c == '"') {
				actString(c, false);
				state = ST_LITERAL;
			} else if (c == 'n') {
				actString('\n', false);
				state = ST_LITERAL;
			} else if (c == 't') {
				actString('\t', false);
				state = ST_LITERAL;
			} else {
				actString(c, false);
				state = ST_LITERAL;
			}
			break;
		case IS_ASCII:
			if (isHexadecimal(c)) {
				if (i >= 2)
					return (E_LEXICAL);
				else {
					if (i == 0)
						firstHex = c;
					if (i == 1)
						secondHex = c;
					i++;
					if (i == 2)
                        {
						Asciikod = HexToInt(firstHex, secondHex);
						if (Asciikod >= 32)
                            actString(Asciikod, false);
                        else
                            actString(' ', false);
                        i = 0;
                        state = ST_LITERAL;
                        }
                    }
			} else
				return (E_LEXICAL);
			break;
		case IS_COMMENT_OR_DIV:
			if (c == '/')
				state = ST_LINE_COMMENT;
			else if (c == '*')
				state = ST_BLOCK_COMMENT;
			else {
				ungetc(c, sourceFile);
				return (ST_DIV);
			}
			break;
		case ST_LINE_COMMENT:
			if (c == EOF)
				return (ST_EOF);
			else if (c == '\n')
				state = ST_START;
			break;
		case ST_BLOCK_COMMENT:
			if (c == '*')
				state = ST_BLOCK_COMMENT_END;
			else if (c == EOF)
				return (E_LEXICAL);
			break;
		case ST_BLOCK_COMMENT_END:
			if (c == '/')
				state = ST_START;
			else
				state = ST_BLOCK_COMMENT;
			break;
		case ST_EQUAL:
			if (c == '=')
				return (ST_EQUAL);
			else {
				ungetc(c, sourceFile);
				return (ST_ASSIGNMENT);
			}
			break;
		case ST_ABOVE:
			if (c == '=')
				return (ST_ABOVE_EQUAL);
			else {
				ungetc(c, sourceFile);
				return (ST_ABOVE);
			}
			break;
		case ST_BELOW:
			if (c == '=')
				return (ST_BELOW_EQUAL);
			else {
				ungetc(c, sourceFile);
				return (ST_BELOW);
			}
			break;
        case ST_NOT_EQUAL:
			if (c == '=')
				return (ST_NOT_EQUAL);
            else
				return (E_LEXICAL);
			break;

		default:
			return (E_AUTHOR);
			break;
		}
	}
	return (E_AUTHOR);
}

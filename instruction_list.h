/*
 * Projekt: IFJ15 2015
 * Autoři:	Blašková Adriana (xblask02)
 * 			Čech Ondřej (xcecho03)
 * 			Černý Lukáš (xcerny63)
 * 			Dokoupil Václav (xdokou12)
 * 			Gábrle Martin (xgabrl01)
 */

#ifndef instruction_list
#define	instruction_list

#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>
#include <stdbool.h>


#include "error.h"
#include "ial.h"
#include "list.h"







enum InstructionType {
	InstrUndef = 0 ,
	I_START,
	I_STOP,
	I_READ,
	I_WRITE,
	I_GOTO,
	I_IFSTART,
	I_IFEND,
	I_ELSE,
	I_ASSIGN,
	I_STARTFUNCTION,
	I_CALL,
	I_RETURN,
	I_RETURN_SAVE,
	I_ENDFUNCTION,
	I_MOV,
	I_DIV,
	I_ADD,
	I_SUB,
	I_EQUAL,
	I_LESSOREQUAL,
	I_GREATOREQUAL,
	I_NONEQUAL,
	I_LESS,
	I_GREATER,
	I_LENGTH,
	I_SUBSTR,
	I_CONCAT,
	I_FIND,
	I_SORT,
	I_BLOCK_START,
	I_BLOCK_END,
	I_DATA,
	I_ARG,
	I_ARG_END,
	I_ARG_KONST,
	};

// defionovat typy jednotlivých proměných
//struktura pro tříadresový kód
typedef struct {
	int Instr;					// zakodovaná instrukce která se má provádět
	void *address1;				// první adresa se kterou pracuje instrukce
	void *address2;				// první adresa se kterou pracuje instrukce
	void *address3;				// první adresa se kterou pracuje instrukce

} tInstr;

typedef struct listItem {
    struct listItem *next;		// ukazatel na následující prvek v seznamu
    tInstr *data;				// ukazatel na užitečná data
} tListItem;


typedef struct {
    struct listItem *Act;  		// ukazatel na aktuální prvek
    struct listItem *First;		// ukazatel na první prven
    struct listItem *Last;		// ukazatel na poslední prvek
} tListOfInstruction;


typedef struct typevalue
{
	int type;
	uValue value;

} tValue;


//struktura pro zapsání alokovaných proměných do seznamu
// Jedna proměnná
typedef struct {
	char *ID;						// identifikátor proměnné
	int type;				// druh prroměnné
	tValue value;
	int defined;
} tDekVar;

typedef struct KNSTlist{
	struct KNSTlist *next;		//ukazatel na následující prvek v seznamu
	tDekVar *data;				//ukazatel na užitečná data
} tKNST_list;

// Zasobnikový ramec
typedef struct listofKNST{
	struct KNSTlist *Act;		// ukazatel na aktuální prvek
	struct KNSTlist *First;		// ukazatel na první prvek seznamu
	struct KNSTlist *Last;		// ukazatel na poslední prvek seznamu

} tListOfKNST;



// Pravzání proměnných mezi sebou...
typedef struct listVar{
	struct listVar *next;		//ukazatel na následující prvek v seznamu
	tDekVar *data;				//ukazatel na užitečná data
} tListVar;

// Zasobnikový ramec
typedef struct listofVar{
	struct listVar *Act;		// ukazatel na aktuální prvek
	struct listVar *First;		// ukazatel na první prvek seznamu
	struct listVar *Last;		// ukazatel na poslední prvek seznamu
	struct listVar *Block;
	struct listVar *Blockend;

} tListOfVar;


// Provázaní zásobnikových rámců
typedef struct lPTRVar{			//struktura pro uložení seznamů v určitých funkcí
	struct lPTRVar *Lptr;
	struct lPTRVar *Rptr;
	struct listofVar *PtrVAR;
} tPTRVar;


typedef struct {				//ukazatele na začátek a na konec struktury v které jsou uloženy odkazy na seznamy proměnných
	struct lPTRVar *First;
	struct lPTRVar *Last;
} tListPtrVar;
//


typedef struct listOfStackP{

	struct listVar *First;
	struct listVar *Last;
} tListOfStackP;



typedef struct listOfStackB{

	struct listItem *First;
	struct listItem *Last;
} tListOfStackB;

/*
 * funkce pro vložení informací do struktury a zavolání funkce instruction_list_Insert_Last přiřadí strukturu na konec seznamu
 * @ I            -- proměná do které uložím alokované místo v paměti pro strukturu
 * @ Instr     -- Proměná do které ukládáme určitou instrukci
 * @ address1   -- Proměná v které je uložena první adresa se kterou případně pracuje daná instrukce
 * @ address2   -- Proměná v které je uložena druhá adresa se kterou případně pracuje daná instrukce
 * @ address3   -- Proměná v které je uložena třetí adresa se kterou případně pracuje daná instrukce
 * @ list         -- globální proměná v které je uložen jednosměrný seznam pro instrukce
 *
 *  v případě chyby alokování paměti při zavolání malloc se do pom uloží NULL a zavolá funkci List_ERROR
 */
tInstr *generateInstr(int Instr , void *address1, void *address2, void *address3);
void List_ERROR();
void instruction_list_Init();
void instruction_list_Free(tListOfInstruction *I);
void instruction_list_Insert_Last(tListOfInstruction *I, tInstr *D);
void instruction_list_Activate_First(tListOfInstruction *I);
void instruction_list_Activate_Next(tListOfInstruction *I);
void instruction_list_GO_TO(tListOfInstruction *I, void *gotoInstruction);
void instruction_list_Activate_Last(tListOfInstruction *I);
void setSecondADR(tInstr *I, tInstr *pom);
void setThirdADR(tInstr *I,  tInstr *pom);
tInstr *instruction_list_Get_Data(tListOfInstruction *I);

void List_Var_ERROR();
void Var_List_Init();
void Var_list_Free(tListOfVar *V);
void Var_list_Insert_Last(tListOfVar *V, tDekVar *D);
void Var_list_Activate_First(tListOfVar *V);
void Var_list_Activate_Next(tListOfVar *V);
tDekVar *Var_list_Get_Data(tListOfVar *V);

 void generateVar(tBTNode *N) ;
 //funkce která mi uloží typ , název a hodnotu proměnné do seznamu pro naplnění hodnot volá funkci rekurze
 void Rekurze(tBTNode *N);
 // prochází postubně binárním stromem a vytahuje z něho hodnoty a názvy proměných , které pak rekurzivně ukládá do seznamu .

void PTR_Var_ERROR();
//Funkce která se zavolá když při alokaci nastane chyba
void PTR_List_Init();
// funkce která inicializuje seznam a uloží ukazel do seznamu do globální proměnné PTRlist
void PTR_list_Free(tListPtrVar *P);
// funkce která postupně uvolní celý seznam funkcí
void PTR_list_Insert_Last(tListPtrVar *P, tBTNode *BT);
// funkce která vloží na konec seznamu nový seznam funkce do které se pak ukládají proměnné a jeho hodnoty a typy
void PTR_list_Delete_Last(tListPtrVar *P);
// funkce která smaže poslední prvek v seznamu funkcí
tListOfVar *PTR_list_Get_Data(tListPtrVar *P);
// funkce která nám vrátí hodnotu z posledního seznamu
void setValue(char *I, tValue D);
tValue getValue(char *D);


tDekVar *initTDekVar(char *name, int type, uValue value);

tListOfStackP *SInitP();
void SPushP(tListOfStackP *S, tListVar *ptr);
tListVar *STopPopP(tListOfStackP *S);
tListVar *STopP(tListOfStackP *S);
bool SEmptyP(tListOfStackP *S);

tListOfStackB *SInitB();
void SPushB(tListOfStackB *S, tListItem *ptr);
tListItem *STopPopB(tListOfStackB *S);
tListItem *STopB(tListOfStackB *S);
bool SEmptyB(tListOfStackB *S);

void Block_Init();
void Block_Inser_Last(tListOfVar *V, tDekVar *D);
void connect(tListOfVar *V, tListOfStackP *Stack);
void Block_Delete_Last(tListOfVar *V, tListOfStackP *Stack);
void Block_Free(tListOfVar *V, tListOfStackP *Stack);
void Block_Var_Insert(tBTNode *B, tLParamElem *block, tListOfStackP *Stack);
void Block_rekurze(tBTNode *B);
void Block_list_Activate_First(tListOfVar *V);

void List_KNST_ERROR();
void KNST_List_Init();
void KNST_list_Free(tListOfKNST *K);
tKNST_list *KNST_list_Insert_Last(tListOfKNST *K, int type, uValue V);
void KNST_list_Activate_First(tListOfKNST *K);
void KNST_list_Activate_Next(tListOfKNST *K);
tDekVar *KNST_list_Get_Data(tListOfKNST *K);

#endif

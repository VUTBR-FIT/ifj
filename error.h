/*
 * Projekt: IFJ15 2015
 * Autoři:	Blašková Adriana (xblask02)
 * 			Čech Ondřej (xcecho03)
 * 			Černý Lukáš (xcerny63)
 * 			Dokoupil Václav (xdokou12)
 * 			Gábrle Martin (xgabrl01)
 */
#ifndef ERROR_H_
#define ERROR_H_

#include <stdio.h>
#include <stdlib.h>

typedef enum {
	E_LEXICAL = 1,		// Lexikalní chyby
	E_SYNTAX,			// Syntaktické chyby
	E_SEMANTIC,			// Semantická analýza - nedefinovaná proměnná/funkce nebo pokus o redefincice
	E_SEMANTIC_TYPE_COMPATIBILTY,	// Semantická analýza - chyba typové kompatibility v aritmetických, řetězových a relačních výrazech
						//						špatný počet parametrů u volání funkce
	E_SEMANTIC_UTYPE,	// Semantická analýza - chyba při odvozování datového typu proměnné
	E_SEMANTIC_OTHER,	// Semantická analýza - ostatní chyby
	E_RUN_INPUT,		// Běhová chyba - chyba načítání číselné hodnoty ze vstupu
	E_RUN_VARIABLE,		// Běhová chyba - při práci s neinicializovanou proměnnou
	E_RUN_DIV_ZERO,		// Běhová chyba - dělení nulou
	E_RUN_OTHER,		// Běhová chyba - ostatní běhové chyby
	E_INTERNAL = 99,	// Chyby interpretu - malloc, remalloc, bad arg, bud interpre
	/* Náseldující chybové stavy nejsou součástí požadavků */
	E_AUTHOR = 1000,	// Author FAILED ...
	E_OK = 0,			// All is okay :)
} ERROR_TYPE;

extern const char *ERROR_TEMPLATE[];

void errSyntax(char *msg);
void errLexical();
void errSemantic();
void errSemanticUType();
void errSemanticOther();
void errSemanticBadType();
void errInternal(char *msg);

void errorMsg(const char *template, ...);
#endif /* ERROR_H_ */
